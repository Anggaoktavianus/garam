<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_lahan extends CI_Model
{
    // start datatables
    var $column_order = array(null, 'a.lahan_integrasi','a.lahan_nonintegrasi','a.bulan','a.tahun','b.name','c.name','d.name'); //set column field database for datatable orderable
    var $column_search = array( 'a.lahan_integrasi','a.lahan_nonintegrasi','a.bulan','a.tahun','b.name','c.name','d.name'); //set column field database for datatable searchable
    var $order = array('a.id' => 'asc'); // default order 

    private function _get_datatables_query()
    {
        $this->db->select('a.*, b.name as kabkota, c.name as kecamatan, d.name as desa');
        $this->db->from('garam_lahan a');
        $this->db->join('garam_kota b', 'b.id = a.kabkota_id ' ,'left');
        $this->db->join('garam_kecamatan c', 'c.id = a.kecamatan_id ','left' );
        $this->db->join('garam_desa d', 'd.id = a.kelurahan_id ','left' );
        // $this->db->join('garam_bulan e', 'e.id = a.bulan','left' );
        $this->db->where('a.deleted_at IS NULL');
        $this->db->order_by('a.tahun','desc');
        $i = 0;
        foreach ($this->column_search as $data) { // loop column 
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($data, $_POST['search']['value']);
                } else {
                    $this->db->or_like($data, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

   

    function get_datatables()
    {
        $this->_get_datatables_query();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all()
    {
        $this->db->from('garam_lahan');
       
        return $this->db->count_all_results();
    }
    // end datatables
    // ajax kabkota// DATATABLES ADMINKAB
     private function _get_datatables_query_kota()
    {
        $this->db->select('a.*, b.name as kabkota, c.name as kecamatan, d.name as desa');
        $this->db->from('garam_lahan a');
        $this->db->join('garam_kota b', 'b.id = a.kabkota_id ' ,'left');
        $this->db->join('garam_kecamatan c', 'c.id = a.kecamatan_id ','left' );
        $this->db->join('garam_desa d', 'd.id = a.kelurahan_id ','left' );
        $this->db->where('kabkota_id',$this->session->userdata('kabkota_id'));
        $this->db->where('a.deleted_at IS NULL');
        $this->db->order_by('a.tahun','desc');
        $i = 0;

        foreach ($this->column_search as $data) { // loop column 
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($data, $_POST['search']['value']);
                } else {
                    $this->db->or_like($data, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables_kota()
    {
        $this->_get_datatables_query_kota();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered_kota()
    {
        $this->_get_datatables_query_kota();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all_kota()
    {
        $this->db->from('garam_lahan');
        $this->db->where('kabkota_id',$this->session->userdata('kabkota_id'));
       
        return $this->db->count_all_results();
    }
    // end datatables ADMINKAB
    

    public function getall($id = null)
    {
        
        $this->db->from('garam_lahan');
        
        if ($id != null) {
            $this->db->where('id', $id);
        }
        $query = $this->db->get();
        return $query;
        # code...
    }
    
    public function get($id = null)
    {
        $this->db->select('a.*, b.name as kabkota, c.name as kecamatan, d.name as desa');
        $this->db->from('garam_lahan a');
        $this->db->join('garam_kota b', 'b.id = a.kabkota_id ' ,'left');
        $this->db->join('garam_kecamatan c', 'c.id = a.kecamatan_id ','left' );
        $this->db->join('garam_desa d', 'd.id = a.kelurahan_id ','left' );
        $this->db->where('a.deleted_at IS NULL');
        if ($id != null) {
            $this->db->where('a.id', $id );
            
        }
        $query = $this->db->get();
        return $query;
        # code...
    }

    public function get_kab()
    {
        
         $sql =
            " SELECT * FROM `garam_kota` WHERE `province_id` = '33'";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }
    
    public function get_keca()
    {
        
         $sql =
            " SELECT a.*, c.province_id as provinsi 
            FROM garam_kecamatan a 
            -- INNER JOIN garam_desa b ON b.id = a.district_id
            INNER JOIN garam_kota c ON c.id = a.regency_id
            WHERE c.province_id = '33'";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
        # code...
    }

    public function get_kelu()
    {
         $sql =
            " SELECT a.*, b.regency_id as kel, c.province_id as provinsi 
            FROM garam_desa a 
            INNER JOIN garam_kecamatan b ON b.id = a.district_id
            INNER JOIN garam_kota c ON c.id = b.regency_id
            WHERE c.province_id = '33'";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
        # code...
    }

    public function get_keca_kota()
    {
        
        $this->db->select('a.*,  b.province_id as provinsi ');
        $this->db->from('garam_kecamatan a ');
        $this->db->join('garam_kota b', 'b.id = a.regency_id ' ,'left');
        $this->db->where('b.province_id' , 33);
        $this->db->where('a.regency_id',$this->session->userdata('kabkota_id'));
            
        
        $query = $this->db->get();
        return $query->result();

        
    }
    
    public function get_koperasi()
    {
        
         $sql =
            " SELECT *
            FROM garam_koperasi 
            WHERE deleted_at IS NULL";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
        # code...
    }

    public function get_kelompok()
    {
        
         $sql =
            " SELECT *
            FROM garam_kelompok
            WHERE deleted_at IS NULL";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
        # code...
    }

    public function get_role()
    {
        // $this->db->select('garam_admin.*, garam_kota.name as kabkota');

        // 
         $sql =
            " SELECT * FROM `garam_role` WHERE `id` ";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
        # code...
    }

    public function add() {

        $data = array(  'lahan_integrasi' =>$this->input->post('lahan_integrasi'),
                        'lahan_nonintegrasi' =>$this->input->post('lahan_nonintegrasi'),
                        'kabkota_id'=>$this->input->post('kabkota_id'),
                        'kecamatan_id'=>$this->input->post('kecamatan_id'),
                        'kelurahan_id'=>$this->input->post('desa'),
                        'latitude'=>$this->input->post('latitude'),
                        'longitude'=>$this->input->post('longitude'),
                        'bulan'=>$this->input->post('bulan'),
                        'tahun'=>$this->input->post('tahun'),
                       'created_at'=>date('Y-m-d H:i:s'),
                     );
                       

        return $this->db->insert('garam_lahan',$data);

	}

    public function edit() {
        $id = $this->input->post('id', true);
         
        $data = array(
                     'lahan_integrasi' =>$this->input->post('lahan_integrasi'),
                        'lahan_nonintegrasi' =>$this->input->post('lahan_nonintegrasi'),
                        'kabkota_id'=>$this->input->post('kabkota_id'),
                        'kecamatan_id'=>$this->input->post('kecamatan_id'),
                        'kelurahan_id'=>$this->input->post('desa'),
                        'latitude'=>$this->input->post('latitude'),
                        'longitude'=>$this->input->post('longitude'),
                        'bulan'=>$this->input->post('bulan'),
                        'tahun'=>$this->input->post('tahun'),
                       'updated_at'=>date('Y-m-d H:i:s'),
                      
                    );
         $this->db->where('id', $id); 
    
        $this->db->update('garam_lahan',$data); 

	}

    public function del($id)
    {
        $data = array(
            'deleted_at' => date('Y-m-d H:i:s')
            );
        $this->db->where('id', $id);
        $this->db->update('garam_lahan',$data);
    }

    public function aktivasi() {
        $id = $this->input->post('id', true);
         
        $data = array(
                       
                       'status'=>$this->input->post('status'),
                       'update_at'=>date('Y-m-d H:i:s'),
                      
                    );
         $this->db->where('id', $id); 
    
        $this->db->update('garam_admin',$data); 

	}

    public function bulan()
    {
        
         $sql =
            " SELECT * FROM `garam_lahan` WHERE deleted_at IS NULL GROUP BY bulan";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }
    
    public function tahun()
    {
        
         $sql =
            " SELECT * FROM `garam_lahan` WHERE deleted_at IS NULL GROUP BY tahun";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    function get_latlong($kode){
        $hsl=$this->db->query("SELECT * FROM garam_desa WHERE id ='$kode'");
        if($hsl->num_rows()>0){
            foreach ($hsl->result() as $data) {
                $hsl=array(
                    'id' => $data->id,
                    'name' => $data->name,
                    'lat' => $data->lat,
                    'lng' => $data->lng,
                    );
            }
        }
        return $hsl;
    }

    public function get_koordinat_kota()
    {
        $this->db->select('*');
        $this->db->from('garam_kota a ');
        // $this->db->join('garam_kota b', 'b.id = a.regency_id ' ,'left');
        $this->db->where('a.province_id' , 33);
        $this->db->where('a.id',$this->session->userdata('kabkota_id'));            
        
        $query = $this->db->get();
        return $query->result();

        

        
    }

    public function get_koordinat()
    {
        $this->db->select('a.*, b.name as kabkota,b.id as kabkota_id, c.name as kecamatan, d.name as desa');
        $this->db->from('garam_lahan a');
        $this->db->join('garam_kota b', 'b.id = a.kabkota_id ' ,'left');
        $this->db->join('garam_kecamatan c', 'c.id = a.kecamatan_id ','left' );
        $this->db->join('garam_desa d', 'd.id = a.kelurahan_id ','left' );
        $this->db->where('a.kabkota_id',$this->session->userdata('kabkota_id'));
        $this->db->where('a.deleted_at IS NULL');
        $query = $this->db->get();
        return $query->result();
    }

    public function geojson()
    {
        $this->db->select('a.*');
        $this->db->from('garam_geojson a');
        $this->db->where('a.kabkota_id',$this->session->userdata('kabkota_id'));
        $query = $this->db->get();
        return $query->result();
    }

    public function import_data($databarang)
    {
        $jumlah = count($databarang);
        if ($jumlah > 0) {
            $this->db->replace('garam_lahan', $databarang);
        }
    }

    public function getDataBarang()
    {
        return $this->db->get('garam_lahan')->result_array();
    }

}
