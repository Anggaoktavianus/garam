<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_contact extends CI_Model
{
    
    public function getall($id = null)
    {
        
        $this->db->from('garam_contact');
        
        if ($id != null) {
            $this->db->where('id', $id);
        }
        $query = $this->db->get();
        return $query;
        # code...
    }
    
    public function get($id = null)
    {
        $this->db->select('a.*, b.name as kabkota, c.name as kecamatan, d.name as desa');
        $this->db->from('garam_gudang a');
        $this->db->join('garam_kota b', 'b.id = a.kabkota_id ' ,'left');
        $this->db->join('garam_kecamatan c', 'c.id = a.kecamatan_id ','left' );
        $this->db->join('garam_desa d', 'd.id = a.kelurahan_id ','left' );
        // $this->db->join('garam_role', 'garam_role.id = garam_gudang.id ' );
        $this->db->where('a.deleted_at IS NULL');
        
        if ($id != null) {
            $this->db->where('a.id', $id );
            
        }
        $query = $this->db->get();
        return $query;
        # code...
    }

    

    public function edit() {
        $id = $this->input->post('id', true);
         
        $data = array(
                      'alamat' =>$this->input->post('alamat'),
                      'email'=>$this->input->post('email'),
                      'telepon'=>$this->input->post('telepon'),
                       'lokasi'=>$this->input->post('lokasi'),
                       'wa'=>$this->input->post('wa'),
                       'ig'=>$this->input->post('ig'),
                       'twitter'=>$this->input->post('twitter'),
		       'web'=>$this->input->post('web'),
                       'updated_at'=>date('Y-m-d H:i:s'),
                      
                    );
         $this->db->where('id', $id); 
    
        $this->db->update('garam_contact',$data); 

	}
  
    public function m_cek_mail() {

     return $this->db->get_where('garam_admin',array('email' => $this->input->post('email'),'status' => 1));

    }	
    public function m_cek_alamat() {

     return $this->db->get_where('garam_gudang',array('alamat' => $this->input->post('email')));

    }	

    public function del($id)
    {
        $data = array(
            'deleted_at' => date('Y-m-d H:i:s')
            );
        $this->db->where('id', $id);
        $this->db->update('garam_gudang',$data);
    }
     
    


}
