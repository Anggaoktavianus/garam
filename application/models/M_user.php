<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_user extends CI_Model
{

    
    public function getall($id = null)
    {
        $this->db->from('garam_admin');
        
        if ($id != null) {
            $this->db->where('id', $id);
        }
        $query = $this->db->get();
        return $query;
        # code...
    }
    
    public function get($id = null)
    {
        $this->db->select('garam_admin.*, garam_role.nama as role');
        $this->db->from('garam_admin');
        // $this->db->join('garam_kota', 'garam_kota.id = garam_admin.kabkota_id ' );
        $this->db->join('garam_role', 'garam_role.id = garam_admin.role_id ' );
        $this->db->where('deleted_at IS NULL');
        if ($id != null) {
            $this->db->where('garam_admin.id', $id );
            
        }
        $query = $this->db->get();
        return $query;
    }

    public function get_kab()
    {
        // $this->db->select('garam_admin.*, garam_kota.name as kabkota');
        // 
         $sql =
            " SELECT * FROM `garam_kota` WHERE `province_id` = '33' ORDER BY name ASC";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
        # code...
    }

    public function get_kec($id_kota)
    {
       // Tampilkan semua data kota berdasarkan id provinsi
        $result = $this->db->query("SELECT * FROM garam_kecamatan WHERE regency_id ='$id_kota' ORDER BY name ASC ")->result();

        return $result;
    }

    public function get_kel($id_kec)
    {
       $result = $this->db->query("SELECT * FROM garam_desa WHERE district_id ='$id_kec' ORDER BY name ASC ")->result();

        return $result;

    }

    public function get_aktivasi()
    {
        // $this->db->select('garam_admin.*, garam_kota.name as kabkota');

        // 
         $sql =
            " SELECT a.*, b.nama as role 
            FROM `garam_admin` a
            INNER JOIN `garam_role` b ON b.id = a.role_id 
            WHERE a.role_id = 3 AND a.status = 0 AND deleted_at IS NULL
            ORDER BY a.id DESC";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
        # code...
    }

    public function get_aktivasi_kota()
    {
        // $this->db->select('garam_admin.*, garam_kota.name as kabkota');

        // 
        $this->db->select('a.*,  b.nama as role');
        $this->db->from('garam_admin a ');
        $this->db->join('garam_role b', 'b.id = a.role_id ' ,'left');
        $this->db->where('a.kabkota_id',$this->session->userdata('kabkota_id'));
        $this->db->where('a.role_id',3);
         $this->db->where('a.status',0);
         $this->db->where('deleted_at IS NULL');
        
        //  $sql =
        //     " SELECT a.*, b.nama as role 
        //     FROM `garam_admin` a
        //     INNER JOIN `garam_role` b ON b.id = a.role_id 
        //     WHERE a.role_id = 3 AND a.status = 0 AND deleted_at IS NULL
        //     ORDER BY a.id DESC";

        $query = $this->db->get();
        return $query->result();
        # code...
    }

    public function count()
    {
        // $this->db->select('garam_admin.*, garam_kota.name as kabkota');

        // 
         $sql =
            " SELECT COUNT(status) as total FROM garam_admin WHERE status = 0 AND role_id = 3 AND deleted_at IS NULL";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result_array();
        } else {
            $data = array();
        }

        return $data;
        # code...
    }

    public function count_kota()
    {
        $this->db->select('count(status) as total');
        $this->db->from('garam_admin');  
        $this->db->where('kabkota_id',$this->session->userdata('kabkota_id'));
        $this->db->where('role_id',3);
        $this->db->where('status',0);
        $this->db->where('deleted_at IS NULL');

        // $this->db->join('tblProduct', 'tblSaler.SalerID = tblProduct.SalerID'); 
        // $this->db->group_by('tblSaler.SalerID');       
        $query = $this->db->get();
        return $query->result_array();
        
    }

    public function get_gudang()
    {
        // $this->db->select('garam_admin.*, garam_kota.name as kabkota');

        // 
         $this->db->select('a.*, b.nama as role');
        $this->db->from('garam_admin a');  
        $this->db->join('garam_role b', 'b.id = a.role_id ' ,'left');
        $this->db->where('kabkota_id',$this->session->userdata('kabkota_id'));
        $this->db->where('role_id',3);
        $this->db->where('deleted_at IS NULL');

        // $this->db->join('tblProduct', 'tblSaler.SalerID = tblProduct.SalerID'); 
        // $this->db->group_by('tblSaler.SalerID');       
        $query = $this->db->get();
        return $query->result();

        //  $sql =
        //     " SELECT a.*, b.nama as role FROM `garam_admin` a
        //     INNER JOIN `garam_role` b ON b.id = a.role_id WHERE `role_id` = '3' AND deleted_at IS NULL";

        // $query = $this->db->query($sql);

        // if ($query->num_rows() > 0) {
        //     $data = $query->result();
        // } else {
        //     $data = array();
        // }

        // return $data;
    }

    public function get_role()
    {
        // $this->db->select('garam_admin.*, garam_kota.name as kabkota');

        // 
         $sql =
            " SELECT * FROM `garam_role` WHERE `id` ";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
        # code...
    }
   
    public function m_register() {

        $data = array('nama' =>$this->input->post('nama'),
                      'email'=>$this->input->post('email'),
                      'kabkota_id'=>$this->input->post('kabkota_id'),
                      'kecamatan_id'=>$this->input->post('kecamatan_id'),
                      'kelurahan_id'=>$this->input->post('kelurahan_id'),
                       'role_id'=>$this->input->post('role_id'),
                       'status'=>$this->input->post('status'),
                       'created_at'=>date('Y-m-d H:i:s'),
                      'password'=>get_hash($this->input->post('password')));
                       

        return $this->db->insert('garam_admin',$data);

	}

    public function m_register_gudang() {
        

        $data = array('nama' =>$this->input->post('nama'),
                      'email'=>$this->input->post('email'),
                      'kabkota_id'=>$this->input->post('kabkota_id'),
                      'kecamatan_id'=>$this->input->post('kecamatan_id'),
                      'kelurahan_id'=>$this->input->post('desa'),
                       'role_id'=>$this->input->post('role_id'),
                       'status'=>$this->input->post('status'),
                       'created_at'=>date('Y-m-d H:i:s'),
                      'password'=>get_hash($this->input->post('password')));
                       

        $this->db->insert('garam_admin',$data);

        $data1 = array('nama' =>$this->input->post('nama'),
                      'kabkota_id'=>$this->input->post('kabkota_id'),
                       'kecamatan_id'=>$this->input->post('kecamatan_id'),
                       'kelurahan_id'=>$this->input->post('desa'),
                       'created_at'=>date('Y-m-d H:i:s')
                    );
                       

        return $this->db->insert('garam_gudang',$data1);

	}

    public function edit() {
        $id = $this->input->post('id', true);
         
        $data = array(
                      'nama' =>$this->input->post('nama'),
                      'email'=>$this->input->post('email'),
                      'kabkota_id'=>$this->input->post('kabkota_id'),
                      'kecamatan_id'=>$this->input->post('kecamatan_id'),
                      'kelurahan_id'=>$this->input->post('kelurahan_id'),
                       'role_id'=>$this->input->post('role_id'),
                       'status'=>$this->input->post('status'),
                       'update_at'=>date('Y-m-d H:i:s'),
                      
                    );
         $this->db->where('id', $id); 
    
        $this->db->update('garam_admin',$data); 

	}
  

    public function m_cek_mail() {

     return $this->db->get_where('garam_admin',array('email' => $this->input->post('email'),'status' => 1));

    }
     
    public function m_cek_mails() {

     return $this->db->get_where('garam_admin',array('email' => $this->input->post('email'),'status' => 0));

    }
    public function m_cek_alamat() {

    return $this->db->get_where('garam_gudang',array('alamat' => $this->input->post('email')));

    }	

    public function del($id)
    {
        $data = array(
            'deleted_at' => date('Y-m-d H:i:s')
            );
        $this->db->where('id', $id);
        $this->db->update('garam_admin',$data);
    }

    function reset($id)
    {
        $pass = get_hash('12345');
        
        $data = array(
            'password' => $pass
            );
        $this->db->where('id', $id);
        $reset = $this->db->update('garam_admin', $data);
        if($reset){
            return true;
        }else{
            return false;
        }
    }

    public function aktivasi() {
        $id = $this->input->post('id', true);
         
        $data = array(
                       
                       'status'=>$this->input->post('status'),
                       'update_at'=>date('Y-m-d H:i:s'),
                      
                    );
         $this->db->where('id', $id); 
    
        $this->db->update('garam_admin',$data); 

	}

    public function stock_garam()
    {
        
         $sql =
           "SELECT *, SUM(stock) AS total FROM garam_stock WHERE deleted_at IS NULL";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
        # code...
    }

    public function stock_garam_kota()
    {
        $this->db->select('SUM(stock) as totals');
        $this->db->from('garam_stock');  
        $this->db->where('kabkota_id',$this->session->userdata('kabkota_id'));
        $this->db->where('deleted_at IS NULL');

        // $this->db->join('tblProduct', 'tblSaler.SalerID = tblProduct.SalerID'); 
        // $this->db->group_by('tblSaler.SalerID');       
        $query = $this->db->get();
        return $query->result();
        
    }

    public function lahan_integrasi()
    {
        
         $sql =
           "SELECT *, SUM(lahan_integrasi) AS total FROM garam_lahan WHERE deleted_at IS NULL";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
        # code...
    }

    public function lahan_integrasi_kota()
    {
        $this->db->select('SUM(lahan_integrasi) as totals');
        $this->db->from('garam_lahan');  
        $this->db->where('kabkota_id',$this->session->userdata('kabkota_id'));
        $this->db->where('deleted_at IS NULL');

        // $this->db->join('tblProduct', 'tblSaler.SalerID = tblProduct.SalerID'); 
        // $this->db->group_by('tblSaler.SalerID');       
        $query = $this->db->get();
        return $query->result();
        
    }

    public function lahan_nonintegrasi()
    {
        
         $sql =
           "SELECT *, SUM(lahan_nonintegrasi) AS total FROM garam_lahan WHERE deleted_at IS NULL ";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
        # code...
    }

    public function lahan_nonintegrasi_kota()
    {
        $this->db->select('SUM(lahan_nonintegrasi) as totals');
        $this->db->from('garam_lahan');  
        $this->db->where('kabkota_id',$this->session->userdata('kabkota_id'));
        $this->db->where('deleted_at IS NULL');

        // $this->db->join('tblProduct', 'tblSaler.SalerID = tblProduct.SalerID'); 
        // $this->db->group_by('tblSaler.SalerID');       
        $query = $this->db->get();
        return $query->result();
        
    }

    public function total_penambak()
    {
        
         $sql =
           "SELECT  count(id) AS total FROM garam_petambak WHERE deleted_at IS NULL";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
        # code...
    }

    public function total_penambak_kota()
    {
        $this->db->select('count(id) as totals');
        $this->db->from('garam_petambak');  
        $this->db->where('kabkota_id',$this->session->userdata('kabkota_id'));
        $this->db->where('deleted_at IS NULL');

        // $this->db->join('tblProduct', 'tblSaler.SalerID = tblProduct.SalerID'); 
        // $this->db->group_by('tblSaler.SalerID');       
        $query = $this->db->get();
        return $query->result();
        
    }

    public function total_gudang()
    {
        
         $sql =
           "SELECT  count(id) AS total FROM garam_gudang WHERE deleted_at IS NULL";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
        # code...
    }

    public function total_gudang_kota()
    {
        $this->db->select('count(id) as totals');
        $this->db->from('garam_gudang');  
        $this->db->where('kabkota_id',$this->session->userdata('kabkota_id'));
        $this->db->where('deleted_at IS NULL');

        // $this->db->join('tblProduct', 'tblSaler.SalerID = tblProduct.SalerID'); 
        // $this->db->group_by('tblSaler.SalerID');       
        $query = $this->db->get();
        return $query->result();
        
    }

    public function produksi_integrasi()
    {
        
         $sql =
           "SELECT  SUM(kp1_lahan_integrasi + kp2_lahan_integrasi) AS total FROM garam_produksi WHERE deleted_at IS NULL";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
        # code...
    }

    public function produksi_integrasi_kota()
    {
        $this->db->select('SUM(kp1_lahan_integrasi + kp2_lahan_integrasi) as totals');
        $this->db->from('garam_produksi');  
        $this->db->where('kabkota_id',$this->session->userdata('kabkota_id'));
        $this->db->where('deleted_at IS NULL');

        // $this->db->join('tblProduct', 'tblSaler.SalerID = tblProduct.SalerID'); 
        // $this->db->group_by('tblSaler.SalerID');       
        $query = $this->db->get();
        return $query->result();
        
    }

    public function produksi_nonintegrasi()
    {
        
         $sql =
           "SELECT  SUM(kp1_lahan_nonintegrasi + kp2_lahan_nonintegrasi) AS total FROM garam_produksi WHERE deleted_at IS NULL";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
        # code...
    }

    public function produksi_nonintegrasi_kota()
    {
        $this->db->select('SUM(kp1_lahan_nonintegrasi + kp2_lahan_nonintegrasi) as totals');
        $this->db->from('garam_produksi');  
        $this->db->where('kabkota_id',$this->session->userdata('kabkota_id'));
        $this->db->where('deleted_at IS NULL');

        // $this->db->join('tblProduct', 'tblSaler.SalerID = tblProduct.SalerID'); 
        // $this->db->group_by('tblSaler.SalerID');       
        $query = $this->db->get();
        return $query->result();
        
    }
    

    public function update($data, $id)
    {
         $this->db->form('garam_admin');
        $this->db->where('id', $id);
        $this->db->update('garam_admin', $data);
        return $this->db->affected_rows();
	}

    
    public function getalls($id = null)
    {
        $this->db->select('a.*, b.name as kabkota, c.name as kecamatan, d.name as desa');
        $this->db->from('garam_admin a');
        $this->db->join('garam_kota b', 'b.id = a.kabkota_id ' ,'left');
        $this->db->join('garam_kecamatan c', 'c.id = a.kecamatan_id ','left' );
        $this->db->join('garam_desa d', 'd.id = a.kelurahan_id ','left' );
        $this->db->where('a.kabkota_id',$this->session->userdata('kabkota_id'));
        $this->db->where('a.deleted_at IS NULL');
        if ($id != null) {
            $this->db->where('a.id', $id );
             
        }
        $query = $this->db->get();
        return $query;
        # code...
    }

}
