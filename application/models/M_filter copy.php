<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_filter extends CI_Model {

	var $table = 'garam_kota';
	var $column_order = array(null, 'id','name'); //set column field database for datatable orderable
	var $column_search = array('a.id','a.name','c.name'); //set column field database for datatable searchable 
	var $order = array('id' => 'asc'); // default order 

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query($id_kota, $id_kecamatan, $id_desa)
	{
		
		//add custom filter here
		if($this->input->post('name'))
		{
			$this->db->where('a.name', $this->input->post('name'));
		}
		if($this->input->post('kecamatan'))
		{
			$this->db->like('c.kecamatan', $this->input->post('kecamatan'));
		}
		// if($this->input->post('LastName'))
		// {
		// 	$this->db->like('LastName', $this->input->post('LastName'));
		// }
		// if($this->input->post('address'))
		// {
		// 	$this->db->like('address', $this->input->post('address'));
		// }

		
		$this->db->select('a.*, c.name as kecamatan,c.id as id_kec, d.name as desa, d.id as id_des');
		$this->db->from('garam_kota as a');
		$this->db->join('garam_kecamatan c', 'c.regency_id = a.id ','left' );
		$this->db->join('garam_desa d', 'd.district_id = c.id ','left' );
		$this->db->where('province_id = 33');

		if($id_kota != ''){
			$this->db->where('a.id', $id_kota);
		}

		if($id_kecamatan != ''){
			$this->db->where('c.id', $id_kecamatan);
		}


		if($id_desa != ''){
			$this->db->where('d.id', $id_desa);
		}

		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function get_datatables($id_kota, $id_kecamatan, $id_desa)
	{
		$this->_get_datatables_query($id_kota, $id_kecamatan, $id_desa);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	public function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		// $this->db->from($this->table);
		// $this->db->where('province_id = 33');
		$this->db->select('a.*, c.name as kecamatan,c.id as id_kec, d.name as desa, d.id as id_des');
		$this->db->from('garam_kota as a');
		$this->db->join('garam_kecamatan c', 'c.regency_id = a.id ','left' );
		$this->db->join('garam_desa d', 'd.district_id = c.id ','left' );
		$this->db->where('province_id = 33');
		return $this->db->count_all_results();
	}

	public function get_list_kota()
	{
		$this->db->select('name');
		$this->db->from($this->table);
		$this->db->where('province_id = 33');
		$this->db->order_by('name','asc');
		$query = $this->db->get();
		$result = $query->result();

		$countries = array();
		foreach ($result as $row) 
		{
			$countries[] = $row->name;
		}
		return $countries;
	}
	public function get_list_kecamatan($id_kota)
	{
		$this->db->select('name');
		$this->db->from('garam_kecamatan');
		$this->db->where('regency_id',$id_kota);
		$this->db->order_by('name','asc');
		$query = $this->db->get();
		$result = $query->result();

		$kecamatan = array();
		foreach ($result as $row) 
		{
			$kecamatan[] = $row->name;
		}
		return $kecamatan;
	}

}
