<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_ggn extends CI_Model
{
    // start datatables
    var $column_order = array(null,'a.nama', 'a.masuk','a.keluar','a.sisa','a.harga','b.name','c.name','d.name'); //set column field database for datatable orderable
    var $column_search = array( 'a.nama','a.masuk','a.keluar','a.sisa','a.harga','b.name','c.name','d.name'); //set column field database for datatable searchable
    var $order = array('a.id' => 'asc'); // default order 

    private function _get_datatables_query()
    {
        $this->db->select('a.*, b.name as kabkota, c.name as kecamatan, d.name as desa, e.nama as koperasi, f.nama as kelompok');
        $this->db->from('garam_ggn a');
        $this->db->join('garam_kota b', 'b.id = a.kabkota_id ' ,'left');
        $this->db->join('garam_kecamatan c', 'c.id = a.kecamatan_id ','left' );
        $this->db->join('garam_desa d', 'd.id = a.kelurahan_id ','left' );
        $this->db->join('garam_koperasi e', 'e.id = a.koperasi_id ','left' );
        $this->db->join('garam_kelompok f', 'f.id = a.kelompok_id ','left' );
        $this->db->where('a.deleted_at IS NULL');
        $this->db->order_by('a.id','desc');
        $i = 0;

        foreach ($this->column_search as $data) { // loop column 
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($data, $_POST['search']['value']);
                } else {
                    $this->db->or_like($data, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables()
    {
        $this->_get_datatables_query();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all()
    {
        $this->db->from('garam_ggn');
       
        return $this->db->count_all_results();
    }
    // end datatables
     // DATATABLES ADMINKAB
    private function _get_datatables_query_kota()
    {
        $this->db->select('a.*, b.name as kabkota, c.name as kecamatan, d.name as desa, e.nama as koperasi, f.nama as kelompok');
        $this->db->from('garam_ggn a');
        $this->db->join('garam_kota b', 'b.id = a.kabkota_id ' ,'left');
        $this->db->join('garam_kecamatan c', 'c.id = a.kecamatan_id ','left' );
        $this->db->join('garam_desa d', 'd.id = a.kelurahan_id ','left' );
        $this->db->join('garam_koperasi e', 'e.id = a.koperasi_id ','left' );
        $this->db->join('garam_kelompok f', 'f.id = a.kelompok_id ','left' );
        $this->db->where('a.kabkota_id',$this->session->userdata('kabkota_id'));
        $this->db->where('a.deleted_at IS NULL');
        $this->db->order_by('a.id','desc');
       
        $i = 0;

        foreach ($this->column_search as $data) { // loop column 
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($data, $_POST['search']['value']);
                } else {
                    $this->db->or_like($data, $_POST['search']['value']);
                }
                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables_kota()
    {
        $this->_get_datatables_query_kota();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered_kota()
    {
        $this->_get_datatables_query_kota();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all_kota()
    {
        $this->db->from('garam_ggn');
        $this->db->where('kabkota_id',$this->session->userdata('kabkota_id'));
       
        return $this->db->count_all_results();
    }
    // end datatables ADMINKAB

    public function getall($id = null)
    {
        
        $this->db->from('garam_ggn');
        
        if ($id != null) {
            $this->db->where('id', $id);
        }
        $query = $this->db->get();
        return $query;
        # code...
    }
    
    public function get($id = null)
    {
        $this->db->select('a.*, b.name as kabkota, c.name as kecamatan, d.name as desa');
        $this->db->from('garam_ggn a');
        $this->db->join('garam_kota b', 'b.id = a.kabkota_id ' ,'left');
        $this->db->join('garam_kecamatan c', 'c.id = a.kecamatan_id ','left' );
        $this->db->join('garam_desa d', 'd.id = a.kelurahan_id ','left' );
        // $this->db->join('garam_role', 'garam_role.id = garam_ggn.id ' );
        $this->db->where('a.deleted_at IS NULL');
        
        if ($id != null) {
            $this->db->where('a.id', $id );
            
        }
        $query = $this->db->get();
        return $query;
        # code...
    }

    public function get_filter()
    {
       $sql =
            " SELECT a.*, b.regency_id as id_kel, c.province_id as id_prov ,b.name as kel, c.name as prov
            FROM garam_desa a 
            INNER JOIN garam_kecamatan b ON b.id = a.district_id
            INNER JOIN garam_kota c ON c.id = b.regency_id
            WHERE c.province_id = '33'";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }
    public function get_kab()
    {
        // $this->db->select('garam_admin.*, garam_kota.name as kabkota');

        // 
         $sql =
            " SELECT * FROM `garam_kota` WHERE `province_id` = '33'";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
        # code...
    }
    public function get_keca()
    {
        // $this->db->select('garam_admin.*, garam_kota.name as kabkota');

        // 
         $sql =
            " SELECT a.*, c.province_id as provinsi 
            FROM garam_kecamatan a 
            -- INNER JOIN garam_desa b ON b.id = a.district_id
            INNER JOIN garam_kota c ON c.id = a.regency_id
            WHERE c.province_id = '33'";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
        # code...
    }
    public function get_kelu()
    {
        // $this->db->select('garam_admin.*, garam_kota.name as kabkota');

        // 
         $sql =
            " SELECT a.*, b.regency_id as kel, c.province_id as provinsi 
            FROM garam_desa a 
            INNER JOIN garam_kecamatan b ON b.id = a.district_id
            INNER JOIN garam_kota c ON c.id = b.regency_id
            WHERE c.province_id = '33'";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
        # code...
    }

    public function get_kec($id_kota)
    {
       // Tampilkan semua data kota berdasarkan id provinsi
        $result = $this->db->query("SELECT * FROM garam_kecamatan WHERE regency_id ='$id_kota' ")->result();

        return $result;
    }


    public function get_kel($id_kec)
    {
       $result = $this->db->query("SELECT * FROM garam_desa WHERE district_id ='$id_kec' ")->result();

        return $result;

    }

    public function get_koperasi($id_kel)
    {
       $result = $this->db->query("SELECT * FROM garam_koperasi WHERE kelurahan_id ='$id_kel' ")->result();

        return $result;

    }

    public function get_kelompok($id_kop)
    {
       $result = $this->db->query("SELECT * FROM garam_kelompok WHERE koperasi_id ='$id_kop' ")->result();

        return $result;

    }
    public function get_aktivasi()
    {
        // $this->db->select('garam_admin.*, garam_kota.name as kabkota');

        // 
         $sql =
            " SELECT a.*, b.nama as role 
            FROM `garam_admin` a
            INNER JOIN `garam_role` b ON b.id = a.role_id 
            WHERE a.role_id = 3 AND a.status = 0 AND deleted_at IS NULL
            ORDER BY a.id DESC";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
        # code...
    }
    public function count()
    {
        // $this->db->select('garam_admin.*, garam_kota.name as kabkota');

        // 
         $sql =
            " SELECT COUNT(status) as total FROM garam_admin WHERE status = 0 AND role_id = 3 AND deleted_at IS NULL";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result_array();
        } else {
            $data = array();
        }

        return $data;
        # code...
    }

    public function get_ggn()
    {
        // $this->db->select('garam_admin.*, garam_kota.name as kabkota');

        // 
         $sql =
            " SELECT a.*, b.nama as role FROM `garam_admin` a
            INNER JOIN `garam_role` b ON b.id = a.role_id WHERE `role_id` = '3' AND deleted_at IS NULL";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
        # code...
    }

    

     public function get_role()
    {
        // $this->db->select('garam_admin.*, garam_kota.name as kabkota');

        // 
         $sql =
            " SELECT * FROM `garam_role` WHERE `id` ";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
        # code...
    }
   

    public function add() {

        $data = array(
                
                        'nama' => $this->input->post('nama'),            
                        'masuk' =>$this->input->post('masuk'),
                        'keluar'=>$this->input->post('keluar'),
                        'sisa'=>$this->input->post('masuk')-$this->input->post('keluar'),
		                'harga'=>$this->input->post('harga'),
                        'kabkota_id'=>$this->input->post('kabkota_id'),
                        'kecamatan_id'=>$this->input->post('kecamatan_id'),
                        'koperasi_id'=>$this->input->post('koperasi_id'),
                        'kelompok_id'=>$this->input->post('kelompok_id'),
                        'kelurahan_id'=>$this->input->post('desa'),
                        'created_at'=>date('Y-m-d H:i:s'),
                     );
                       

        return $this->db->insert('garam_ggn',$data);

	}

    

    public function edit() {
        $id = $this->input->post('id', true);
         
        $data = array(
                        'nama' => $this->input->post('nama'),            
                        'masuk' =>$this->input->post('masuk'),
                        'keluar'=>$this->input->post('keluar'),
                        'sisa'=>$this->input->post('masuk')-$this->input->post('keluar'),
		                'harga'=>$this->input->post('harga'),
                        'koperasi_id'=>$this->input->post('koperasi_id'),
                        'kelompok_id'=>$this->input->post('kelompok_id'),
                        'kabkota_id'=>$this->input->post('kabkota_id'),
                        'kecamatan_id'=>$this->input->post('kecamatan_id'),
                        'kelurahan_id'=>$this->input->post('desa'),
                        'updated_at'=>date('Y-m-d H:i:s'),
                      
                    );
        $this->db->where('id', $id); 
    
        $this->db->update('garam_ggn',$data); 

	}
  

    public function m_cek_mail() {

     return $this->db->get_where('garam_admin',array('email' => $this->input->post('email'),'status' => 1));

    }	

    public function m_cek_alamat() {

     return $this->db->get_where('garam_ggn',array('alamat' => $this->input->post('email')));

    }	

    public function del($id)
    {
        $data = array(
            'deleted_at' => date('Y-m-d H:i:s')
            );
        $this->db->where('id', $id);
        $this->db->update('garam_ggn',$data);
    } 

    public function aktivasi() {
        $id = $this->input->post('id', true);
         
        $data = array(
                       
                       'status'=>$this->input->post('status'),
                       'update_at'=>date('Y-m-d H:i:s'),
                      
                    );
         $this->db->where('id', $id); 
    
        $this->db->update('garam_admin',$data); 

	}

    public function import_data($databarang)
    {
        $jumlah = count($databarang);
        if ($jumlah > 0) {
            $this->db->replace('garam_ggn', $databarang);
        }
    }

    public function getDataBarang()
    {
        return $this->db->get('garam_ggn')->result_array();
    }

    function pencarian_d($kecamatan,$desa){
   
        $sql = "SELECT a.*, b.regency_id as id_kel,b.name as kel
                FROM garam_desa a 
                INNER JOIN garam_kecamatan b ON b.id = a.district_id
                -- INNER JOIN garam_kota c ON c.id = b.regency_id
                WHERE a.id = '$desa' AND a.district_id = '$kecamatan' ";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    } 


}
