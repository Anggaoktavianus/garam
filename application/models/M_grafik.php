<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_grafik extends CI_Model
{
    public function grafik_lahan()
    {
        
	// $sql= "SELECT  SUM(`lahan_integrasi`) as lahan_integrasi ,SUM(`lahan_nonintegrasi`) as lahan_nonintegrasi , b.name FROM garam_lahan a
    //         INNER JOIN garam_kota b ON a.kabkota_id = b.id
    //         WHERE b.province_id = '33'
    //         AND a.deleted_at IS NULL GROUP BY a.kabkota_id
    //         ";
            $sql = "SELECT c.nama, SUM(`lahan_integrasi`) as lahan_integrasi ,SUM(`lahan_nonintegrasi`) as lahan_nonintegrasi , a.kabkota_id as kotaid, b.name FROM garam_lahan a
                    INNER JOIN garam_kota b ON b.id = a.kabkota_id 
                    INNER JOIN garam_bulan c ON c.id = MONTH(a.created_at)
                    WHERE  MONTH(a.created_at) = MONTH(CURRENT_DATE()) AND deleted_at IS NULL
                    GROUP BY a.kabkota_id";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }
 
        return $data;
        
    }

    public function grafik_lahan_kota()
    {
        // SELECT SUM(`lahan_integrasi`) as lahan_integrasi, bulan FROM garam_lahan GROUP BY bulan ORDER BY id ASC LIMIT 1
        $this->db->select('c.nama, SUM(`lahan_integrasi`) as lahan_integrasi ,SUM(`lahan_nonintegrasi`) as lahan_nonintegrasi , b.name');
        $this->db->from('garam_lahan a');  
        $this->db->join('garam_desa b', 'a.kelurahan_id = b.id'); 
        $this->db->join('garam_bulan c', 'c.id = MONTH(a.created_at)'); 
        $this->db->where('a.kabkota_id',$this->session->userdata('kabkota_id'));
        $this->db->where('MONTH(a.created_at) = MONTH(CURRENT_DATE())');
        $this->db->where('a.deleted_at IS NULL');
        $this->db->group_by('a.kelurahan_id');       
        $query = $this->db->get();
        return $query->result();
        
    }

    public function grafik_produksi()
    {
        
	$sql= "SELECT  SUM(`kp1_lahan_integrasi`) as kp1_lahan_integrasi ,SUM(`kp2_lahan_integrasi`) as kp2_lahan_integrasi, SUM(`kp3_lahan_integrasi`) as kp3_lahan_integrasi, SUM(`kp1_lahan_nonintegrasi`) as kp1_lahan_nonintegrasi ,SUM(`kp2_lahan_nonintegrasi`) as kp2_lahan_nonintegrasi,SUM(`kp3_lahan_nonintegrasi`) as kp3_lahan_nonintegrasi, SUM(`kp1_garam_nontambak`) as kp1_garam_nontambak ,SUM(`kp2_garam_nontambak`) as kp2_garam_nontambak, SUM(`kp3_garam_nontambak`) as kp3_garam_nontambak, b.name 
            FROM garam_produksi a
            INNER JOIN garam_kota b ON a.kabkota_id = b.id
            WHERE b.province_id = '33'
            AND a.deleted_at IS NULL GROUP BY a.kabkota_id";
            // SELECT MAX(MONTH(CURDATE())) FROM garam_produksi;


        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }
 
        return $data;
        
    }

     public function grafik_produksi_kota()
    {
        $this->db->select('SUM(kp1_lahan_integrasi) as kp1_lahan_integrasi,SUM(`kp2_lahan_integrasi`) as kp2_lahan_integrasi, SUM(`kp3_lahan_integrasi`) as kp3_lahan_integrasi, SUM(`kp1_lahan_nonintegrasi`) as kp1_lahan_nonintegrasi ,SUM(`kp2_lahan_nonintegrasi`) as kp2_lahan_nonintegrasi,SUM(`kp3_lahan_nonintegrasi`) as kp3_lahan_nonintegrasi, SUM(`kp1_garam_nontambak`) as kp1_garam_nontambak ,SUM(`kp2_garam_nontambak`) as kp2_garam_nontambak, SUM(`kp3_garam_nontambak`) as kp3_garam_nontambak, b.name');
        $this->db->from('garam_produksi a');  
        $this->db->join('garam_desa b', 'a.kelurahan_id = b.id'); 
        $this->db->where('a.kabkota_id',$this->session->userdata('kabkota_id'));
        $this->db->where('a.deleted_at IS NULL');
        $this->db->group_by('a.kelurahan_id');       
        $query = $this->db->get();
        return $query->result();
        
    }

    public function grafik_stok()
    {
	// $sql= "SELECT  SUM(`stock`) as stok, b.name ,a.id as id FROM garam_stock a
    //         INNER JOIN garam_kota b ON a.kabkota_id = b.id
    //         WHERE b.province_id = '33'
    //         AND a.deleted_at IS NULL 
    //         GROUP BY a.kabkota_id ";
      $sql= "SELECT c.nama, SUM(`stock`) as stok, a.kabkota_id as kotaid, b.name  FROM garam_stock a
            INNER JOIN garam_kota b ON b.id = a.kabkota_id 
            INNER JOIN garam_bulan c ON c.id = MONTH(a.created_at)
            WHERE  MONTH(a.created_at) = MONTH(CURRENT_DATE()) AND a.deleted_at IS NULL 
            GROUP BY a.kabkota_id" ;

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }
 
        return $data;
        
    }

    public function grafik_stok_kota()
    {
        $this->db->select('c.nama, SUM(`stock`) as stok, b.name as nama,a.id as id');
        $this->db->from('garam_stock a');  
        $this->db->join('garam_desa b', 'a.kelurahan_id = b.id'); 
        $this->db->join('garam_bulan c', 'c.id = MONTH(a.created_at)'); 
        $this->db->where('a.kabkota_id',$this->session->userdata('kabkota_id'));
        $this->db->where('MONTH(a.created_at) = MONTH(CURRENT_DATE())');
        $this->db->where('a.deleted_at IS NULL');
        $this->db->group_by('a.kelurahan_id');       
        $query = $this->db->get();
        return $query->result();
        
    }

    public function grafik_stok_gudang()
    {
        $this->db->select('c.nama, SUM(`stock`) as stok, b.name as nama,a.id as id');
        $this->db->from('garam_stock a');  
        $this->db->join('garam_desa b', 'a.kelurahan_id = b.id'); 
        $this->db->join('garam_bulan c', 'c.id = MONTH(a.created_at)'); 
        $this->db->where('a.kelurahan_id',$this->session->userdata('kelurahan_id'));
        $this->db->where('MONTH(a.created_at) = MONTH(CURRENT_DATE())');
        $this->db->where('a.deleted_at IS NULL');
        $this->db->group_by('a.kelurahan_id');       
        $query = $this->db->get();
        return $query->result();
        
    }

     public function grafik_petambak()
    {
        
	$sql= "SELECT count(a.id) as jum, b.name as nama
        FROM garam_petambak a
        INNER JOIN garam_desa b ON b.id = a.`kelurahan_id`
        WHERE a.deleted_at IS NULL
        GROUP BY a.kelurahan_id ";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }
 
        return $data;
        
    }

    public function grafik_petambak_kota()
    {
        $this->db->select('count(a.kelurahan_id) as jum, b.name as nama');
        $this->db->from('garam_petambak a');  
        $this->db->join('garam_desa b', 'a.kelurahan_id = b.id'); 
        $this->db->where('a.kabkota_id',$this->session->userdata('kabkota_id'));
        $this->db->where('a.deleted_at IS NULL');
        $this->db->group_by('a.kelurahan_id');       
        $query = $this->db->get();
        return $query->result();
        
    }


}
