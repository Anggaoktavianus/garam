<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Garam Gayeng</title>

  <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('assets/dist/img/jateng.png')?>">
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url('assets/plugins/fontawesome-free/css/all.min.css')?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="<?= base_url('assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') ?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?= base_url('assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css')?>">
  <link rel="stylesheet" href="https://cdn.datatables.net/fixedcolumns/3.2.4/css/fixedColumns.dataTables.min.css">
  <!-- JQVMap -->
  <!-- <link rel="stylesheet" href="<?= base_url('assets/plugins/jqvmap/jqvmap.min.css')?>"> -->
  <!-- datatables -->
  
  <link rel="stylesheet" href="<?= base_url('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')?>">
  <link rel="stylesheet" href="<?= base_url('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')?>">
  <link rel="stylesheet" href="<?= base_url('assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url('assets/dist/css/adminlte.min.css')?>">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?= base_url('assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')?>">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?= base_url('assets/plugins/daterangepicker/daterangepicker.css')?>">
  <!-- summernote -->
  <link rel="stylesheet" href="<?= base_url('assets/plugins/summernote/summernote-bs4.min.css')?>">
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" />
  <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>
  
  <!-- <?= $map['js'];?> -->
  
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Preloader -->
  <!-- <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__shake" src="<?= base_url('assets/dist/img/AdminLTELogo.png')?>" alt="AdminLTELogo" height="60" width="60">
  </div> -->

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?= base_url('dashboard')?>" class="nav-link">Home</a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Navbar Search -->
      <li class="nav-item">
        <a class="nav-link" data-widget="navbar-search" href="#" role="button">
          <i class="fas fa-search"></i>
        </a>
        <div class="navbar-search-block">
          <form class="form-inline">
            <div class="input-group input-group-sm">
              <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
              <div class="input-group-append">
                <button class="btn btn-navbar" type="submit">
                  <i class="fas fa-search"></i>
                </button>
                <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                  <i class="fas fa-times"></i>
                </button>
              </div>
            </div>
          </form>
        </div>
      </li>

      <!-- Notif adminprov -->
      <?php if ($this->fungsi->user_login()->role_id == 1  ) { ?>
      <?php 
        foreach($hitung as $row):
        $total = $row['total'];
        ?>
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-bell"></i>
          <span class="badge badge-warning navbar-badge"><?= $total?></span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header"><?= $total?> Notifications</span>
          <div class="dropdown-divider"></div>
          <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#test1">Open Modal 1 </button> -->
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item" data-toggle="modal" data-target="#test1">
            <i class="fas fa-users mr-2"></i> <?= $total?> permintaan aktivasi
            <span class="float-right text-muted text-sm"></span>
          </a>
        </div>
      </li>
      <?php endforeach; ?>
      <?php } ?>

      <!-- Notif adminKab -->
      <?php if ($this->fungsi->user_login()->role_id == 2  ) { ?>
      <?php 
        foreach($hitungs as $row):
        $total = $row['total'];
        ?>
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-bell"></i>
          <span class="badge badge-warning navbar-badge"><?= $total?></span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header"><?= $total?> Notifications</span>
          <div class="dropdown-divider"></div>
          <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#test1">Open Modal 1 </button> -->
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item" data-toggle="modal" data-target="#test1">
            <i class="fas fa-users mr-2"></i> <?= $total?> permintaan aktivasi
            <span class="float-right text-muted text-sm"></span>
          </a>
        </div>
      </li>
      <?php endforeach; ?>
       <?php } ?>
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-user"></i>
          <span class="badge badge-warning navbar-badge"></span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header">Profile</span>
          <div class="dropdown-divider"></div>
          <a href="<?= base_url('user/profile/'.$this->session->userdata('id')) ?>" class="dropdown-item">
            <i class="fas fa-envelope mr-2"></i> <?= $this->session->userdata('email')?>
            <span class="float-right text-muted text-sm"></span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="<?= base_url('user/profile/'.$this->session->userdata('id')) ?>" class="dropdown-item">
            <i class="fas fa-user mr-2"></i> <?= $this->session->userdata('nama')?>
            <span class="float-right text-muted text-sm"></span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="<?=site_url('login/logout')?>" class="dropdown-item">
            <i class="fas fa-power-off mr-2"></i> Logout
            <span class="float-right text-muted text-sm"></span>
          </a>
          <div class="dropdown-divider"></div>
          
        </div>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?= base_url('dashboard')?>" class="brand-link">
      <img src="<?= base_url('assets/dist/img/jateng.png')?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Garam Gayeng</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex"> 
        <div class="image">
          <img src="<?= base_url('assets/dist/img/user2-160x160.jpg')?>" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="<?= base_url('user/profile/'.$this->session->userdata('id')) ?>" class="d-block"><?= $this->session->userdata('nama')?></a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <!-- <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div> -->
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="<?= site_url('dashboard')?>" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <i class="right fas fa-angle"></i>
              </p>
            </a>
          </li>
          <?php if ($this->fungsi->user_login()->role_id == 1  ) { ?>
          <li class="nav-item">
            <a href="<?= site_url('lahan') ?>" class="nav-link">
              <i class="nav-icon fas fa-map-marker-alt"></i>
              <p>
                Data Lahan
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>
	        <li class="nav-item">
            <a href="<?= site_url('produksi') ?>" class="nav-link">
              <i class="nav-icon fas fa-list"></i>
              <p>
                Data Produksi
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>
	        <li class="nav-item">
            <a href="<?= site_url('stock') ?>" class="nav-link">
              <i class="nav-icon fas fa-chart-bar"></i>
              <p>
                Data Stok Garam
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= site_url('koperasi') ?>" class="nav-link">
              <i class="nav-icon fas fa-pallet"></i>
              <p>
                Data Koperasi
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= site_url('kelompok') ?>" class="nav-link">
              <i class="nav-icon fas fa-object-group"></i>
              <p>
                Data Kelompok
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>
	        <li class="nav-item">
            <a href="<?= site_url('petambak') ?>" class="nav-link">
              <i class="nav-icon fas fa-user-friends"></i>
              <p>
                Data Petambak
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-warehouse"></i>
              <p>
                Data Gudang Garam
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= site_url('gudang') ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>
                    Gudang Garam Rakyat
                    <span class="right badge badge-danger"></span>
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= site_url('GGN') ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>
                    Gudang Garam Nasional
                    <span class="right badge badge-danger"></span>
                  </p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item">
            <a href="<?= site_url('hibah') ?>" class="nav-link">
              <i class="nav-icon fas fa-boxes"></i>
              <p>
                Data Hibah
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>
	        
           <!-- <li class="nav-item">
            <a href="<?= site_url('laporan')?>" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Laporan
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li> -->
          <li class="nav-item">
            <a href="<?= site_url('user')?>" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Management User
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= site_url('contact') ?>" class="nav-link">
              <i class="nav-icon fas fa-address-book"></i>
              <p>
                Contact
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>
          
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Rekap
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= site_url('rekap/')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Lahan Garam</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= site_url('rekap/produksi')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Produksi Garam</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= site_url('rekap/stok')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Stok Garam</p>
                </a>
              </li>
              <!-- <li class="nav-item">
                <a href="<?= site_url('rekap/petambak')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Petambak Garam</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= site_url('rekap/kelompok')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Kelompok</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= site_url('rekap/koperasi')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Koperasi</p>
                </a>
              </li> -->
              <li class="nav-item">
                <a href="<?= site_url('rekap/petambak')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Petambak Garam</p>
                </a>
              </li>
              
            </ul>
          </li>
            <?php } ?>
            <?php if ($this->fungsi->user_login()->role_id == 2  ) { ?>
          <li class="nav-item">
            <a href="<?= site_url('lahan') ?>" class="nav-link">
              <i class="nav-icon fas fa-map-marker-alt"></i>
              <p>
                Data Lahan
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>
	        <li class="nav-item">
            <a href="<?= site_url('produksi') ?>" class="nav-link">
              <i class="nav-icon fas fa-list"></i>
              <p>
                Data Produksi
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>
	        <li class="nav-item">
            <a href="<?= site_url('stock') ?>" class="nav-link">
              <i class="nav-icon fas fa-chart-bar"></i>
              <p>
                Data Stok Garam
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= site_url('koperasi') ?>" class="nav-link">
              <i class="nav-icon fas fa-pallet"></i>
              <p>
                Data Koperasi
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= site_url('kelompok') ?>" class="nav-link">
              <i class="nav-icon fas fa-object-group"></i>
              <p>
                Data Kelompok
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>
	        <li class="nav-item">
            <a href="<?= site_url('petambak') ?>" class="nav-link">
              <i class="nav-icon fas fa-user-friends"></i>
              <p>
                Data Petambak
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-warehouse"></i>
              <p>
                Data Gudang Garam
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= site_url('gudang') ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>
                    Gudang Garam Rakyat
                    <span class="right badge badge-danger"></span>
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= site_url('GGN') ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>
                    Gudang Garam Nasional
                    <span class="right badge badge-danger"></span>
                  </p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item">
            <a href="<?= site_url('hibah') ?>" class="nav-link">
              <i class="nav-icon fas fa-boxes"></i>
              <p>
                Data Hibah
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>
	        
           <!-- <li class="nav-item">
            <a href="<?= site_url('laporan')?>" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Laporan
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li> -->
          
          <li class="nav-item">
            <a href="<?= site_url('user')?>" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Management User
              </p>
            </a>
          </li>
          
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Rekap
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= site_url('rekap/lahan_kota')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Lahan Garam</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= site_url('rekap/produksi_kota')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Produksi Garam</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= site_url('rekap/stok_kota')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Stok Garam</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= site_url('rekap/petambak_kota')?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Petambak Garam</p>
                </a>
              </li>
              
            </ul>
          </li>
	       
            <?php } ?>
            <?php if ($this->fungsi->user_login()->role_id == 3) { ?>
            <li class="nav-item">
            <a href="<?= base_url('stock')?>" class="nav-link">
              <i class="nav-icon fas fa-chart-bar"></i>
              <p>
                Data Stok Garam
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>
          <?php } ?>
	        <li class="nav-item">
          <!-- <li class="nav-item">
            <a href="<?= site_url('login/logout')?>" class="nav-link">
              <i class="nav-icon fas fa-power-off"></i>
              <p>
                Logout
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li> -->
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

<!-- MODAL AKTIVASI AKUN -->
<div id="test1" class="modal fade" role="dialog" style="z-index: 1400;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Ini adalah Bagian Header Modal -->
            <div class="modal-header">
                <h4 class="modal-title">Akun belum teraktivasi</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Ini adalah Bagian Body Modal -->
      <div class="modal-body">
        <div class="table-responsive">
            <?php if ($this->fungsi->user_login()->role_id == 1  ) { ?>
          <table id="example2" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Email</th>
              <th>Level</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
              <?php $no = 1;
                foreach ($aktif as $key => $data) {
              ?>
            <tr>
              <td><?= $no++ ?></td>
                  <td><?= $data->nama ?></td>
                  <td><?= $data->email ?></td>
                  <td><?= $data->role ?></td>
                  <td>
                    <form action="<?= site_url('user/aktivasi') ?>" method="post">
                    <input type="hidden" name="status" value="1">
                    <input type="hidden" name="id" value="<?= $data->id ?>">
                    <button onclick="return confirm('Apakah anda yakin aktivasi user ini?')" class="btn btn-danger btn-sm text-white">
                    Aktivasi
                    </button>
                  </form>
                </td>
            </tr>
              <?php
              } ?>     
            </tbody>
            <?php } ?>
          </table>
          <?php if ($this->fungsi->user_login()->role_id == 2  ) { ?>
          <table id="example3" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Email</th>
              <th>Level</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
              <?php $no = 1;
                foreach ($aktifkota as $key => $data) {
              ?>
            <tr>
              <td><?= $no++ ?></td>
                  <td><?= $data->nama ?></td>
                  <td><?= $data->email ?></td>
                  <td><?= $data->role ?></td>
                  <td>
                    <form action="<?= site_url('user/aktivasi') ?>" method="post">
                    <input type="hidden" name="status" value="1">
                    <input type="hidden" name="id" value="<?= $data->id ?>">
                    <button onclick="return confirm('Apakah anda yakin aktivasi user ini?')" class="btn btn-danger btn-sm text-white">
                    Aktivasi
                    </button>
                  </form>
                </td>
            </tr>
              <?php
              } ?>
            
            </tbody>
            <?php } ?>
          </table>
        </div>
              <!-- /.card-body -->
      </div>      
    </div>
  </div>
</div>
<!-- jQuery -->
<script src="<?= base_url('assets/plugins/jquery/jquery.min.js')?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?= base_url('assets/plugins/jquery-ui/jquery-ui.min.js')?>"></script>
<?php echo $contents ?>
<!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2021 <a href="https://dkp.jatengprov.go.id">All Rights Reserved by Dinas Kelautan dan Perikanan Provinsi Jawa Tengah</a>.</strong>
    <div class="float-right d-none d-sm-inline-block">
      <b>Version:</b>1.0
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?= base_url('assets/plugins/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
<!-- ChartJS -->
<script src="<?= base_url('assets/plugins/chart.js/Chart.min.js')?>"></script>
<!-- Sparkline -->
<script src="<?= base_url('assets/plugins/sparklines/sparkline.js')?>"></script>
<!-- JQVMap -->
<!-- <script src="<?= base_url('assets/plugins/jqvmap/jquery.vmap.min.js')?>"></script>
<script src="<?= base_url('assets/plugins/jqvmap/maps/jquery.vmap.usa.js')?>"></script> -->
<!-- jQuery Knob Chart -->
<script src="<?= base_url('assets/plugins/jquery-knob/jquery.knob.min.js')?>"></script>
<!-- daterangepicker -->
<script src="<?= base_url('assets/plugins/moment/moment.min.js')?>"></script>
<script src="<?= base_url('assets/plugins/daterangepicker/daterangepicker.js')?>"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?= base_url('assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')?>"></script>
<!-- Summernote -->
<script src="<?= base_url('assets/plugins/summernote/summernote-bs4.min.js')?>"></script>
<!-- overlayScrollbars -->
<script src="<?= base_url('assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')?>"></script>
<!-- datatables -->
<script src="<?= base_url('assets/plugins/datatables/jquery.dataTables.min.js')?>"></script>
<script src="<?= base_url('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')?>"></script>
<script src="<?= base_url('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js')?>"></script>
<script src="<?= base_url('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')?>"></script>
<script src="<?= base_url('assets/plugins/datatables-buttons/js/dataTables.buttons.min.js')?>"></script>
<script src="<?= base_url('assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js')?>"></script>
<script src="<?= base_url('assets/plugins/jszip/jszip.min.js')?>"></script>
<script src="<?= base_url('assets/plugins/pdfmake/pdfmake.min.js')?>"></script>
<script src="<?= base_url('assets/plugins/pdfmake/vfs_fonts.js')?>"></script>
<script src="<?= base_url('assets/plugins/datatables-buttons/js/buttons.html5.min.js')?>"></script>
<script src="<?= base_url('assets/plugins/datatables-buttons/js/buttons.print.min.js')?>"></script>
<script src="<?= base_url('assets/plugins/datatables-buttons/js/buttons.colVis.min.js')?>"></script>
<!-- jquery-validation -->
<script src="<?= base_url('assets/plugins/jquery-validation/jquery.validate.min.js')?>"></script>
<script src="<?= base_url('assets/plugins/jquery-validation/additional-methods.min.js')?>"></script>
<!-- AdminLTE App -->
<script src="<?= base_url('assets/dist/js/adminlte.js')?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url('assets/dist/js/demo.js')?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?= base_url('assets/dist/js/pages/dashboard.js')?>"></script>
<!-- Page specific script -->

<script>
        $(document).ready(function() { // Ketika halaman sudah siap (sudah selesai di load)
            // Kita sembunyikan dulu untuk loadingnya
            $("#loading").hide();

            $("#kota").change(function() { // Ketika user mengganti atau memilih data provinsi
                $("#kecamatan").hide(); // Sembunyikan dulu combobox kota nya
		// $("#desa").hide();
                $("#loading").show();
                // Tampilkan loadingnya

                $.ajax({
                    type: "POST", // Method pengiriman data bisa dengan GET atau POST
                    url: "<?php echo base_url("gudang/list_sub"); ?>",
                    // Isi dengan url/path file php yang dituju
                    data: {
                        id: $("#kota").val()
                    }, // data yang akan dikirim ke file yang dituju
                    dataType: "json",
                    beforeSend: function(e) {
                        if (e && e.overrideMimeType) {
                            e.overrideMimeType("application/json;charset=UTF-8");
                        }
                    },
                    success: function(response) { // Ketika proses pengiriman berhasil
                        $("#loading").hide();
                        // Sembunyikan loadingnya

                        // set isi dari combobox kota
                        // lalu munculkan kembali combobox kotanya
                        $("#kecamatan").html(response.list_sub).show();

                    },
                    error: function(xhr, ajaxOptions, thrownError) { // Ketika ada error
                        alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
                    }
                });

            });
        });
</script>
<script>
    $(document).ready(function() { // Ketika halaman sudah siap (sudah selesai di load)
        // Kita sembunyikan dulu untuk loadingnya
        $("#loading2").hide();

        $("#kecamatan").change(function() { // Ketika user mengganti atau memilih data provinsi
            $("#desa").hide(); // Sembunyikan dulu combobox kota nya
            $("#loading2").show(); // Tampilkan loadingnya

            $.ajax({
                type: "POST", // Method pengiriman data bisa dengan GET atau POST
                url: "<?php echo base_url("gudang/list_desa"); ?>", // Isi dengan url/path file php yang dituju
                data: {
                    id: $("#kecamatan").val()
                }, // data yang akan dikirim ke file yang dituju
                dataType: "json",
                beforeSend: function(e) {
                    if (e && e.overrideMimeType) {
                        e.overrideMimeType("application/json;charset=UTF-8");
                    }
                },
                success: function(response) { // Ketika proses pengiriman berhasil
                    $("#loading2").hide(); // Sembunyikan loadingnya

                    // set isi dari combobox kota
                    // lalu munculkan kembali combobox kotanya
                    $("#desa").html(response.list_desa).show();
                },
                error: function(xhr, ajaxOptions, thrownError) { // Ketika ada error
                    alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
                }
            });
        });
    });
</script>
<script>
    $(document).ready(function() { // Ketika halaman sudah siap (sudah selesai di load)
        // Kita sembunyikan dulu untuk loadingnya
        $("#loading3").hide();

        $("#desa").change(function() { // Ketika user mengganti atau memilih data provinsi
            $("#koperasi").hide(); // Sembunyikan dulu combobox kota nya
            $("#loading3").show(); // Tampilkan loadingnya

            $.ajax({
                type: "POST", // Method pengiriman data bisa dengan GET atau POST
                url: "<?php echo base_url("gudang/list_koperasi"); ?>", // Isi dengan url/path file php yang dituju
                data: {
                    id: $("#desa").val()
                }, // data yang akan dikirim ke file yang dituju
                dataType: "json",
                beforeSend: function(e) {
                    if (e && e.overrideMimeType) {
                        e.overrideMimeType("application/json;charset=UTF-8");
                    }
                },
                success: function(response) { // Ketika proses pengiriman berhasil
                    $("#loading3").hide(); // Sembunyikan loadingnya

                    // set isi dari combobox kota
                    // lalu munculkan kembali combobox kotanya
                    $("#koperasi").html(response.list_koperasi).show();
                },
                error: function(xhr, ajaxOptions, thrownError) { // Ketika ada error
                    alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
                }
            });
        });
    });
</script>
<script>
    $(document).ready(function() { // Ketika halaman sudah siap (sudah selesai di load)
        // Kita sembunyikan dulu untuk loadingnya
        $("#loading4").hide();

        $("#koperasi").change(function() { // Ketika user mengganti atau memilih data provinsi
            $("#kelompok").hide(); // Sembunyikan dulu combobox kota nya
            $("#loading4").show(); // Tampilkan loadingnya

            $.ajax({
                type: "POST", // Method pengiriman data bisa dengan GET atau POST
                url: "<?php echo base_url("gudang/list_kelompok"); ?>", // Isi dengan url/path file php yang dituju
                data: {
                    id: $("#koperasi").val()
                }, // data yang akan dikirim ke file yang dituju
                dataType: "json",
                beforeSend: function(e) {
                    if (e && e.overrideMimeType) {
                        e.overrideMimeType("application/json;charset=UTF-8");
                    }
                },
                success: function(response) { // Ketika proses pengiriman berhasil
                    $("#loading4").hide(); // Sembunyikan loadingnya

                    // set isi dari combobox kota
                    // lalu munculkan kembali combobox kotanya
                    $("#kelompok").html(response.list_kelompok).show();
                },
                error: function(xhr, ajaxOptions, thrownError) { // Ketika ada error
                    alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
                }
            });
        });
    });
</script>
<script>
$(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      alert( "Form successful submitted!" );
    }
  });
  $('#quickForm').validate({
    rules: {
      email: {
        required: true,
        email: true,
      },
      password: {
        required: true,
        minlength: 5
      },
      terms: {
        required: true
      },
    },
    messages: {
      email: {
        required: "Please enter a email address",
        email: "Please enter a vaild email address"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      terms: "Please accept our terms"
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>
<script>
  $(function () {
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */

    //--------------
    //- AREA CHART -
    //--------------

    // Get context with jQuery - using jQuery's .get() method.
    var areaChartCanvas = $('#areaChart').get(0).getContext('2d')

    var areaChartData = {
      labels  : ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
      datasets: [
        {
          label               : 'Digital Goods',
          backgroundColor     : 'rgba(60,141,188,0.9)',
          borderColor         : 'rgba(60,141,188,0.8)',
          pointRadius          : false,
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : [28, 48, 40, 19, 86, 27, 90]
        },
        {
          label               : 'Electronics',
          backgroundColor     : 'rgba(210, 214, 222, 1)',
          borderColor         : 'rgba(210, 214, 222, 1)',
          pointRadius         : false,
          pointColor          : 'rgba(210, 214, 222, 1)',
          pointStrokeColor    : '#c1c7d1',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(220,220,220,1)',
          data                : [65, 59, 80, 81, 56, 55, 40]
        },
      ]
    }

    var areaChartOptions = {
      maintainAspectRatio : false,
      responsive : true,
      legend: {
        display: false
      },
      scales: {
        xAxes: [{
          gridLines : {
            display : false,
          }
        }],
        yAxes: [{
          gridLines : {
            display : false,
          }
        }]
      }
    }

    // This will get the first returned node in the jQuery collection.
    new Chart(areaChartCanvas, {
      type: 'line',
      data: areaChartData,
      options: areaChartOptions
    })

    //-------------
    //- LINE CHART -
    //--------------
    var lineChartCanvas = $('#lineChart').get(0).getContext('2d')
    var lineChartOptions = $.extend(true, {}, areaChartOptions)
    var lineChartData = $.extend(true, {}, areaChartData)
    lineChartData.datasets[0].fill = false;
    lineChartData.datasets[1].fill = false;
    lineChartOptions.datasetFill = false

    var lineChart = new Chart(lineChartCanvas, {
      type: 'line',
      data: lineChartData,
      options: lineChartOptions
    })

    //-------------
    //- DONUT CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var donutChartCanvas = $('#donutChart').get(0).getContext('2d')
    var donutData        = {
      labels: [
          'Chrome',
          'IE',
          'FireFox',
          'Safari',
          'Opera',
          'Navigator',
      ],
      datasets: [
        {
          data: [700,500,400,600,300,100],
          backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
        }
      ]
    }
    var donutOptions     = {
      maintainAspectRatio : false,
      responsive : true,
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    new Chart(donutChartCanvas, {
      type: 'doughnut',
      data: donutData,
      options: donutOptions
    })

    //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
    var pieData        = donutData;
    var pieOptions     = {
      maintainAspectRatio : false,
      responsive : true,
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    new Chart(pieChartCanvas, {
      type: 'pie',
      data: pieData,
      options: pieOptions
    })

    //-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas = $('#barChart').get(0).getContext('2d')
    var barChartData = $.extend(true, {}, areaChartData)
    var temp0 = areaChartData.datasets[0]
    var temp1 = areaChartData.datasets[1]
    barChartData.datasets[0] = temp1
    barChartData.datasets[1] = temp0

    var barChartOptions = {
      responsive              : true,
      maintainAspectRatio     : false,
      datasetFill             : false
    }

    new Chart(barChartCanvas, {
      type: 'bar',
      data: barChartData,
      options: barChartOptions
    })

    //---------------------
    //- STACKED BAR CHART -
    //---------------------
    var stackedBarChartCanvas = $('#stackedBarChart').get(0).getContext('2d')
    var stackedBarChartData = $.extend(true, {}, barChartData)

    var stackedBarChartOptions = {
      responsive              : true,
      maintainAspectRatio     : false,
      scales: {
        xAxes: [{
          stacked: true,
        }],
        yAxes: [{
          stacked: true
        }]
      }
    }

    new Chart(stackedBarChartCanvas, {
      type: 'bar',
      data: stackedBarChartData,
      options: stackedBarChartOptions
    })
  })
</script>
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, 
      "lengthChange": false, 
      "autoWidth": false,
      "buttons": ["csv", "excel", "pdf"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
    $('#example3').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
<script>
$("#admin").change(function() {
  if ($(this).val() == "1") {
    $('#otherFieldDiv').hide();
    $('#otherField').removeAttr('required');
    $('#otherField').removeAttr('data-error');
  } else {
    $('#otherFieldDiv').show();
    $('#otherField').attr('required', '');
    $('#otherField').attr('data-error', 'This field is required.');
  }
});
$("#admin").trigger("change");
</script>
<script>

  /** add active class and stay opened when selected */
var url = window.location;

// for sidebar menu entirely but not cover treeview
$('ul.nav-sidebar a').filter(function() {
    return this.href == url;
}).addClass('active');

// for treeview
$('ul.nav-treeview a').filter(function() {
    return this.href == url;
}).parentsUntil(".nav-sidebar > .nav-treeview").addClass('menu-open').prev('a').addClass('active');
</script>

<script>
    $(document).ready(function() { // Ketika halaman sudah siap (sudah selesai di load)
        // Kita sembunyikan dulu untuk loadingnya
        $("#loading2").hide();

        $("#desa").change(function() { // Ketika user mengganti atau memilih data provinsi

            // $.ajax({
            //     type: "POST", // Method pengiriman data bisa dengan GET atau POST
            //     url: "<?php echo base_url("gudang/list_desa"); ?>", // Isi dengan url/path file php yang dituju
            //     data: {
            //         id: $("#kecamatan").val()
            //     }, // data yang akan dikirim ke file yang dituju
            //     dataType: "json",
            //     beforeSend: function(e) {
            //         if (e && e.overrideMimeType) {
            //             e.overrideMimeType("application/json;charset=UTF-8");
            //         }
            //     },
            //     success: function(response) { // Ketika proses pengiriman berhasil
            //         $("#loading2").hide(); // Sembunyikan loadingnya

            //         // set isi dari combobox kota
            //         // lalu munculkan kembali combobox kotanya
            //         $("#des").html(response.list_desa).show();
            //     },
            //     error: function(xhr, ajaxOptions, thrownError) { // Ketika ada error
            //         alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
            //     }
            // });
            $.ajax({
                    type : "POST",
                    url  : "<?php echo base_url('lahan/get_latlong')?>",
                    dataType : "JSON",
                    data : {id: $("#desa").val()
                    },
                    cache:false,
                    success: function(data){
                        $.each(data,function(id,name, lat, lng){
                            $('[name="latitude"]').val(data.lat);
                            $('[name="longitude"]').val(data.lng);
                             
                        });
                         
                    },
                    error: function(xhr, ajaxOptions, thrownError) { // Ketika ada error
                    alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error  
                  }
                });
                return false;
        });
    });
</script>

</body>
</html>