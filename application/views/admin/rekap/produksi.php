<?php if ($this->fungsi->user_login()->role_id == 1) { ?>
  <?php
    /* Mengambil query report*/
    foreach($grafik_produksi as $produksi){
		    $nama[]   = $produksi->name;
        $kp1_li[]      = (float)$produksi->kp1_lahan_integrasi;
        $kp2_li[]      = (float)$produksi->kp2_lahan_integrasi;
        $kp3_li[]      = (float)$produksi->kp3_lahan_integrasi;
        $kp1_ln[]      = (float)$produksi->kp1_lahan_nonintegrasi;
        $kp2_ln[]      = (float)$produksi->kp2_lahan_nonintegrasi;
        $kp3_ln[]      = (float)$produksi->kp3_lahan_nonintegrasi;
        $kp1_gn[]      = (float)$produksi->kp1_garam_nontambak;
        $kp2_gn[]      = (float)$produksi->kp2_garam_nontambak;
        $kp3_gn[]      = (float)$produksi->kp3_garam_nontambak;
    }
	/* end mengambil query*/
?>
<?php } ?>

<?php if ($this->fungsi->user_login()->role_id == 2) { ?>
  <?php
    /* Mengambil query report*/
    foreach($grafik_produksi_kota as $produksi_kota){
		    $nama_kota[]   = $produksi_kota->name;
        $kp1_li_kota[]      = (float)$produksi_kota->kp1_lahan_integrasi;
        $kp2_li_kota[]      = (float)$produksi_kota->kp2_lahan_integrasi;
        $kp3_li_kota[]      = (float)$produksi_kota->kp3_lahan_integrasi;
        $kp1_ln_kota[]      = (float)$produksi_kota->kp1_lahan_nonintegrasi;
        $kp2_ln_kota[]      = (float)$produksi_kota->kp2_lahan_nonintegrasi;
        $kp3_ln_kota[]      = (float)$produksi_kota->kp3_lahan_nonintegrasi;
        $kp1_gn_kota[]      = (float)$produksi_kota->kp1_garam_nontambak;
        $kp2_gn_kota[]      = (float)$produksi_kota->kp2_garam_nontambak;
        $kp3_gn_kota[]      = (float)$produksi_kota->kp3_garam_nontambak;
    }
	/* end mengambil query*/
?>
<?php } ?>

<style>
#container {
  height: 500px; 
}

.highcharts-figure, .highcharts-data-table table {
  min-width: 310px; 
  max-width: 800px;
  margin: 1em auto;
}

.highcharts-data-table table {
  font-family: Verdana, sans-serif;
  border-collapse: collapse;
  border: 1px solid #EBEBEB;
  margin: 10px auto;
  text-align: center;
  width: 100%;
  max-width: 500px;
}
.highcharts-data-table caption {
  padding: 1em 0;
  font-size: 1.2em;
  color: #555;
}
.highcharts-data-table th {
  font-weight: 600;
  padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
  padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
  background: #f8f8f8;
}
.highcharts-data-table tr:hover {
  background: #f1f7ff;
}
</style>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Rekap Produksi Garam</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Produksi</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- =========================================================== -->
      <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
              <div class="card-header p-0 pt-1">
                <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true">Grafik</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#custom-tabs-one-profile" role="tab" aria-controls="custom-tabs-one-profile" aria-selected="false">Download</a>
                  </li>
                </ul>
              </div>
              <div class="card-body">
                <div class="tab-content" id="custom-tabs-three-tabContent">
                  <div class="tab-pane fade show active" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-three-home-tab">
                    <div class="chart">
                      <?php if ($this->fungsi->user_login()->role_id == 1) { ?>
                      <div id="produksi"></div>
                      <?php }?> 
                      <?php if ($this->fungsi->user_login()->role_id == 2) { ?>
                      <div id="produksi_kota"></div>
                      <?php }?>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="custom-tabs-one-profile" role="tabpanel" aria-labelledby="custom-tabs-three-profile-tab">
                     <div class="table-responsive">
                    <?php if ($this->fungsi->user_login()->role_id == 1) { ?>
                      
                    <table id="example1" class="table table-bordered table-striped" style="width: 100%;">
                    <thead>
                    <tr>
                         <th>No</th>
                        <th>Kelurahan/Desa</th>
                        <th>KP1 Produksi Integrasi (Kg)</th>
                        <th>KP2 Produksi Integrasi (Kg)</th>
                        <th>KP3 Produksi Integrasi (Kg)</th>
                        <th>KP1 Produksi Nonintegrasi (Kg)</th>
                        <th>KP2 Produksi Nonintegrasi (Kg)</th>
                        <th>KP3 Produksi Nonintegrasi (Kg)</th>
                        <th>KP1 Produksi Nontambak (Kg)</th>
                        <th>KP2 Produksi Nontambak (Kg)</th>
                        <th>KP3 Produksi Nontambak (Kg)</th>
                    </tr>
                    </thead> 
                    <tbody>
                    <?php $no = 1;
                      foreach ($grafik_produksi as $key => $data) {
                    ?>
                      <tr>
                        <td><?= $no++ ?></td>
                        <td><?= $data->name?></td>
                        <td><?= $data->kp1_lahan_integrasi?></td>
                        <td><?= $data->kp2_lahan_integrasi?></td>
                        <td><?= $data->kp3_lahan_integrasi?></td>
                        <td><?= $data->kp1_lahan_nonintegrasi?></td>
                        <td><?= $data->kp2_lahan_nonintegrasi?></td>
                        <td><?= $data->kp3_lahan_nonintegrasi?></td>
                        <td><?= $data->kp1_garam_nontambak?></td>
                        <td><?= $data->kp2_garam_nontambak?></td>
                        <td><?= $data->kp3_garam_nontambak?></td>
                      </tr>
                    <?php
                    } ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Kelurahan/Desa</th>
                        <th>KP1 Produksi Integrasi (Kg)</th>
                        <th>KP2 Produksi Integrasi (Kg)</th>
                        <th>KP3 Produksi Integrasi (Kg)</th>
                        <th>KP1 Produksi Nonintegrasi (Kg)</th>
                        <th>KP2 Produksi Nonintegrasi (Kg)</th>
                        <th>KP3 Produksi Nonintegrasi (Kg)</th>
                        <th>KP1 Produksi Nontambak (Kg)</th>
                        <th>KP2 Produksi Nontambak (Kg)</th>
                        <th>KP3 Produksi Nontambak (Kg)</th>
                    </tr>
                    </tfoot>
                    </table>
                    <?php } ?>
                    <?php if ($this->fungsi->user_login()->role_id == 2) { ?>
                    <table id="example1" class="table table-bordered table-striped" style="width: 100%;">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Kelurahan/Desa</th>
                        <th>KP1 Produksi Integrasi (Kg)</th>
                        <th>KP2 Produksi Integrasi (Kg)</th>
                        <th>KP3 Produksi Integrasi (Kg)</th>
                        <th>KP1 Produksi Nonintegrasi (Kg)</th>
                        <th>KP2 Produksi Nonintegrasi (Kg)</th>
                        <th>KP3 Produksi Nonintegrasi (Kg)</th>
                        <th>KP1 Produksi Nontambak (Kg)</th>
                        <th>KP2 Produksi Nontambak (Kg)</th>
                        <th>KP3 Produksi Nontambak (Kg)</th>
                    </tr>
                    </thead> 
                    <tbody>
                    <?php $no = 1;
                      foreach ($grafik_produksi_kota as $key => $data) {
                    ?>
                      <tr>
                        <td><?= $no++ ?></td>
                        <td><?= $data->name?></td>
                        <td><?= $data->kp1_lahan_integrasi?></td>
                        <td><?= $data->kp2_lahan_integrasi?></td>
                        <td><?= $data->kp3_lahan_integrasi?></td>
                        <td><?= $data->kp1_lahan_nonintegrasi?></td>
                        <td><?= $data->kp2_lahan_nonintegrasi?></td>
                        <td><?= $data->kp3_lahan_nonintegrasi?></td>
                        <td><?= $data->kp1_garam_nontambak?></td>
                        <td><?= $data->kp2_garam_nontambak?></td>
                        <td><?= $data->kp3_garam_nontambak?></td>
                      </tr>
                    <?php
                    } ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Kelurahan/Desa</th>
                        <th>KP1 Produksi Integrasi (Kg)</th>
                        <th>KP2 Produksi Integrasi (Kg)</th>
                        <th>KP3 Produksi Integrasi (Kg)</th>
                        <th>KP1 Produksi Nonintegrasi (Kg)</th>
                        <th>KP2 Produksi Nonintegrasi (Kg)</th>
                        <th>KP3 Produksi Nonintegrasi (Kg)</th>
                        <th>KP1 Produksi Nontambak (Kg)</th>
                        <th>KP2 Produksi Nontambak (Kg)</th>
                        <th>KP3 Produksi Nontambak (Kg)</th>
                    </tr>
                    </tfoot>
                    </table>
                    <?php } ?>
                </div>
                  </div>
                </div>
              </div>
              <!-- /.card -->
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<!-- Grafik produksi -->
<script>
  Highcharts.chart('produksi', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'Grafik Produksi Garam Provinsi Jawa Tengah'
  },
  subtitle: {
    text: 'Source: <a href="http://dkp.jatengprov.gp.id">Dinas Kelautan dan Perikanan Provinsi Jawa Tengah</a>'
  },
  xAxis: {
    categories: <?=json_encode($nama);?>,
    crosshair: true
  },
  yAxis: {
    min: 0,
    title: {
      text: 'Produksi Garam (Kg)'
    }
  },
  tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.1f} Kg</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.2,
      borderWidth: 0
    }
  },
  series: [{
    name: 'KP1 Lahan Integrasi',
    
    data: <?=json_encode($kp1_li);?>

  },{
  name: 'KP2 Lahan Integrasi',
    
    data: <?=json_encode($kp2_li);?>

  },{
  name: 'KP3 Lahan Integrasi',
    
    data: <?=json_encode($kp3_li);?>

  }, {
    name: 'KP1 Lahan Nonintegrasi',
    data: <?=json_encode($kp1_ln);?>

  }, {
    name: 'KP2 Lahan Nonintegrasi',
    data: <?=json_encode($kp2_ln);?>

  }, {
    name: 'KP3 Lahan Nonintegrasi',
    data: <?=json_encode($kp3_ln);?>

  }
]
});
</script>
<!-- end grafik produksi -->

<!-- Grafik produksi Kota-->
<script>
  Highcharts.chart('produksi_kota', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'Grafik Produksi Garam '
  },
  subtitle: {
    text: 'Source: <a href="http://dkp.jatengprov.gp.id">Dinas Kelautan dan Perikanan Provinsi Jawa Tengah</a>'
  },
  xAxis: {
    categories: <?=json_encode($nama_kota);?>,
    crosshair: true
  },
  yAxis: {
    min: 0,
    title: {
      text: 'Produksi Garam (Kg)'
    }
  },
  tooltip: {
    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
      '<td style="padding:0"><b>{point.y:.1f} Kg</b></td></tr>',
    footerFormat: '</table>',
    shared: true,
    useHTML: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.2,
      borderWidth: 0
    }
  },
  series: [{
    name: 'KP1 Lahan Integrasi',
    
    data: <?=json_encode($kp1_li_kota);?>

  },{
  name: 'KP2 Lahan Integrasi',
    
    data: <?=json_encode($kp2_li_kota);?>

  },{
  name: 'KP3 Lahan Integrasi',
    
    data: <?=json_encode($kp3_li_kota);?>

  }, {
    name: 'KP1 Lahan Nonintegrasi',
    data: <?=json_encode($kp1_ln_kota);?>

  }, {
    name: 'KP2 Lahan Nonintegrasi',
    data: <?=json_encode($kp2_ln_kota);?>

  }, {
    name: 'KP3 Lahan Nonintegrasi',
    data: <?=json_encode($kp3_ln_kota);?>

  }
]
});
</script>
<!-- end grafik produksi Kota-->