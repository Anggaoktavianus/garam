 <style>
#container {
  height: 500px; 
}

.highcharts-figure, .highcharts-data-table table {
  min-width: 310px; 
  max-width: 800px;
  margin: 1em auto;
}

.highcharts-data-table table {
  font-family: Verdana, sans-serif;
  border-collapse: collapse;
  border: 1px solid #EBEBEB;
  margin: 10px auto;
  text-align: center;
  width: 100%;
  max-width: 500px;
}
.highcharts-data-table caption {
  padding: 1em 0;
  font-size: 1.2em;
  color: #555;
}
.highcharts-data-table th {
  font-weight: 600;
  padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
  padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
  background: #f8f8f8;
}
.highcharts-data-table tr:hover {
  background: #f1f7ff;
}
</style>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Rekap Koperasi Garam</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Koperasi</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- =========================================================== -->
      <div class="row">
          <div class="col-md-12">
            <!-- AREA CHART -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Rekap Koperasi Garam</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
              <div class="card-body">
                <div class="chart">
                  <div id="container"></div>
                  <!-- <canvas id="areaChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas> -->
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
	  </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script>
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Grafik Koperasi Garam Provinsi Jawa Tengah'
    },
    subtitle: {
        text: 'Source: <a href="http://dkp.jatengprov.gp.id">Dinas Kelautan dan Perikanan Provinsi Jawa Tengah</a>'
    },
    xAxis: {
        type: 'category',
        labels: {
            rotation: -45,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: ' Koperasi (Ha)'
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: ' Koperasi: <b>{point.y:.1f} Ha</b>'
    },
    series: [{
        name: ' Koperasi',
        data: [
            ['Kabupaten Cilacap', 24.2],
            ['Kabupaten Banyumas', 20.8],
            ['Kabupaten Purbalingga', 14.9],
            ['Kabupaten Banjarnegara', 13.7],
            ['Kabupaten Kebumen', 13.1],
            ['Kabupaten Purworejo', 12.7],
            ['Kabupaten Wonosobo', 12.4],
            ['Kabupaten Magelang', 12.2],
            ['Kabupaten Boyolali', 12.0],
            ['Kabupaten Klaten', 11.7],
            ['Kabupaten Sukoharjo', 11.5],
            ['Kabupaten Wonogiri', 11.2],
            ['Kabupaten Karanganyar', 11.1],
            ['Kabupaten Sragen', 10.6],
            ['Kabupaten Grobogan', 10.6],
            ['Kabupaten Blora', 10.6],
            ['Kabupaten Rembang', 10.3],
            ['Kabupaten Pati', 9.8],
            ['Kabupaten Kudus', 9.3],
            ['Kabupaten Jepara', 9.3]
        ],
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:.1f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    }]
});
</script>