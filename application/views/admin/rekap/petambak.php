 <style>
#container {
  height: 500px; 
}

.highcharts-figure, .highcharts-data-table table {
  min-width: 310px; 
  max-width: 800px;
  margin: 1em auto;
}

.highcharts-data-table table {
  font-family: Verdana, sans-serif;
  border-collapse: collapse;
  border: 1px solid #EBEBEB;
  margin: 10px auto;
  text-align: center;
  width: 100%;
  max-width: 500px;
}
.highcharts-data-table caption {
  padding: 1em 0;
  font-size: 1.2em;
  color: #555;
}
.highcharts-data-table th {
  font-weight: 600;
  padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
  padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
  background: #f8f8f8;
}
.highcharts-data-table tr:hover {
  background: #f1f7ff;
}
</style>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Rekap Petambak Garam</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Petambak</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- =========================================================== -->
      <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-primary card-tabs">
              <div class="card-header p-0 pt-1">
                <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true">Grafik</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#custom-tabs-one-profile" role="tab" aria-controls="custom-tabs-one-profile" aria-selected="false">Download</a>
                  </li>
                </ul>
              </div>
              <div class="card-body">
                <div class="tab-content" id="custom-tabs-three-tabContent">
                  <div class="tab-pane fade show active" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-three-home-tab">
                    <div class="chart">
                      <?php if ($this->fungsi->user_login()->role_id == 1) { ?>
                      <div id="petambak"></div>
                      <?php } ?>
                      <?php if ($this->fungsi->user_login()->role_id == 2) { ?>
                      <div id="petambak_kota"></div>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="custom-tabs-one-profile" role="tabpanel" aria-labelledby="custom-tabs-three-profile-tab">
                     <div class="table-responsive">
                    <?php if ($this->fungsi->user_login()->role_id == 1) { ?>
                    <table id="example1" class="table table-bordered table-striped" style="width: 100%;">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Kelurahan/Desa</th>
                        <th>Jumlah Petambak</th>
                    </tr>
                    </thead> 
                    <tbody>
                    <?php $no = 1;
                      foreach ($grafik_petambak as $key => $data) {
                    ?>
                      <tr>
                        <td><?= $no++ ?></td>
                        <td><?= $data->nama?></td>
                        <td><?= $data->jum?></td>
                      </tr>
                    <?php
                    } ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Kelurahan/Desa</th>
                        <th>Jumlah Petambak</th>
                    </tr>
                    </tfoot>
                    </table>
                    <?php } ?>
                    <?php if ($this->fungsi->user_login()->role_id == 2) { ?>
                    <table id="example1" class="table table-bordered table-striped" style="width: 100%;">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Kelurahan/Desa</th>
                        <th>Jumlah Petambak</th>
                    </tr>
                    </thead> 
                    <tbody>
                    <?php $no = 1;
                      foreach ($grafik_petambak_kota as $key => $data) {
                    ?>
                      <tr>
                        <td><?= $no++ ?></td>
                        <td><?= $data->nama?></td>
                        <td><?= $data->jum?></td>
                      </tr>
                    <?php
                    } ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Kelurahan/Desa</th>
                        <th>Jumlah Petambak</th>
                    </tr>
                    </tfoot>
                    </table>
                    <?php } ?>
                  </div>
                  </div>
                </div>
              </div>
              <!-- /.card -->
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<!-- Grafik Petambak -->
<script>
  Highcharts.chart('petambak', {
  chart: {
    plotBackgroundColor: null,
    plotBorderWidth: null,
    plotShadow: false,
    type: 'pie'
  },
  title: {
    text: 'Grafik Petambak Garam'
  },
  tooltip: {
    // pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b> <br>'+'{series.name}'
    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}:<b>{point.percentage:.1f}%</b></td></br>' +
      'Jumlah :<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
    
  },
  accessibility: {
    point: {
      valueSuffix: '%'
    }
  },
  plotOptions: {
    pie: {
      allowPointSelect: true,
      cursor: 'pointer',
      dataLabels: {
        enabled: true,
        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
      }
    }
  },
  series: [{
    name: 'Rasio',
    colorByPoint: true,
    data: [
					<?php 
					// data yang diambil dari database
					if($grafik_petambak)
					{
					   foreach ($grafik_petambak as $stk) {
					   echo "['" .$stk->nama . "'," .(float)$stk->jum."],\n";
					   }
					}
					?>
			]
  }]
});
</script>
<!-- End Grafik Petambak -->
<!-- Grafik Petambak -->
<script>
  Highcharts.chart('petambak_kota', {
  chart: {
    plotBackgroundColor: null,
    plotBorderWidth: null,
    plotShadow: false,
    type: 'pie'
  },
  title: {
    text: 'Grafik Petambak Garam Provinsi Jawa Tengah'
  },
  tooltip: {
    // pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b> <br>'+'{series.name}'
    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}:<b>{point.percentage:.1f}%</b></td></br>' +
      'Petembak<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
    
  },
  accessibility: {
    point: {
      valueSuffix: '%'
    }
  },
  plotOptions: {
    pie: {
      allowPointSelect: true,
      cursor: 'pointer',
      dataLabels: {
        enabled: true,
        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
      }
    }
  },
  series: [{
    name: 'Jumlah',
    colorByPoint: true,
    data: [
					<?php 
					// data yang diambil dari database
					if(count($grafik_petambak_kota)>0)
					{
					   foreach ($grafik_petambak_kota as $stk) {
					   echo "['" .$stk->nama . "'," . (float) $stk->jum ."],\n";
					   }
					}
					?>
			]
  }]
});
</script>
<!-- End Grafik Petambak -->