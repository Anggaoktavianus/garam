<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Tambah Penerima Hibah</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Tambah</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Form Tambah Data Hibah </h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="<?= site_url('hibah/add')?>" method="POST">
                <div class="card-body">
                  <p style="color:red;"><small>*Penulisan angka desimal menggunakan titik (.) Contoh : 10.00</small></p>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group"> 
                        <label for="exampleInputEmail1">Nama Kelompok</label>
                        <input type="text" name="kelompok" class="form-control" value="<?= set_value('kelompok') ?>" placeholder="Masukkan kelompok" required>
                          <?= form_error('kelompok', '<div class="text-danger"><small>', '</small></div>') ?>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Ketua</label>
                        <input type="text" name="ketua" class="form-control" value="<?= set_value('ketua') ?>" id="exampleInputEmail1" placeholder="Masukkan nama ketua" required>
                        <?= form_error('ketua', '<div class="text-danger"><small>', '</small></div>') ?>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword2">Telepon</label>
                        <input type="number" name="telepon" class="form-control" value="<?= set_value('telepon') ?>" id="exampleInputPassword2" placeholder="Masukkan nomor" required>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword2">Jenis Hibah</label>
                        <input type="text" name="jenis_hibah" class="form-control" value="<?= set_value('jenis_hibah') ?>" id="exampleInputPassword2" placeholder="Masukkan jenis hibah" required>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword2">Sumber Pendanaan</label>
                        <input type="text" name="sumberdana" class="form-control" value="<?= set_value('sumberdana') ?>" id="exampleInputPassword2" placeholder="Sumber Pendanaan" required>
                      </div> 
                      <div class="form-group">
                          <label>Alamat</label>
                          <textarea class="form-control" name="alamat" value="" rows="5" placeholder="Masukkan alamat ..."><?= set_value('alamat') ?></textarea>
                      </div>
                    </div>
                    <div class="col-md-6">
                       <div class="form-group">
                        <label for="exampleInputPassword2">Anggaran (Rp)</label>
                        <input type="number" name="anggaran" class="form-control" value="<?= set_value('anggaran') ?>" id="exampleInputPassword2" placeholder="Jumlah anggaran" required>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword2">Volume</label>
                        <input type="text" name="volume" class="form-control" value="<?= set_value('volume') ?>" id="exampleInputPassword2" placeholder="Jumlah volume" required>
                      </div> 
                      <div class="form-group">
                        <label for="exampleInputPassword2">Satuan</label>
                        <input type="text" name="satuan" class="form-control" value="<?= set_value('satuan') ?>" id="exampleInputPassword2" placeholder="Satuan" required>
                      </div> 
                      <div class="form-group">
                        <label for="exampleInputPassword2">Tahun</label>
                         <select name="tahun" class="form-control not-dark" required>
                              <option value="">--Select--</option>
                              <option value="2030">2030</option>
                              <option value="2029">2029</option>
                              <option value="2028">2028</option>
                              <option value="2027">2027</option>
                              <option value="2026">2026</option>
                              <option value="2025">2025</option>
                              <option value="2024">2024</option>
                              <option value="2023">2023</option>
                              <option value="2022">2022</option>
                              <option value="2021">2021</option>
                              <option value="2020">2020</option>
                              <option value="2019">2019</option>
                              <option value="2018">2018</option>
                              <option value="2017">2017</option>
                              <option value="2016">2016</option>
                              <option value="2015">2015</option>
                        </select>
                      </div> 
                      <?php if ($this->fungsi->user_login()->role_id == 1) { ?>
                      <div class="form-group" >
                          <label for="kabkota">Kab/Kota</label>
                          <select name="kabkota_id" class="form-control not-dark" id="kota" required>
                            <option value="">--Select--</option>
                            <?php foreach ($kabkota as $key => $data) { ?>
                            <option value="<?= $data->id ?>" ><?= $data->name ?></option>
                            <?php } ?>
										      </select>
                      </div>
                      <div class="form-group" >
                          <label for="kecamatan_id">Kecamatan</label>
                          <select name="kecamatan_id" class="form-control not-dark" id="kecamatan" required>
												        <option value="">--Select--</option>
											    </select>
                      </div>
                      <?php } ?>
                      <?php if ($this->fungsi->user_login()->role_id == 2) { ?>
                      <input type="hidden" name="kabkota_id"  class="form-control" value="<?= $this->session->userdata('kabkota_id')?>" readonly>
                      <div class="form-group" >
                          <label for="kecamatan_id">Kecamatan</label>
                          <select name="kecamatan_id" class="form-control not-dark" id="kecamatan" required>
												        <option value="">--Select--</option>
                                 <?php foreach ($kecam as $key => $data) { ?>
                                <option value="<?= $data->id ?>" ><?= $data->name ?></option>
                                <?php } ?>
											    </select>
                      </div>
                      
                      <?php } ?>
                      <div class="form-group" id="otherFieldDiv">
                          <label for="desa">Kelurahan</label>
                          <select name="desa" class="form-control not-dark" id="desa" required>
												    <option value="">--Select--</option>
											    </select>
                      </div>
                    </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (left) -->
          <!-- right column -->
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  