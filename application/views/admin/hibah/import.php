<!-- /.content-wrapper -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Import Hibah</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Import Hibah </li>
            </ol>
          </div>
        </div>
        <?php
            $info= $this->session->flashdata('info');
            $pesan= $this->session->flashdata('pesan');
            if($info == 'success'){ ?>
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <i class="icon-gift"></i><strong><?=$info?></strong> <?=$pesan?>.
                </div>
            <?php    
            }elseif($info == 'danger'){ ?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <i class="icon-gift"></i><strong><?=$info?></strong> <?=$pesan?>.	
                </div>
        <?php  }else{ } ?>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
               <div class="card">
                    <div class="card-body">
                        <?= form_open_multipart('hibah/uploaddata') ?>
                        <p style="color:red;"><small>*Pastikan sudah mendownload format import excel yang ada pada tabel dibawah !!</small></p>
                        <div class="form-row">
                            <div class="col-4">
                                <input type="file" class="form-control-file" id="importexcel" name="importexcel" accept=".xlsx,.xls">
                            </div>
                            <div class="col">
                                <button type="submit" class="btn btn-primary btn-sm">Import</button>
                            </div>
                            <div class="col">
                                <?= $this->session->flashdata('pesan'); ?>
                            </div>
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Tabel Import Hibah</h3><br><br>
                    <div class="card-body">
                        <form id="form-filter" class="form-horizontal">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group"> 
                                        <label for="exampleInputEmail1">Kab/Kota</label>
                                        <select name="kota" class="form-control not-dark kotas" id="kota" >
                                            <option value="">--Select--</option>
                                            <?php foreach ($kabkota as $key => $data) { ?>
                                            <option value="<?= $data->id ?>" ><?= $data->name ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword2">Kelurahan</label>
                                        <select name="desa" class="form-control not-dark des" id="desa" >
                                            <option value="">--Select--</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Kecamatan</label>
                                        <select name="kota" class="form-control not-dark keca" id="kecamatan" >
                                            <option value="">--Select--</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </form>   
                    </div>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Penerima Hibah</th>
                            <th>Alamat</th>
                            <th>Ketua</th>
                            <th>Telepon</th>
                            <th>Jenis Hibah</th>
                            <th>Anggaran</th>
                            <th>Volume</th>
                            <th>Satuan</th>
                            <th>Tahun</th>
                            <th>Sumber Dana</th>
                            <th>Desa</th>
                            <th>Kode Desa</th>
                            <th>Kecamatan</th>
                            <th>Kode Kecamatan</th>
                            <th>Kab/Kota</th>
                            <th>Kode Kab/Kota</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>

                    <tfoot>
                        <tr>
                             <th>No</th>
                            <th>Penerima Hibah</th>
                            <th>Alamat</th>
                            <th>Ketua</th>
                            <th>Telepon</th>
                            <th>Jenis Hibah</th>
                            <th>Anggaran</th>
                            <th>Volume</th>
                            <th>Satuan</th>
                            <th>Tahun</th>
                            <th>Sumber Dana</th>
                            <th>Desa</th>
                            <th>Kode Desa</th>
                            <th>Kecamatan</th>
                            <th>Kode Kecamatan</th>
                            <th>Kab/Kota</th>
                            <th>Kode Kab/Kota</th>
                            
                        </tr>
                    </tfoot>
                  </table>
                </div>
                
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<script>
  $(function () {
    $("#example").DataTable({
      "processing": true,
      "searching": true,
      "responsive": false, 
      "lengthChange": true,
      "autoWidth": false,
      "serverSide": true,
      "scrollX": true,
        "fixedColumns":   {
            leftColumns: 1,
            rightColumns: 1
        },
        
      "ajax": {
                "url": "<?= site_url('filter/ajax_list_hibah') ?>",
                "type": "POST"
            },
      
      dom: '<"html5buttons">Bfrtip',
      'lengthMenu' : [
                        [ 10, 25, 50, -1 ],
                        [ '10 rows', '25 rows', '50 rows', 'Show all' ]
                ],   
                                   
      
      // "buttons": ["csv", "excel", "pdfHtml5","pageLength"],
      buttons: [
            
            {
                extend: 'excelHtml5', 
                title: '',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 ]
                }
            },
            // {
            //     extend: 'pdfHtml5',
            //     title: 'Data Import Gudang',
            //     orientation: 'landscape',
            //     pageSize: 'A4',
            //     width: 'auto',
            //     exportOptions: {
                 
            //         columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8],
                    
            //     }
                
            // },
            {
                extend: 'pageLength',
                title: 'Data Import Gudang',
                exportOptions: {
                    columns: ':visible'
                }
            },
        ]
       
    }).buttons().container().appendTo('#example_wrapper .col-md-6:eq(0)');

      
  });
</script>

<script>
    $(document).ready(function() { // Ketika halaman sudah siap (sudah selesai di load)
        // Kita sembunyikan dulu untuk loadingnya
        $("#loading2").hide();

        $("#kecamatan").change(function() { // Ketika user mengganti atau memilih data provinsi
            $("#desa").hide(); // Sembunyikan dulu combobox kota nya
            $("#loading2").show(); // Tampilkan loadingnya

            $.ajax({
                type: "POST", // Method pengiriman data bisa dengan GET atau POST
                url: "<?php echo base_url("gudang/list_desa"); ?>", // Isi dengan url/path file php yang dituju
                data: {
                    id: $("#kecamatan").val()
                }, // data yang akan dikirim ke file yang dituju
                dataType: "json",
                beforeSend: function(e) {
                    if (e && e.overrideMimeType) {
                        e.overrideMimeType("application/json;charset=UTF-8");
                    }
                },
                success: function(response) { // Ketika proses pengiriman berhasil
                    $("#loading2").hide(); // Sembunyikan loadingnya

                    // set isi dari combobox kota
                    // lalu munculkan kembali combobox kotanya
                    $("#desa").html(response.list_desa).show();
                },
                error: function(xhr, ajaxOptions, thrownError) { // Ketika ada error
                    alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
                }
            });
        });
    });
</script>
<script>
    $(document).ready(function() { // Ketika halaman sudah siap (sudah selesai di load)
        // Kita sembunyikan dulu untuk loadingnya
        $("#loading3").hide();

        $("#desa").change(function() { // Ketika user mengganti atau memilih data provinsi
            $("#koperasi").hide(); // Sembunyikan dulu combobox kota nya
            $("#loading3").show(); // Tampilkan loadingnya

            $.ajax({
                type: "POST", // Method pengiriman data bisa dengan GET atau POST
                url: "<?php echo base_url("gudang/list_koperasi"); ?>", // Isi dengan url/path file php yang dituju
                data: {
                    id: $("#desa").val()
                }, // data yang akan dikirim ke file yang dituju
                dataType: "json",
                beforeSend: function(e) {
                    if (e && e.overrideMimeType) {
                        e.overrideMimeType("application/json;charset=UTF-8");
                    }
                },
                success: function(response) { // Ketika proses pengiriman berhasil
                    $("#loading3").hide(); // Sembunyikan loadingnya

                    // set isi dari combobox kota
                    // lalu munculkan kembali combobox kotanya
                    $("#koperasi").html(response.list_koperasi).show();
                },
                error: function(xhr, ajaxOptions, thrownError) { // Ketika ada error
                    alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
                }
            });
        });
    });
</script>
<script type="text/javascript">
	$(document).ready(function() {
	    $('#example').DataTable(
      );
	    function filterData () {
		    $('#example').DataTable().search(
		        $('.kotas').val()
		    	).draw();
		}
		$('.kotas').on('change', function () {
	        filterData();
	    });
         $('.btn-reset').on('click', function () {
	        filterData('.kotas').clear();
	    });

	});
</script>
<script type="text/javascript">
	$(document).ready(function() {
	    $('#example').DataTable(
      );
	    function filterData () {
		    $('#example').DataTable().search(
		        $('.keca').val()
		    	).draw();
		}
		$('.keca').on('change', function () {
	        filterData();
	    });
	});
</script>
<script type="text/javascript">
	$(document).ready(function() {
	    $('#example').DataTable(
      );
	    function filterData () {
		    $('#example').DataTable().search(
		        $('.des').val()
		    	).draw();
		}
		$('.des').on('change', function () {
	        filterData();
	    });
       
	});
</script>


