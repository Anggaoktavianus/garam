<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Penerima Hibah</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Edit</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Form Edit Data Hibah </h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="" method="POST" enctype="multipart/form-data">
                <div class="card-body">
                  <p style="color:red;"><small>*Penulisan angka desimal menggunakan titik (.) Contoh : 10.00</small></p>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group"> 
                        <label for="exampleInputEmail1">Nama Kelompok</label>
                        <input type="hidden" name="id" class="form-control"  value="<?= $row->id ?>" >
                        <input type="text" name="kelompok" class="form-control" value="<?= $row->kelompok ?>" placeholder="Masukkan Nama Kelompok" >
                          <?= form_error('kelompok', '<div class="text-danger"><small>', '</small></div>') ?>
                      </div>
                       <div class="form-group">
                        <label for="exampleInputPassword2">Nama Ketua</label>
                        <input type="text" name="ketua" class="form-control" id="exampleInputPassword2" value="<?= $row->ketua?>" placeholder="Nama ketua" required>
                      </div> 
                      <div class="form-group">
                        <label for="exampleInputEmail1">Telepon</label>
                        <input type="text" name="telepon" class="form-control" value="<?= $row->telepon ?>" id="exampleInputEmail1" placeholder="Luas meja" >
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword2">Jenis Hibah</label>
                        <input type="text" name="jenis_hibah" class="form-control" value="<?= $row->jenis_hibah ?>" id="exampleInputPassword2" placeholder="Jenis hibah" >
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword2">Sumber Pendanaan</label>
                        <input type="text" name="sumberdana" class="form-control" value="<?= $row->sumberdana ?>" id="exampleInputPassword2" placeholder="Sumber Pendanaan" >
                      </div>
                      <div class="form-group">
                          <label>Alamat</label>
                          <textarea class="form-control" name="alamat" rows="5"  placeholder="Masukkan alamat ..."><?= $row->alamat ?></textarea>
                      </div>
                    </div>
                    <div class="col-md-6">  
                       <div class="form-group">
                        <label for="exampleInputPassword2">Anggaran (Rp)</label>
                        <input type="text" name="anggaran" class="form-control" value="<?= $row->anggaran ?>" id="exampleInputPassword2" placeholder="Jumlah anggaran" >
                      </div>
                       <div class="form-group">
                        <label for="exampleInputPassword2">Volume</label>
                        <input type="text" name="volume" class="form-control" value="<?= $row->volume ?>" id="exampleInputPassword2" placeholder="Volume" >
                      </div>
                       <div class="form-group">
                        <label for="exampleInputPassword2">Satuan</label>
                        <input type="text" name="satuan" class="form-control" value="<?= $row->satuan ?>" id="exampleInputPassword2" placeholder="Satuan" >
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword2">Tahun</label>
                         <select name="tahun" class="form-control not-dark">
                              <option value="<?= $row->tahun ?>" ><?= $row->tahun ?></option>
                              <option value="2030">2030</option>
                              <option value="2029">2029</option>
                              <option value="2028">2028</option>
                              <option value="2027">2027</option>
                              <option value="2026">2026</option>
                              <option value="2025">2025</option>
                              <option value="2024">2024</option>
                              <option value="2023">2023</option>
                              <option value="2022">2022</option>
                              <option value="2021">2021</option>
                              <option value="2020">2020</option>
                              <option value="2019">2019</option>
                              <option value="2018">2018</option>
                              <option value="2017">2017</option>
                              <option value="2016">2016</option>
                              <option value="2015">2015</option>
                        </select>
                      </div> 
                       <?php if ($this->fungsi->user_login()->role_id == 1) { ?>
                      <div class="form-group" >
                          <label for="kabkota">Kab/Kota</label>
                          <select name="kabkota_id" class="form-control not-dark" id="kota" >
                            <option value="">--Select--</option>
                            <?php foreach ($kabkota as $key => $data) { ?>
                            <option value="<?= $data->id ?>"<?= $data->id == $row->kabkota_id ? "selected" : null ?> ><?= $data->name ?></option>
                            <?php } ?>
										      </select>
                      </div>
                      <div class="form-group" >
                          <label for="kecamatan_id">Kecamatan</label>
                          <select name="kecamatan_id" class="form-control not-dark" id="kecamatan">
												        <option value="">--Select--</option>
                                <?php foreach ($keca as $key => $data) { ?>
                                <option value="<?= $data->id ?>"<?= $data->id == $row->kecamatan_id ? "selected" : null ?> ><?= $data->name ?></option>
                                <?php } ?>
											    </select>
                      </div>
                      <?php } ?>
                      <?php if ($this->fungsi->user_login()->role_id == 2) { ?>
                      <input type="hidden" name="kabkota_id"  class="form-control" value="<?= $this->session->userdata('kabkota_id')?>" readonly>
                      <div class="form-group" >
                          <label for="kecamatan_id">Kecamatan</label>
                          <select name="kecamatan_id" class="form-control not-dark" id="kecamatan" >
												        <option value="">--Select--</option>
                                 <?php foreach ($kecam as $key => $data) { ?>
                                <option value="<?= $data->id ?>"<?= $data->id == $row->kecamatan_id ? "selected" : null ?> ><?= $data->name ?></option>
                                <?php } ?>
											    </select>
                      </div>
                      <?php } ?>
                      <div class="form-group" id="otherFieldDiv">
                          <label for="desa">Kelurahan</label>
                          <select name="desa" class="form-control not-dark" id="desa" >
												    <option value="">--Select--</option>
                            <?php foreach ($desa as $key => $data) { ?>
                            <option value="<?= $data->id ?>"<?= $data->id == $row->kelurahan_id ? "selected" : null ?> ><?= $data->name ?></option>
                            <?php } ?>
											    </select>
                      </div>
                    </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (left) -->
          <!-- right column -->
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  