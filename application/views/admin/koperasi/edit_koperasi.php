<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Koperasi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Edit</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Form Edit Data Koperasi </h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="" method="POST" enctype="multipart/form-data">
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group"> 
                        <label for="exampleInputEmail1">Nama</label>
                        <input type="hidden" name="id" class="form-control"  value="<?= $row->id ?>" >
                        <input type="text" name="nama" class="form-control" value="<?= $row->nama ?>" placeholder="Masukkan Nama" >
                          <?= form_error('nama', '<div class="text-danger"><small>', '</small></div>') ?>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword2">Tanggal Akta</label>
                        <input type="date" name="tgl_akta" class="form-control" id="exampleInputPassword2" value="<?= $row->tgl_akta ?>"  >
                      </div>
                    </div>
                    <div class="col-md-6">  
                      <div class="form-group">
                        <label for="exampleInputEmail1">No Akta</label>
                        <input type="text" name="no_akta" class="form-control" value="<?= $row->no_akta ?>" id="exampleInputEmail1" placeholder="Luas meja" >
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword2">Email</label>
                        <input type="email" name="email" class="form-control" value="<?= $row->email ?>" id="exampleInputPassword2" placeholder="Longitude" >
                      </div>
                    </div>
                    <div class="col-md-6">  
                      <div class="form-group">
                        <label for="exampleInputEmail1">Telepon</label>
                        <input type="text" name="telepon" class="form-control" value="<?= $row->telepon ?>" id="exampleInputEmail1" placeholder="Luas meja" >
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword2">Jumlah Anggota</label>
                        <input type="text" name="jumlah_anggota" class="form-control" value="<?= $row->jumlah_anggota ?>" id="exampleInputPassword2" placeholder="Longitude" >
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="exampleInputPassword2">Nama Ketua</label>
                        <input type="text" name="ketua" class="form-control" id="exampleInputPassword2" value="<?= $row->ketua?>" placeholder="Nama ketua koperasi" required>
                      </div> 
                      <?php if ($this->fungsi->user_login()->role_id == 1) { ?>
                      <div class="form-group" id="otherFieldDiv">
                          <label for="kabkota">Kab/Kota</label>
                          <select name="kabkota_id" class="form-control not-dark" id="kota" >
                            <option value="">--Select--</option>
                            <?php foreach ($kabkota as $key => $data) { ?>
                            <option value="<?= $data->id ?>"<?= $data->id == $row->kabkota_id ? "selected" : null ?> ><?= $data->name ?></option>
                            <?php } ?>
										      </select>
                      </div>
                      <?php } ?>
                    </div>
                    <div class="col-md-6">
                      <?php if ($this->fungsi->user_login()->role_id == 2) { ?>
                      <input type="hidden" name="kabkota_id"  class="form-control" value="<?= $this->session->userdata('kabkota_id')?>" readonly>
                      <div class="form-group" >
                          <label for="kecamatan_id">Kecamatan</label>
                          <select name="kecamatan_id" class="form-control not-dark" id="kecamatan" >
												        <option value="">--Select--</option>
                                 <?php foreach ($kecam as $key => $data) { ?>
                                <option value="<?= $data->id ?>"<?= $data->id == $row->kecamatan_id ? "selected" : null ?> ><?= $data->name ?></option>
                                <?php } ?>
											    </select>
                      </div>
                      <?php } ?>
                      <?php if ($this->fungsi->user_login()->role_id == 1) { ?>
                        <div class="form-group" id="otherFieldDiv">
                          <label for="kecamatan_id">Kecamatan</label>
                          <select name="kecamatan_id" class="form-control not-dark" id="kecamatan">
												        <option value="">--Select--</option>
                                <?php foreach ($keca as $key => $data) { ?>
                            <option value="<?= $data->id ?>"<?= $data->id == $row->kecamatan_id ? "selected" : null ?> ><?= $data->name ?></option>
                            <?php } ?>
											    </select>
                      </div>
                       <?php } ?>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group" id="otherFieldDiv">
                          <label for="desa">Kelurahan</label>
                          <select name="desa" class="form-control not-dark" id="desa" >
												    <option value="">--Select--</option>
                            <?php foreach ($desa as $key => $data) { ?>
                            <option value="<?= $data->id ?>"<?= $data->id == $row->kelurahan_id ? "selected" : null ?> ><?= $data->name ?></option>
                            <?php } ?>
											    </select>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                          <label>Alamat</label>
                          <textarea class="form-control" name="alamat" rows="3"  placeholder="Masukkan alamat ..."><?= $row->alamat ?></textarea>
                      </div>
                    </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (left) -->
          <!-- right column -->
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  
  