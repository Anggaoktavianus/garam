<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Koperasi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Koperasi </li>
            </ol>
          </div>
        </div>
        <?php
					$info= $this->session->flashdata('info');
					$pesan= $this->session->flashdata('pesan');

					if($info == 'success'){ ?>
						<div class="alert alert-success">
  						  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  						  <i class="icon-gift"></i><strong><?=$info?></strong> <?=$pesan?>.
  						</div>
					<?php    
					}elseif($info == 'danger'){ ?>
						<div class="alert alert-danger">
  						  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="icon-gift"></i><strong><?=$info?></strong> <?=$pesan?>.	
						</div>
					<?php  }else{ } ?>
      </div><!-- /.container-fluid -->
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Data Tabel Koperasi</h3><br><br>
                <a href="<?= site_url('Koperasi/add_index')?>" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i>Tambah Data</a>
              </div>
             
              <!-- <font color="green"><?php echo $this->session->flashdata('pesan'); ?></font> -->
              <!-- /.card-header -->
              
              <div class="card-body">
                <div class="table-responsive">
                <?php if ($this->fungsi->user_login()->role_id == 1) { ?>
                <table id="example" class="table table-bordered table-striped" style="width: 100%;">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>No Akta</th>
                    <th>Tanggal</th>
                    <th>Telepon</th>
                    <th>Email</th>
                    <th>Alamat</th>
                    <th>Ketua</th>
                    <th>Anggota</th>
                    <th>Action</th>
                  </tr>
                  </thead> 
                  <tbody>
                  </tbody>
                  <tfoot>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>No Akta</th>
                    <th>Tanggal</th>
                    <th>Telepon</th>
                    <th>Email</th>
                    <th>Alamat</th>
                    <th>Ketua</th>
                    <th>Anggota</th>
                    <th>Action</th>
                  </tr>
                  </tfoot>
                </table>
                <?php } ?>
                <?php if ($this->fungsi->user_login()->role_id == 2) { ?>
                <table id="kota" class="table table-bordered table-striped" style="width: 100%;">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>No Akta</th>
                    <th>Tanggal</th>
                    <th>Telepon</th>
                    <th>Email</th>
                    <th>Alamat</th>
                    <th>Ketua</th>
                    <th>Anggota</th>
                    <th>Action</th>
                  </tr>
                  </thead> 
                  <tbody>
                  </tbody>
                  <tfoot>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>No Akta</th>
                    <th>Tanggal</th>
                    <th>Telepon</th>
                    <th>Email</th>
                    <th>Alamat</th>
                    <th>Ketua</th>
                    <th>Anggota</th>
                    <th>Action</th>
                  </tr>
                  </tfoot>
                </table>
                <?php } ?>
                </div>
                
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- <script> 
    $(document).ready(function(){
        var table = $('#example').DataTable({
        processing: true,
        serverSide: true,
        responsive: true, 
        lengthChange: false, 
        stateSave: true,
        autoWidth: false,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        dom: '<"html5buttons">Bfrtip',
        language: {
                buttons: {
                    colvis : 'Show/Hide', // label button show / hide
                    colvisRestore: "Reset Kolom", //lael untuk reset kolom ke default
                    columns: '0,1,2,3,4,5,6'
                }
        },
        
        buttons : [
                    {extend: 'colvis', postfixButtons: [ 'colvisRestore' ] },
                    {extend: 'pdf', title:'Data Koperasi'},
                    {extend: 'excel', title: 'Data Koperasi'},
                    {extend:'print',title: 'Data Koperasi'},
        ],
        ajax: {
                "url": "<?= site_url('Koperasi/get_ajax') ?>",
                "type": "POST"
            },
    });
    });
</script> -->

<script>
  $(function () {
    $("#example").DataTable({
       "processing": true,
      "responsive": false, 
      "lengthChange": true,
      "stateSave": true, 
      "autoWidth": false,
      "savestate" : false,
      "serverSide": true,
      // "scrollX": true,
      //   "fixedColumns":   {
      //       leftColumns: 1,
      //       rightColumns: 1
      //   },
        
      "ajax": {
                "url": "<?= site_url('koperasi/get_ajax') ?>",
                "type": "POST"
            },
      dom: '<"html5buttons">Bfrtip',
      'lengthMenu' : [
                        [ 10, 25, 50, -1 ],
                        [ '10 rows', '25 rows', '50 rows', 'Show all' ]
                ],   
                                   
      
      // "buttons": ["csv", "excel", "pdfHtml5","pageLength"],

      buttons: [
            
            {
                extend: 'excelHtml5', 
                title: 'Data Koperasi',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ]
                }
            },
            {
                extend: 'pdfHtml5',
                title: 'Data Koperasi',
                orientation: 'landscape',
                pageSize: 'A4',
                width: 'auto',
                exportOptions: {
                 
                    columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8],
                    
                }
                
            },
            {
                extend: 'pageLength',
                title: 'Data Koperasi',
                exportOptions: {
                    columns: ':visible'
                }
            },
        ]
       
    }).buttons().container().appendTo('#example_wrapper .col-md-6:eq(0)');
  });
</script>
<script>
  $(function () {
    $("#kota").DataTable({
       "processing": true,
      "responsive": false, 
      "lengthChange": true,
      "stateSave": true, 
      "autoWidth": false,
      "savestate" : false,
      "serverSide": true,
      // "scrollX": true,
      //   "fixedColumns":   {
      //       leftColumns: 1,
      //       rightColumns: 1
      //   },
        
      "ajax": {
                "url": "<?= site_url('koperasi/get_ajax_kota') ?>",
                "type": "POST"
            },
      dom: '<"html5buttons">Bfrtip',
      'lengthMenu' : [
                        [ 10, 25, 50, -1 ],
                        [ '10 rows', '25 rows', '50 rows', 'Show all' ]
                ],   
                                   
      
      // "buttons": ["csv", "excel", "pdfHtml5","pageLength"],

      buttons: [
            
            {
                extend: 'excelHtml5', 
                title: 'Data Koperasi',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ]
                }
            },
            {
                extend: 'pdfHtml5',
                title: 'Data Koperasi',
                orientation: 'landscape',
                pageSize: 'A4',
                width: 'auto',
                exportOptions: {
                 
                    columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8],
                    
                }
                
            },
            {
                extend: 'pageLength',
                title: 'Data Koperasi',
                exportOptions: {
                    columns: ':visible'
                }
            },
        ]
       
    }).buttons().container().appendTo('#kota_wrapper .col-md-6:eq(0)');
  });
</script>