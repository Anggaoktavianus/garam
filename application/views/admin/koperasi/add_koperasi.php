<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Tambah Koperasi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Tambah</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Form Tambah Data Koperasi </h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="<?= site_url('koperasi/add')?>" method="POST">
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group"> 
                        <label for="exampleInputEmail1">Nama</label>
                        <input type="text" name="nama" class="form-control" value="<?= set_value('nama') ?>" placeholder="Masukkan Nama" required>
                          <?= form_error('nama', '<div class="text-danger"><small>', '</small></div>') ?>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Tanggal Akta</label>
                        <input type="date" name="tgl_akta" class="form-control" value="<?= set_value('tgl_akta') ?>" id="exampleInputEmail1" required>
                      </div>
                    </div>
                    <div class="col-md-6"> 
                      <div class="form-group">
                        <label for="exampleInputPassword2">No Akta</label>
                        <input type="text" name="no_akta" class="form-control" value="<?= set_value('no_akta') ?>" id="exampleInputPassword2" placeholder="Nomer akta" >
                      </div> 
                      <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="email" name="email" class="form-control" value="<?= set_value('email') ?>" id="exampleInputEmail1" placeholder="masukkan email" required>
                        <?= form_error('email', '<div class="text-danger"><small>', '</small></div>') ?>
                      </div>
                       
                    </div>
                   <div class="col-md-6">
                     <div class="form-group">
                        <label for="exampleInputPassword2">Telepon</label>
                        <input type="number" name="telepon" class="form-control" value="<?= set_value('telepon') ?>" id="exampleInputPassword2" placeholder="Masukkan nomer" required>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword2">Jumlah Anggota</label>
                        <input type="number" name="jumlah_anggota" class="form-control" value="<?= set_value('jumlah_anggota') ?>" id="exampleInputPassword2" placeholder="Jumlah anggota" required>
                      </div>
                   </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="exampleInputPassword2">Nama Ketua</label>
                        <input type="text" name="ketua" class="form-control" value="<?= set_value('ketua') ?>" id="exampleInputPassword2" placeholder="Nama ketua koperasi" required>
                      </div> 
                      <?php if ($this->fungsi->user_login()->role_id == 1) { ?>
                      <div class="form-group" >
                          <label for="kabkota">Kab/Kota</label>
                          <select name="kabkota_id" class="form-control not-dark" id="kota" required>
                            <option value="">--Select--</option>
                            <?php foreach ($kabkota as $key => $data) { ?>
                            <option value="<?= $data->id ?>" ><?= $data->name ?></option>
                            <?php } ?>
										      </select>
                      </div>
                      <?php } ?>
                    </div>
                    <div class="col-md-6">
                      <?php if ($this->fungsi->user_login()->role_id == 1) { ?>
                      <div class="form-group" >
                          <label for="kecamatan_id">Kecamatan</label>
                          <select name="kecamatan_id" class="form-control not-dark" id="kecamatan" required>
												        <option value="">--Select--</option>
											    </select>
                      </div>
                       <?php } ?>
                      <?php if ($this->fungsi->user_login()->role_id == 2) { ?>
                      <input type="hidden" name="kabkota_id"  class="form-control" value="<?= $this->session->userdata('kabkota_id')?>" readonly>
                      <div class="form-group" >
                          <label for="kecamatan_id">Kecamatan</label>
                          <select name="kecamatan_id" class="form-control not-dark" id="kecamatan" required>
												        <option value="">--Select--</option>
                                 <?php foreach ($kecam as $key => $data) { ?>
                                <option value="<?= $data->id ?>" ><?= $data->name ?></option>
                                <?php } ?>
											    </select>
                      </div>
                      <?php } ?>
                      
                    </div>
                    <div class="col-md-6"> 
                      <div class="form-group" >
                          <label for="desa">Kelurahan</label>
                          <select name="desa" class="form-control not-dark" id="desa" required>
												    <option value="">--Select--</option>
											    </select>
                      </div>
                      
                    </div>
                    <div class="col-md-12"> 
                      <div class="form-group">
                          <label>Alamat</label>
                          <textarea class="form-control" name="alamat" value="" rows="3" placeholder="Masukkan alamat ..."><?= set_value('alamat') ?></textarea>
                      </div>
                    </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (left) -->
          <!-- right column -->
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>