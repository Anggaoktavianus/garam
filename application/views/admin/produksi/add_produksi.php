
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Tambah Produksi Garam</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Tambah</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Form Tambah Data produksi </h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="<?= site_url('produksi/add')?>" method="POST">
                <div class="card-body">
                  <p style="color:red;"><small>*Penulisan angka desimal pada KP menggunakan titik (.) Contoh : 10.00</small></p>
                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Bulan</label>
                         <select name="bulan" class="form-control not-dark bulan">
                              <option value="">--Select--</option>
                              <option value="Januari">Januari</option>
                              <option value="Februari">Februari</option>
                              <option value="Maret">Maret</option>
                              <option value="April">April</option>
                              <option value="Mei">Mei</option>
                              <option value="Juni">Juni</option>
                              <option value="Juli">Juli</option>
                              <option value="Agustus">Agustus</option>
                              <option value="September">September</option>
                              <option value="Oktober">Oktober</option>
                              <option value="November">November</option>
                              <option value="Desember">Desember</option>
                          </select>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="exampleInputPassword2">Tahun</label>
                         <select name="tahun" class="form-control not-dark" required>
                              <option value="">--Select--</option>
                              <option value="2030">2030</option>
                              <option value="2029">2029</option>
                              <option value="2028">2028</option>
                              <option value="2027">2027</option>
                              <option value="2026">2026</option>
                              <option value="2025">2025</option>
                              <option value="2024">2024</option>
                              <option value="2023">2023</option>
                              <option value="2022">2022</option>
                              <option value="2021">2021</option>
                              <option value="2020">2020</option>
                              <option value="2019">2019</option>
                              <option value="2018">2018</option>
                              <option value="2017">2017</option>
                              <option value="2016">2016</option>
                              <option value="2015">2015</option>
                        </select>
                      </div> 
                    </div>
                    <?php if ($this->fungsi->user_login()->role_id == 1) { ?>
                    <div class="col-md-6">
                      <div class="form-group" >
                          <label for="kabkota">Kab/Kota</label>
                          <select name="kabkota_id" class="form-control not-dark" id="kota" required>
                            <option value="">--Select--</option>
                            <?php foreach ($kabkota as $key => $data) { ?>
                            <option value="<?= $data->id ?>" ><?= $data->name ?></option>
                            <?php } ?>
										      </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group" >
                          <label for="kecamatan_id">Kecamatan</label>
                          <select name="kecamatan_id" class="form-control not-dark" id="kecamatan" required>
												        <option value="">--Select--</option>
											    </select>
                      </div>
                    </div>
                    <?php } ?>
                    <?php if ($this->fungsi->user_login()->role_id == 2) { ?>
                    <input type="hidden" name="kabkota_id"  class="form-control" value="<?= $this->session->userdata('kabkota_id')?>" readonly>
                    <div class="col-md-6">
                      <div class="form-group" >
                        <label for="kecamatan_id">Kecamatan</label>
                        <select name="kecamatan_id" class="form-control not-dark" id="kecamatan" required>
                              <option value="">--Select--</option>
                                <?php foreach ($kecam as $key => $data) { ?>
                              <option value="<?= $data->id ?>" ><?= $data->name ?></option>
                              <?php } ?>
                        </select>
                      </div>
                    </div>
                    <?php } ?>
                    <div class="col-md-6">
                      <div class="form-group" >
                          <label for="desa">Kelurahan</label>
                          <select name="desa" class="form-control not-dark" id="desa" required>
												    <option value="">--Select--</option>
											    </select>
                      </div>
                    </div>
                    <div class="col-md-7"> 
                      <label for="exampleInputEmail1">Produksi Integrasi (Kg)</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-2">
                      <div class="form-group"> 
                          <label for="exampleInputEmail1"><small><b>KP1 </b></small> </label>
                          <input type="text" name="kp1_lahan_integrasi" class="form-control" value="<?= set_value('kp1_lahan_integrasi') ?>" placeholder="KP1 Produksi Integrasi" required>
                            <?= form_error('kp1_lahan_integrasi', '<div class="text-danger"><small>', '</small></div>') ?>
                        </div> 
                    </div>
                    <div class="col-md-2">
                      <div class="form-group"> 
                           <label for="exampleInputEmail1"><small><b>Harga(Kg/Rp.)</b></small></label>
                            <input type="text" name="kp1_harga_integrasi" class="form-control" value="<?= set_value('kp1_harga_integrasi') ?>" placeholder="Harga KP1" required>
                            <?= form_error('kp1_harga_integrasi', '<div class="text-danger"><small>', '</small></div>') ?>
                        </div> 
                    </div>
                    <div class="col-md-2">
                      <div class="form-group"> 
                          <label for="exampleInputEmail1"><small><b>KP2</b></small></label>
                          <input type="text" name="kp2_lahan_integrasi" class="form-control" value="<?= set_value('kp2_lahan_integrasi') ?>" placeholder="KP2 Produksi Integrasi" required>
                            <?= form_error('kp2_lahan_integrasi', '<div class="text-danger"><small>', '</small></div>') ?>
                        </div> 
                    </div>
                    <div class="col-md-2">
                      <div class="form-group"> 
                           <label for="exampleInputEmail1"><small><b>Harga(Kg/Rp.)</b></small></label>
                            <input type="text" name="kp2_harga_integrasi" class="form-control" value="<?= set_value('kp2_harga_integrasi') ?>" placeholder="Harga KP2" required>
                            <?= form_error('kp2_harga_integrasi', '<div class="text-danger"><small>', '</small></div>') ?>
                        </div> 
                    </div>
                    <div class="col-md-2">
                      <div class="form-group"> 
                          <label for="exampleInputEmail1"><small><b>KP3</b></small></label>
                          <input type="text" name="kp3_lahan_integrasi" class="form-control" value="<?= set_value('kp3_lahan_integrasi') ?>" placeholder="KP3 Produksi Integrasi" required>
                            <?= form_error('kp3_lahan_integrasi', '<div class="text-danger"><small>', '</small></div>') ?>
                        </div> 
                    </div>
                    <div class="col-md-2">
                      <div class="form-group"> 
                           <label for="exampleInputEmail1"><small><b>Harga(Kg/Rp.)</b></small></label>
                            <input type="text" name="kp3_harga_integrasi" class="form-control" value="<?= set_value('kp3_harga_integrasi') ?>" placeholder="Harga KP3" required>
                            <?= form_error('kp3_harga_integrasi', '<div class="text-danger"><small>', '</small></div>') ?>
                        </div> 
                    </div>
                    
                    <div class="col-md-6"> 
                      <label for="exampleInputEmail1">Produksi Non Integrasi (Kg)</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-2">
                      <div class="form-group"> 
                          <label for="exampleInputEmail1"><small><b>KP1</b></small></label>
                          <input type="text" name="kp1_lahan_nonintegrasi" class="form-control" value="<?= set_value('kp1_lahan_nonintegrasi') ?>" placeholder="KP1 Produksi Non Integrasi" required>
                          <?= form_error('kp1_lahan_nonintegrasi', '<div class="text-danger"><small>', '</small></div>') ?>
                        </div> 
                    </div>
                    <div class="col-md-2">
                      <div class="form-group"> 
                           <label for="exampleInputEmail1"><small><b>Harga(Kg/Rp.)</b></small></label>
                             <input type="text" name="kp1_harga_nonintegrasi" class="form-control" value="<?= set_value('kp1_harga_nonintegrasi') ?>" placeholder="Harga KP!" required>
                          <?= form_error('kp1_harga_nonintegrasi', '<div class="text-danger"><small>', '</small></div>') ?>
                        </div> 
                    </div>
                    <div class="col-md-2">
                      <div class="form-group"> 
                          <label for="exampleInputEmail1"><small><b>KP2</b></small></label>
                           <input type="text" name="kp2_lahan_nonintegrasi" class="form-control" value="<?= set_value('kp2_lahan_nonintegrasi') ?>" placeholder="KP2 Produksi Non Integrasi" required>
                          <?= form_error('kp2_lahan_nonintegrasi', '<div class="text-danger"><small>', '</small></div>') ?>
                        </div> 
                    </div>
                    <div class="col-md-2">
                      <div class="form-group"> 
                           <label for="exampleInputEmail1"><small><b>Harga(Kg/Rp.)</b></small></label>
                             <input type="text" name="kp2_harga_nonintegrasi" class="form-control" value="<?= set_value('kp2_harga_nonintegrasi') ?>" placeholder="Harga KP2" required>
                          <?= form_error('kp2_harga_nonintegrasi', '<div class="text-danger"><small>', '</small></div>') ?>
                        </div> 
                    </div>
                    <div class="col-md-2">
                      <div class="form-group"> 
                          <label for="exampleInputEmail1"><small><b>KP3</b></small></label>
                           <input type="text" name="kp3_lahan_nonintegrasi" class="form-control" value="<?= set_value('kp3_lahan_nonintegrasi') ?>" placeholder="KP3 Produksi Non Integrasi" required>
                          <?= form_error('kp3_lahan_nonintegrasi', '<div class="text-danger"><small>', '</small></div>') ?>
                        </div> 
                    </div>
                    <div class="col-md-2">
                      <div class="form-group"> 
                           <label for="exampleInputEmail1"><small><b>Harga(Kg/Rp.)</b></small></label>
                             <input type="text" name="kp3_harga_nonintegrasi" class="form-control" value="<?= set_value('kp3_harga_nonintegrasi') ?>" placeholder="Harga KP3" required>
                          <?= form_error('kp3_harga_nonintegrasi', '<div class="text-danger"><small>', '</small></div>') ?>
                        </div> 
                    </div>
                    <div class="col-md-6"> 
                      <label for="exampleInputEmail1">Garam Non Tambak (Kg)</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-2">
                      <div class="form-group"> 
                          <label for="exampleInputEmail1"><small><b>KP1</b></small></label>
                          <input type="text" name="kp1_garam_nontambak" class="form-control" value="<?= set_value('kp1_garam_nontambak') ?>" placeholder="KP1 Garam Non Tambak" required>
                          <?= form_error('kp1_garam_nontambak', '<div class="text-danger"><small>', '</small></div>') ?>
                        </div> 
                    </div>
                    <div class="col-md-2">
                      <div class="form-group"> 
                           <label for="exampleInputEmail1"><small><b>Harga(Kg/Rp.)</b></small></label>
                             <input type="text" name="kp1_harga_nontambak" class="form-control" value="<?= set_value('kp1_harga_nontambak') ?>" placeholder="Harga KP1" required>
                          <?= form_error('kp1_harga_nontambak', '<div class="text-danger"><small>', '</small></div>') ?>
                        </div> 
                    </div>
                    <div class="col-md-2">
                      <div class="form-group"> 
                          <label for="exampleInputEmail1"><small><b>KP2</b></small></label>
                          <input type="text" name="kp2_garam_nontambak" class="form-control" value="<?= set_value('kp2_garam_nontambak') ?>" placeholder="KP2 Garam Non Tambak" required>
                          <?= form_error('kp2_garam_nontambak', '<div class="text-danger"><small>', '</small></div>') ?>
                        </div> 
                    </div>
                    <div class="col-md-2">
                      <div class="form-group"> 
                           <label for="exampleInputEmail1"><small><b>Harga(Kg/Rp.)</b></small></label>
                             <input type="text" name="kp2_harga_nontambak" class="form-control" value="<?= set_value('kp2_harga_nontambak') ?>" placeholder="Harga KP2" required>
                          <?= form_error('kp2_harga_nontambak', '<div class="text-danger"><small>', '</small></div>') ?>
                        </div> 
                    </div>
                    <div class="col-md-2">
                      <div class="form-group"> 
                          <label for="exampleInputEmail1"><small><b>KP3</b></small></label>
                          <input type="text" name="kp3_garam_nontambak" class="form-control" value="<?= set_value('kp3_garam_nontambak') ?>" placeholder="KP3 Garam Non Tambak" required>
                          <?= form_error('kp3_garam_nontambak', '<div class="text-danger"><small>', '</small></div>') ?>
                        </div> 
                    </div>
                    <div class="col-md-2">
                      <div class="form-group"> 
                           <label for="exampleInputEmail1"><small><b>Harga(Kg/Rp.)</b></small></label>
                             <input type="text" name="kp3_harga_nontambak" class="form-control" value="<?= set_value('kp3_harga_nontambak') ?>" placeholder="Harga KP3" required>
                          <?= form_error('kp3_harga_nontambak', '<div class="text-danger"><small>', '</small></div>') ?>
                        </div> 
                    </div>
                  </div>
                  
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                  
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (left) -->
          <!-- right column -->
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  