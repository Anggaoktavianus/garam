<style>
  th {
  display: table-cell;
  vertical-align: inherit;
  font-weight: bold;
  text-align: center;
}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Produksi Garam</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">produksi </li>
            </ol>
          </div>
        </div>
        <?php
					$info= $this->session->flashdata('info');
					$pesan= $this->session->flashdata('pesan');

					if($info == 'success'){ ?>
						<div class="alert alert-success">
  						  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  						  <i class="icon-gift"></i><strong><?=$info?></strong> <?=$pesan?>.
  						</div>
					<?php    
					}elseif($info == 'danger'){ ?>
						<div class="alert alert-danger">
  						  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="icon-gift"></i><strong><?=$info?></strong> <?=$pesan?>.	
						</div>
					<?php  }else{ } ?>
      </div><!-- /.container-fluid -->
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- <div class="card">
              <div class="card-header">
                <h3 class="card-title">Filter Produksi Garam</h3><br>
              </div>
              <div class="card-body">
                <div id="formfilter">
                  <?= form_open_multipart('gudang/uploaddata') ?>
                    <div class="form-row">
                      <div class="col-4">
                         <label for="exampleInputEmail1">Bulan</label>
                         <select name="bulan" class="form-control not-dark bulan">
                              <option value="">--Select--</option>
                              <option value="Januari">January</option>
                              <option value="Februari">February</option>
                              <option value="Maret">March</option>
                              <option value="April">April</option>
                              <option value="Mei">May</option>
                              <option value="Juni">June</option>
                              <option value="Juli">July</option>
                              <option value="Agustus">August</option>
                              <option value="September">September</option>
                              <option value="Oktober">October</option>
                              <option value="November">November</option>
                              <option value="Desember">December</option>
                          </select>
                      </div>
                      <div class="col-4">
                          <label for="exampleInputPassword2">Tahun</label>
                         <select name="tahun" class="form-control not-dark tahun">
                              <option value="">--Select--</option>
                              <?php foreach ($tahun as $key => $data) { ?>
                              <option value="<?= $data->tahun ?>" ><?= $data->tahun ?></option>
                              <?php } ?>
                        </select>
                      </div>
                    </div><br>
                  <?= form_close(); ?>
                </div>
                  
              </div>
            </div> -->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Data Tabel Produksi Garam</h3><br><br>
                <a href="<?= site_url('produksi/add_index')?>" class="btn btn-primary btn-sm"><i class="fas fa-plus">&nbsp;</i>Tambah Data</a>
                <a href="<?= site_url('filter/produksi')?>" class="btn btn-secondary btn-sm"><i class="fas fa-file-excel">&nbsp;</i>Import Data</a><br><br>
                <div id="formfilter">
                  <?= form_open_multipart('gudang/uploaddata') ?>
                    <div class="form-row">
                      <div class="col-4">
                         <label for="exampleInputEmail1">Bulan</label>
                         <select name="bulan" class="form-control not-dark bulan">
                              <option value="">--Select--</option>
                              <option value="Januari">Januari</option>
                              <option value="Februari">Februari</option>
                              <option value="Maret">Maret</option>
                              <option value="April">April</option>
                              <option value="Mei">Mei</option>
                              <option value="Juni">Juni</option>
                              <option value="Juli">Juli</option>
                              <option value="Agustus">Agustus</option>
                              <option value="September">September</option>
                              <option value="Oktober">Oktober</option>
                              <option value="November">November</option>
                              <option value="Desember">Desember</option>
                          </select>
                      </div>
                      <div class="col-4">
                          <label for="exampleInputPassword2">Tahun</label>
                         <select name="tahun" class="form-control not-dark tahun">
                              <option value="">--Select--</option>
                              <?php foreach ($tahun as $key => $data) { ?>
                              <option value="<?= $data->tahun ?>" ><?= $data->tahun ?></option>
                              <?php } ?>
                        </select>
                      </div>
                    </div><br>
                    <!-- <div class="col-2">
                       <a href="" class="btn btn-secondary "><i class="fas fa-search"></i>&nbsp; Cari Data</a>
                       
                    </div> -->
                  <?= form_close(); ?>
                </div>
              </div>
              
              <div class="card-body">
                <div class="table-responsive">
                <?php if ($this->fungsi->user_login()->role_id == 1) { ?>
                 <table id="example" class="table table-bordered table-striped " style="width: 100%;" >
                  <thead>
                    <tr>
                      <th rowspan="2">No</th>
                      <th colspan="6" >Produksi Integrasi (Kg)</th>
                      <th colspan="6">Produksi Non Intregrasi (Kg)</th>
                      <th colspan="6">Garam Non Tambak (Kg)</th>
                    </tr>
                    <tr>
                      <th>KP1</th>
                      <th>Harga(Kg/Rp.)</th>
                      <th>KP2</th>
                      <th>Harga(Kg/Rp.)</th>
                      <th>KP3</th>
                      <th>Harga(Kg/Rp.)</th>
                      <th>KP1</th>
                      <th>Harga(Kg/Rp.)</th>
                      <th>KP2</th>
                      <th>Harga(Kg/Rp.)</th>
                      <th>KP3</th>
                      <th>Harga(Kg/Rp.)</th>
                      <th>KP1</th>
                      <th>Harga(Kg/Rp.)</th>
                      <th>KP2</th>
                      <th>Harga(Kg/Rp.)</th>
                      <th>KP3</th>
                      <th>Harga(Kg/Rp.)</th>
                      <th>Bulan</th>
                      <th>Tahun</th>
                      <th>Kelurahan</th>
                      <th>Kecamatan</th>
                      <th>Kab/Kota</th>
                      <th>Action</th>
                    </tr>
                  </thead> 
                  <tbody>
              
                  </tbody>
                  <!-- <tfoot>
                  <tr>
                    <th>No</th>
                    <th>KP1</th>
                    <th>KP2</th>
                    <th>KP1</th>
                    <th>KP2</th>
                    <th>KP1</th>
                    <th>KP2</th>
                    <th>Bulan</th>
                    <th>Tahun</th>
                    <th>Kelurahan</th>
                    <th>Kecamatan</th>
                    <th>Kab/Kota</th>
                    <th>Action</th>
                  </tr>
                  </tfoot> -->
                </table>
                <?php } ?>
                <?php if ($this->fungsi->user_login()->role_id == 2) { ?>
                 <table id="kota" class="table table-bordered table-striped " style="width: 100%;" >
                  <thead>
                    <tr>
                      <th rowspan="2">No</th>
                      <th colspan="3">Produksi Integrasi (Kg)</th>
                      <th colspan="3">Produksi Non Intregrasi (Kg)</th>
                      <th colspan="3">Garam Non Tambak (Kg)</th>
                    </tr>
                    <tr>
                     <th>KP1</th>
                      <th>Harga(Kg/Rp.)</th>
                      <th>KP2</th>
                      <th>Harga(Kg/Rp.)</th>
                      <th>KP3</th>
                      <th>Harga(Kg/Rp.)</th>
                      <th>KP1</th>
                      <th>Harga(Kg/Rp.)</th>
                      <th>KP2</th>
                      <th>Harga(Kg/Rp.)</th>
                      <th>KP3</th>
                      <th>Harga(Kg/Rp.)</th>
                      <th>KP1</th>
                      <th>Harga(Kg/Rp.)</th>
                      <th>KP2</th>
                      <th>Harga(Kg/Rp.)</th>
                      <th>KP3</th>
                      <th>Harga(Kg/Rp.)</th>
                      <th>Bulan</th>
                      <th>Tahun</th>
                      <th>Kelurahan</th>
                      <th>Kecamatan</th>
                      <th>Kab/Kota</th>
                      <th>Action</th>
                    </tr>
                  </thead> 
                  <tbody>
              
                  </tbody>
                  <!-- <tfoot>
                  <tr>
                    <th>No</th>
                    <th>KP1</th>
                    <th>KP2</th>
                    <th>KP1</th>
                    <th>KP2</th>
                    <th>KP1</th>
                    <th>KP2</th>
                    <th>Bulan</th>
                    <th>Tahun</th>
                    <th>Kelurahan</th>
                    <th>Kecamatan</th>
                    <th>Kab/Kota</th>
                    <th>Action</th>
                  </tr>
                  </tfoot> -->
                </table>
                <?php } ?>
                </div>
                
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script>
  $(function () {
    $("#example").DataTable({
      "processing": true,
      "searching": true,
      "responsive": false, 
      "lengthChange": true,
      "stateSave": true, 
      "autoWidth": true,
      "serverSide": true,
      // "scrollX": true,
      //   "fixedColumns":   {
      //       leftColumns: 1,
      //       rightColumns: 1
      //   },
        
      "ajax": {
                "url": "<?= site_url('produksi/get_ajax') ?>",
                "type": "POST"
            },
      
      dom: '<"html5buttons">Bfrtip',
      'lengthMenu' : [
                        [ 10, 25, 50, -1 ],
                        [ '10 rows', '25 rows', '50 rows', 'Show all' ]
                ],   
                                   
      
      // "buttons": ["csv", "excel", "pdfHtml5","pageLength"],
      buttons: [
            
            {
                extend: 'excelHtml5', 
                title: 'Data Produksi Garam',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]
                }
            },
            // {
            //     extend: 'pdfHtml5',
            //     title: 'Data Produksi Garam',
            //     orientation: 'landscape',
            //     pageSize: 'Legal',
            //     width: 'auto',
            //     exportOptions: {
                 
            //         columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23],
                    
            //     }
                
            // },
            {
                extend: 'pageLength',
                title: 'Data Produksi Garam',
                exportOptions: {
                    columns: ':visible'
                }
            },
        ]
       
    }).buttons().container().appendTo('#example_wrapper .col-md-6:eq(0)');
    function filterData () {
		    $('#example').DataTable().search(
		        $('.bulan').val()
		    	).draw();
		}
		$('.bulan').on('change', function () {
	        filterData();
	    });

      
  });
</script>

<script type="text/javascript">
	$(document).ready(function() {
	    $('#example').DataTable(
      );
	    function filterData () {
		    $('#example').DataTable().search(
		        $('.tahun').val()
		    	).draw();
		}
		$('.tahun').on('change', function () {
	        filterData();
	    });
	});
</script>

<script>
  $(function () {
    $("#kota").DataTable({
      "processing": true,
      "searching": true,
      "responsive": false, 
      "lengthChange": true,
      "stateSave": true, 
      "autoWidth": false,
      "serverSide": true,
      // "scrollX": true,
      //   "fixedColumns":   {
      //       leftColumns: 1,
      //       rightColumns: 1
      //   },
        
      "ajax": {
                "url": "<?= site_url('produksi/get_ajax_kota') ?>",
                "type": "POST"
            },
      
      dom: '<"html5buttons">Bfrtip',
      'lengthMenu' : [
                        [ 10, 25, 50, -1 ],
                        [ '10 rows', '25 rows', '50 rows', 'Show all' ]
                ],   
                                   
      
      // "buttons": ["csv", "excel", "pdfHtml5","pageLength"],
      
                             
                             
      buttons: [
            
            {
                extend: 'excelHtml5', 
                title: 'Data Produksi Garam',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]
                }
            },
            // {
            //     extend: 'pdfHtml5',
            //     title: 'Data Produksi Garam',
            //     orientation: 'landscape',
            //     pageSize: 'A4',
            //     width: 'auto',
            //     exportOptions: {
                 
            //         columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14],
                    
            //     }
                
            // },
            {
                extend: 'pageLength',
                title: 'Data Produksi Garam',
                exportOptions: {
                    columns: ':visible'
                }
            },
        ]
       
    }).buttons().container().appendTo('#kota_wrapper .col-md-6:eq(0)');
    function filterData () {
		    $('#kota').DataTable().search(
		        $('.bulan').val()
		    	).draw();
		}
		$('.bulan').on('change', function () {
	        filterData();
	    });

      
  });
</script>

<script type="text/javascript">
	$(document).ready(function() {
	    $('#kota').DataTable(
      );
	    function filterData () {
		    $('#kota').DataTable().search(
		        $('.tahun').val()
		    	).draw();
		}
		$('.tahun').on('change', function () {
	        filterData();
	    });
	});
</script>

