<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Produksi Garam</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Edit</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Form Edit Data Lahan Garam </h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
             <form action="" method="POST" enctype="multipart/form-data">
                <div class="card-body">
                  <p style="color:red;"><small>*Penulisan angka desimal pada lahan integrasi/nonintegrasi menggunakan titik (.) Contoh : 10.00</small></p>
                  <div class="row">
                    <div class="col-md-5">
                      <div class="form-group"> 
                        <label for="exampleInputEmail1">Lahan Integrasi (㎡)</label>
                         <input type="hidden" name="id" class="form-control"  value="<?= $row->id ?>" >
                        <input type="text" name="lahan_integrasi" class="form-control" value="<?= $row->lahan_integrasi ?>" placeholder="Masukkan KP" >
                          <?= form_error('lahan_integrasi', '<div class="text-danger"><small>', '</small></div>') ?>
                      </div>
                      <div class="form-group"> 
                          <label for="exampleInputEmail1">Lahan Non Integrasi (㎡)</label>
                          <input type="text" name="lahan_nonintegrasi" class="form-control" value="<?= $row->lahan_nonintegrasi ?>" placeholder="Masukkan KP" >
                          <?= form_error('lahan_nonintegrasi', '<div class="text-danger"><small>', '</small></div>') ?>
                      </div>
                      <div class="form-group"> 
                          <label for="exampleInputEmail1">Latitude</label>
                          <input type="text" name="latitude" id="Latitude" class="form-control" value="<?= $row->latitude ?>" placeholder="Latitude"  required>
                      </div>
                      <div class="form-group"> 
                          <label for="exampleInputEmail1">Longitude</label>
                          <input type="text" name="longitude" id="Longitude" class="form-control" value="<?= $row->longitude ?>" placeholder="Longitude" required>
                          
                      </div>
                       <!-- Jika Role Id 1 Admin Provinsi -->
                      <?php if ($this->fungsi->user_login()->role_id == 1) { ?>
                      <div class="form-group" >
                          <label for="kabkota">Kab/Kota</label>
                          <select name="kabkota_id" class="form-control not-dark" id="kota" >
                            <option value="">--Select--</option>
                            <?php foreach ($kabkota as $key => $data) { ?>
                             <option value="<?= $data->id ?>"<?= $data->id == $row->kabkota_id ? "selected" : null ?> ><?= $data->name ?></option>
                            <?php } ?>
										      </select>
                      </div>
                      <div class="form-group" id="otherFieldDiv">
                          <label for="kecamatan_id">Kecamatan</label>
                          <select name="kecamatan_id" class="form-control not-dark" id="kecamatan">
												        <option value="">--Select--</option>
                                <?php foreach ($keca as $key => $data) { ?>
                            <option value="<?= $data->id ?>"<?= $data->id == $row->kecamatan_id ? "selected" : null ?> ><?= $data->name ?></option>
                            <?php } ?>
											    </select>
                      </div>
                      <?php } ?>
                       <!-- Jika Role Id 2 Admin Kabkota -->
                       <?php if ($this->fungsi->user_login()->role_id == 2) { ?>
                       
                      <input type="hidden" name="kabkota_id"  class="form-control" value="<?= $this->session->userdata('kabkota_id')?>" readonly>
                     
                      <div class="form-group" >
                          <label for="kecamatan_id">Kecamatan</label>
                          <select name="kecamatan_id" class="form-control not-dark" id="kecamatan" >
												        <option value="">--Select--</option>
                                 <?php foreach ($kecam as $key => $data) { ?>
                                <option value="<?= $data->id ?>"<?= $data->id == $row->kecamatan_id ? "selected" : null ?> ><?= $data->name ?></option>
                                <?php } ?>
											    </select>
                      </div>
                      <?php } ?>
                      <div class="form-group" id="otherFieldDiv">
                          <label for="desa">Kelurahan</label>
                          <select name="desa" class="form-control not-dark" id="desa" >
												    <option value="">--Select--</option>
                            <?php foreach ($desa as $key => $data) { ?>
                            <option value="<?= $data->id ?>"<?= $data->id == $row->kelurahan_id ? "selected" : null ?> ><?= $data->name ?></option>
                            <?php } ?>
											    </select>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Bulan</label>
                         <select name="bulan" class="form-control not-dark">
                             <option value="<?= $row->bulan ?>" ><?= $row->bulan ?></option>
                              <option value="">--Select--</option>
                              <option value="Januari">Januari</option>
                              <option value="Februari">Februari</option>
                              <option value="Maret">Maret</option>
                              <option value="April">April</option>
                              <option value="Mei">Mei</option>
                              <option value="Juni">Juni</option>
                              <option value="Juli">Juli</option>
                              <option value="Agustus">Agustus</option>
                              <option value="September">September</option>
                              <option value="Oktober">Oktober</option>
                              <option value="November">November</option>
                              <option value="Desember">Desember</option>
                          </select>
                      </div>
                       <div class="form-group">
                        <label for="exampleInputPassword2">Tahun</label>
                         <select name="tahun" class="form-control not-dark">
                              <option value="<?= $row->tahun ?>" ><?= $row->tahun ?></option>
                              <option value="2030">2030</option>
                              <option value="2029">2029</option>
                              <option value="2028">2028</option>
                              <option value="2027">2027</option>
                              <option value="2026">2026</option>
                              <option value="2025">2025</option>
                              <option value="2024">2024</option>
                              <option value="2023">2023</option>
                              <option value="2022">2022</option>
                              <option value="2021">2021</option>
                              <option value="2020">2020</option>
                              <option value="2019">2019</option>
                              <option value="2018">2018</option>
                              <option value="2017">2017</option>
                              <option value="2016">2016</option>
                              <option value="2015">2015</option>
                        </select>
                      </div> 
                    </div>
                    <div class="col-md-7"> 
                     <div class="form-group"> 
                        <label for="exampleInputEmail1">Map Pemetaan Lahan</label>
                         <div id="map" style="height: 500px;"></div>
                      </div>
                    </div>

                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (left) -->
          <!-- right column -->
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  
  <script>
    var curLocation=[0,0];
if (curLocation[0]==0 && curLocation[1]==0) {
	curLocation <?php $row->id?>=[<?= $row->latitude ?>, <?= $row->longitude ?>];	
}
  var map = L.map('map').setView([<?= $row->latitude ?>, <?= $row->longitude ?>], 8);
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
			'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
			'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
			id: 'mapbox/streets-v11',
       tileSize: 512,
    zoomOffset: -1,
    accessToken: 'your.mapbox.access.token'
}).addTo(map);

map.attributionControl.setPrefix(false);

var marker = new L.marker(curLocation, {
	draggable:'true'
});


marker.on('dragend', function(event) {
var position = marker.getLatLng();
marker.setLatLng(position,{
	draggable : 'true'
	}).bindPopup(position).update();
	$("#Latitude").val(position.lat);
	$("#Longitude").val(position.lng).keyup();
});

$("#Latitude, #Longitude").change(function(){
	var position =[parseInt($("#Latitude").val()), parseInt($("#Longitude").val())];
	marker.setLatLng(position, {
	draggable : 'true'
	}).bindPopup(position).update();
	map.panTo(position);
});
map.addLayer(marker);


</script>