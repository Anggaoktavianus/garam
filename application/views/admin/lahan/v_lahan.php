<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Luas Lahan Tambak Garam</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Lahan </li>
            </ol>
          </div>
        </div>
        <?php
					$info= $this->session->flashdata('info');
					$pesan= $this->session->flashdata('pesan');
					if($info == 'success'){ ?>
						<div class="alert alert-success">
  						  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  						  <i class="icon-gift"></i><strong><?=$info?></strong> <?=$pesan?>.
  						</div>
					<?php    
					}elseif($info == 'danger'){ ?>
						<div class="alert alert-danger">
  						  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<i class="icon-gift"></i><strong><?=$info?></strong> <?=$pesan?>.	
						</div>
					<?php  }else{ } ?>
      </div><!-- /.container-fluid -->
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">MAPS</h3><br>
                <button class="btn btn-success btn-xs" id="btn-view"><i class="fas fa-eye">&nbsp; View</i></button>
                <button class="btn btn-danger btn-xs" id="btn-hide"><i class="fas fa-eye-slash">&nbsp; Hide</i> </button>
              </div>
              <div class="card-body" id="view_map">
                <div id="formfilter">
                 <!-- <?= $map['html'];?> -->
                 <?php if ($this->fungsi->user_login()->role_id == 1) { ?>
                  <div id="mapid" style="height: 600px;"></div>
                  <?php } ?>
                <?php if ($this->fungsi->user_login()->role_id == 2) { ?>
                  <div id="mapids" style="height: 600px;"></div>
                  <?php } ?>
                </div>
                  
              </div>
            </div>
            
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Data Tabel Lahan Garam</h3><br><br>
                <a href="<?= site_url('lahan/add_index')?>" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i> Tambah Data</a>
                <a href="<?= site_url('filter/lahan')?>" class="btn btn-secondary btn-sm"><i class="fas fa-file-excel">&nbsp;</i>Import Data</a><br><br>
                <div id="formfilter">
                  <?= form_open_multipart('gudang/uploaddata') ?>
                    <div class="form-row">
                      <div class="col-4">
                         <label for="exampleInputEmail1">Bulan</label>
                         <select name="bulan" class="form-control not-dark bulan">
                              <option value="">--Select--</option>
                              <option value="Januari">Januari</option>
                              <option value="Februari">Februari</option>
                              <option value="Maret">Maret</option>
                              <option value="April">April</option>
                              <option value="Mei">Mei</option>
                              <option value="Juni">Juni</option>
                              <option value="Juli">Juli</option>
                              <option value="Agustus">Agustus</option>
                              <option value="September">September</option>
                              <option value="Oktober">Oktober</option>
                              <option value="November">November</option>
                              <option value="Desember">Desember</option>
                          </select>
                      </div>
                      <div class="col-4">
                          <label for="exampleInputPassword2">Tahun</label>
                         <select name="tahun" class="form-control not-dark tahun">
                              <option value="">--Select--</option>
                              <?php foreach ($tahun as $key => $data) { ?>
                              <option value="<?= $data->tahun ?>" ><?= $data->tahun ?></option>
                              <?php } ?>
                        </select>
                      </div>
                    </div><br>
                    <!-- <div class="col-2">
                       <a href="" class="btn btn-secondary "><i class="fas fa-search"></i>&nbsp; Cari Data</a>
                       
                    </div> -->
                  <?= form_close(); ?>
                </div>
              </div>
              
              <div class="card-body">
                <div class="table-responsive">
                    <?php if ($this->fungsi->user_login()->role_id == 1) { ?>
                    <table id="example" class="table table-bordered table-striped" style="width: 100%;">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Lahan Integrasi (m2)</th>
                        <th>Lahan Nonintegrasi (m2)</th>
                        <th>Bulan</th>
                        <th>Tahun</th>
                        <th>Kelurahan</th>
                        <th>Kecamatan</th>
                        <th>Kab/Kota</th>
                        <th>Action</th>
                    </tr>
                    </thead> 
                    <tbody>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Lahan Integrasi (m2)</th>
                        <th>Lahan Nonintegrasi (m2)</th>
                        <th>Bulan</th>
                        <th>Tahun</th>
                        <th>Kelurahan</th>
                        <th>Kecamatan</th>
                        <th>Kab/Kota</th>
                        <th>Action</th>
                    </tr>
                    </tfoot>
                    </table>
                    <?php } ?>
                    <?php if ($this->fungsi->user_login()->role_id == 2) { ?>
                    <table id="kota" class="table table-bordered table-striped" style="width: 100%;">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Lahan Integrasi (m2)</th>
                        <th>Lahan Nonintegrasi (m2)</th>
                        <th>Bulan</th>
                        <th>Tahun</th>
                        <th>Kelurahan</th>
                        <th>Kecamatan</th>
                        <th>Kab/Kota</th>
                        <th>Action</th>
                    </tr>
                    </thead> 
                    <tbody>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Lahan Integrasi (m2)</th>
                        <th>Lahan Nonintegrasi (m2)</th>
                        <th>Bulan</th>
                        <th>Tahun</th>
                        <th>Kelurahan</th>
                        <th>Kecamatan</th>
                        <th>Kab/Kota</th>
                        <th>Action</th>
                    </tr>
                    </tfoot>
                    </table>
                    <?php } ?>
                </div>
                
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<!-- start ajax prov -->
<script>
  $(function () {
    $("#example").DataTable({
      "processing": true,
      "searching": true,
      "responsive": false, 
      "lengthChange": true,
      "stateSave": true, 
      "autoWidth": false,
      "serverSide": true,
      // "scrollX": true,
      //   "fixedColumns":   {
      //       leftColumns: 1,
      //       rightColumns: 1
      //   },
        
      "ajax": {
                "url": "<?= site_url('lahan/get_ajax') ?>",
                "type": "POST"
            },
      
      dom: '<"html5buttons">Bfrtip',
      'lengthMenu' : [
                        [ 10, 25, 50, -1 ],
                        [ '10 rows', '25 rows', '50 rows', 'Show all' ]
                ],   
                                   
      
      // "buttons": ["csv", "excel", "pdfHtml5","pageLength"],
      buttons: [
            
            {
                extend: 'excelHtml5', 
                title: 'Data Lahan Garam',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6, 7 ]
                }
            },
            {
                extend: 'pdfHtml5',
                title: 'Data Lahan Garam',
                orientation: 'potrait',
                pageSize: 'A4',
                width: 'auto',
                exportOptions: {
                 
                    columns: [ 0, 1, 2, 3, 4, 5, 6, 7],
                    
                }
                
            },
            {
                extend: 'pageLength',
                title: 'Data Lahan Garam',
                exportOptions: {
                    columns: ':visible'
                }
            },
        ]
       
    }).buttons().container().appendTo('#example_wrapper .col-md-6:eq(0)');
    function filterData () {
		    $('#example').DataTable().search(
		        $('.bulan').val()
		    	).draw();
		}
		$('.bulan').on('change', function () {
	        filterData();
	    });

      
  });
</script>

<!-- end ajax prov -->
<!-- start ajax kota -->
<?php foreach ($tahun as $key => $data) { ?>
   <?php } ?>
<script>
  $(function () {
    $("#kota").DataTable({
      "processing": true,
      "searching": true,
      "responsive": false, 
      "lengthChange": true,
      "stateSave": true, 
      "autoWidth": false,
      "serverSide": true,
      // "scrollX": true,
      //   "fixedColumns":   {
      //       leftColumns: 1,
      //       rightColumns: 1
      //   },
        
      "ajax": {
                "url": "<?= site_url('lahan/get_ajax_kota') ?>",
                "type": "POST"
            },
      
      dom: '<"html5buttons">Bfrtip',
      'lengthMenu' : [
                        [ 10, 25, 50, -1 ],
                        [ '10 rows', '25 rows', '50 rows', 'Show all' ]
                ],   
                                   
      
      // "buttons": ["csv", "excel", "pdfHtml5","pageLength"],
      
                             
                             
      buttons: [
            
            {
                extend: 'excelHtml5', 
                title: 'Data Lahan Garam ',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6, 7 ]
                }
            },
            {
                extend: 'pdfHtml5',
                title: 'Data Lahan Garam',
                orientation: 'potrait',
                pageSize: 'A4',
                width: 'auto',
                exportOptions: {
                 
                    columns: [ 0, 1, 2, 3, 4, 5, 6, 7],
                    
                }
                
            },
            {
                extend: 'pageLength',
                title: 'Data Lahan Garam',
                exportOptions: {
                    columns: ':visible'
                }
            },
        ]
       
    }).buttons().container().appendTo('#kota_wrapper .col-md-6:eq(0)');
    function filterData () {
		    $('#kota').DataTable().search(
		        $('.bulan').val()
		    	).draw();
		}
		$('.bulan').on('change', function () {
	        filterData();
	    });

      
  });
</script>

<script type="text/javascript">
	$(document).ready(function() {
	    $('#kota').DataTable(
      );
	    function filterData () {
		    $('#kota').DataTable().search(
		        $('.tahun').val()
		    	).draw();
		}
		$('.tahun').on('change', function () {
	        filterData();
	    });
	});
</script>
<!-- end ajax kota -->
<script src="<?= base_url('assets/leaflet.ajax.js')?>"></script>
<script type="text/javascript">
	$(document).ready(function() {
	    $('#example').DataTable(
      );
	    function filterData () {
		    $('#example').DataTable().search(
		        $('.tahun').val()
		    	).draw();
		}
		$('.tahun').on('change', function () {
	        filterData();
	    });
	});
</script>

<!-- ADMIN PROV MAP --> 
<?php if ($this->fungsi->user_login()->role_id == 1) { ?>
<script>
  var mymap = L.map('mapid').setView([-7.147244405606629, 110.26546943924502], 8.5);
  L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw'
  }).addTo(mymap);

  var greenIcon = L.icon({
      iconUrl: '<?= base_url('uploads/mark.png')?>',
      // shadowUrl: 'leaf-shadow.png',

      iconSize:     [35, 35], // size of the icon
      // shadowSize:   [50, 64], // size of the shadow
      // iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
      // shadowAnchor: [4, 62],  // the same for the shadow
      popupAnchor:  [-3, -10] // point from which the popup should open relative to the iconAnchor
  }); 
  <?php foreach ($row->result() as $key => $data) { ?>
  var lokasi <?php $data->id?> = L.marker([<?= $data->latitude ?>, <?= $data->longitude ?>], {icon: greenIcon}).bindPopup("<b>Data Lahan Garam</b> <br>Lahan Integrasi : <?= $data->lahan_integrasi ?> Ha <br> Lahan NonIntegrasi : <?= $data->lahan_nonintegrasi ?> Ha <br>Kota : <?= $data->kabkota ?> <br>Kecamatan : <?= $data->kecamatan ?> <br>Desa : <?= $data->desa ?> ",
  ).addTo(mymap);
  <?php } ?>

    var cilacap = {
        "color": "#ff0000",
        "weight": 0.5,
        "opacity": 0.9
    };
    var banyumas = {
        "color": "#006400",
        "weight": 0.5,
        "opacity": 0.9
    };
    var purbalingga = {
        "color": "#00008B",
        "weight": 0.5,
        "opacity": 0.9
    };
    var banjarnegara = {
        "color": "#ff0000",
        "weight": 0.5,
        "opacity": 0.9
    };
    var kebumen = {
        "color": "#DAA520",
        "weight": 0.5,
        "opacity": 0.9
    };
    var purworejo = {
        "color": "#4B0082",
        "weight": 0.5,
        "opacity": 0.9
    };
    var wonosobo = {
        "color": "#32CD32",
        "weight": 0.5,
        "opacity": 0.9
    };
    var magelang = {
        "color": "#ff0000",
        "weight": 0.5,
        "opacity": 0.9
    };
    var boyolali = {
        "color": "#4682B4",
        "weight": 0.5,
        "opacity": 0.9
    };
    var klaten = {
        "color": "#2E8B57",
        "weight": 0.5,
        "opacity": 0.9
    };
    var sukoharjo = {
        "color": "#00008B",
        "weight": 0.5,
        "opacity": 0.9
    };
    var wonogiri = {
        "color": "#DC143C",
        "weight": 0.5,
        "opacity": 0.9
    };
    var karanganyar = {
        "color": "#B8860B",
        "weight": 0.5,
        "opacity": 0.9
    };
    var sragen = {
        "color": "#32CD32",
        "weight": 0.5,
        "opacity": 0.9
    };
    var grobogan = {
        "color": "#8B008B",
        "weight": 0.5,
        "opacity": 0.9
    };
    var blora = {
        "color": "#FF8C00",
        "weight": 0.5,
        "opacity": 0.9
    };
    var rembang = {
        "color": "#483D8B",
        "weight": 0.5,
        "opacity": 0.9
    };
    var pati = {
        "color": "#ff0000",
        "weight": 0.5,
        "opacity": 0.9
    };
    var kudus = {
        "color": "#FF8C00",
        "weight": 0.5,
        "opacity": 0.9
    };
    var jepara = {
        "color": "#8B008B",
        "weight": 0.5,
        "opacity": 0.9
    };
    var demak = {
        "color": "#2F4F4F",
        "weight": 0.5,
        "opacity": 0.9
    };
    var semarang = {
        "color": "#FF1493",
        "weight": 0.5,
        "opacity": 0.9
    };
    var temanggung = {
        "color": "#00BFFF",
        "weight": 0.5,
        "opacity": 0.9
    };
    var kendal = {
        "color": "#2F4F4F",
        "weight": 0.5,
        "opacity": 0.9
    };
    var batang = {
        "color": "#FF8C00",
        "weight": 0.5,
        "opacity": 0.9
    };
    var pekalongan = {
        "color": "#1E90FF",
        "weight": 0.5,
        "opacity": 0.9
    };
    var pemalang = {
        "color": "#B22222",
        "weight": 0.5,
        "opacity": 0.9
    };
    var tegal = {
        "color": "#4B0082",
        "weight": 0.5,
        "opacity": 0.9
    };
    var brebes = {
        "color": "#FF8C00",
        "weight": 0.5,
        "opacity": 0.9
    };
    var kotamagelang = {
        "color": "#ff0000",
        "weight": 0.5,
        "opacity": 0.9
    };
    var surakarta = {
        "color": "#ff0000",
        "weight": 0.5,
        "opacity": 0.9
    };
    var salatiga = {
        "color": "#FF8C00",
        "weight": 0.5,
        "opacity": 0.9
    };
    var kotasemarang = {
        "color": "#8B0000",
        "weight": 0.5,
        "opacity": 0.9
    };
    var kotapekalongan = {
        "color": "#8B0000",
        "weight": 0.5,
        "opacity": 0.9
    };
    var kotategal = {
        "color": "#8B0000",
        "weight": 0.5,
        "opacity": 0.9
    };
    var waduk = {
        "color": "#00CED1",
        "weight": 0.5,
        "opacity": 0.9
    };
    var hutan = {
        "color": "#2F4F4F",
        "weight": 0.5,
        "opacity": 0.9
    };
  function popUp(f,l){
      var out = [];
      if (f.properties){
          // for(key in f.properties){
          //   console.log(key);
              
          // }
          out.push("Provinsi: "+f.properties['PROVINSI']);
          out.push("Kab/Kota: "+f.properties['KABKOT']);
          l.bindPopup(out.join("<br />"));
      }
  }
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/cilacap.geojson') ?>"],{onEachFeature:popUp,style: cilacap}).addTo(mymap);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/banyumas.geojson') ?>"],{onEachFeature:popUp,style: banyumas}).addTo(mymap);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/purbalingga.geojson') ?>"],{onEachFeature:popUp,style: purbalingga}).addTo(mymap);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/banjarnegara.geojson') ?>"],{onEachFeature:popUp,style: banjarnegara}).addTo(mymap);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/kebumen.geojson') ?>"],{onEachFeature:popUp,style: kebumen}).addTo(mymap);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/purworejo.geojson') ?>"],{onEachFeature:popUp,style: purworejo}).addTo(mymap);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/wonosobo.geojson') ?>"],{onEachFeature:popUp,style: wonosobo}).addTo(mymap);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/magelang.geojson') ?>"],{onEachFeature:popUp,style: magelang}).addTo(mymap);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/boyolali.geojson') ?>"],{onEachFeature:popUp,style: boyolali}).addTo(mymap);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/klaten.geojson') ?>"],{onEachFeature:popUp,style: klaten}).addTo(mymap);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/sukoharjo.geojson') ?>"],{onEachFeature:popUp,style: sukoharjo}).addTo(mymap);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/wonogiri.geojson') ?>"],{onEachFeature:popUp,style: wonogiri}).addTo(mymap);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/karanganyar.geojson') ?>"],{onEachFeature:popUp,style: karanganyar}).addTo(mymap);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/sragen.geojson') ?>"],{onEachFeature:popUp,style: sragen}).addTo(mymap);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/grobogan.geojson') ?>"],{onEachFeature:popUp,style: grobogan}).addTo(mymap);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/blora.geojson') ?>"],{onEachFeature:popUp,style: blora}).addTo(mymap);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/rembang.geojson') ?>"],{onEachFeature:popUp,style: rembang}).addTo(mymap);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/pati.geojson') ?>"],{onEachFeature:popUp,style: pati}).addTo(mymap);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/kudus.geojson') ?>"],{onEachFeature:popUp,style: kudus}).addTo(mymap);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/jepara.geojson') ?>"],{onEachFeature:popUp,style: jepara}).addTo(mymap);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/demak.geojson') ?>"],{onEachFeature:popUp,style: demak}).addTo(mymap);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/semarang.geojson') ?>"],{onEachFeature:popUp,style: semarang}).addTo(mymap);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/temanggung.geojson') ?>"],{onEachFeature:popUp,style: temanggung}).addTo(mymap);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/kendal.geojson') ?>"],{onEachFeature:popUp,style: kendal}).addTo(mymap);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/batang.geojson') ?>"],{onEachFeature:popUp,style: batang}).addTo(mymap);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/pekalongan.geojson') ?>"],{onEachFeature:popUp,style: pekalongan}).addTo(mymap);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/pemalang.geojson') ?>"],{onEachFeature:popUp,style: pemalang}).addTo(mymap);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/tegal.geojson') ?>"],{onEachFeature:popUp,style: tegal}).addTo(mymap);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/brebes.geojson') ?>"],{onEachFeature:popUp,style: brebes}).addTo(mymap);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/kotamagelang.geojson') ?>"],{onEachFeature:popUp,style: kotamagelang}).addTo(mymap);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/surakarta.geojson') ?>"],{onEachFeature:popUp,style: surakarta}).addTo(mymap);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/salatiga.geojson') ?>"],{onEachFeature:popUp,style: salatiga}).addTo(mymap);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/kotasemarang.geojson') ?>"],{onEachFeature:popUp,style: kotasemarang}).addTo(mymap);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/kotapekalongan.geojson') ?>"],{onEachFeature:popUp,style: kotapekalongan}).addTo(mymap);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/kotategal.geojson') ?>"],{onEachFeature:popUp,style: kotategal}).addTo(mymap);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/waduk.geojson') ?>"],{onEachFeature:popUp,style: waduk}).addTo(mymap);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/hutan.geojson') ?>"],{onEachFeature:popUp,style: hutan}).addTo(mymap);

</script>
<?php } ?>
<!-- END ADMIN PROV MAP --> 

<!-- ADMIN KABKOTA MAP --> 
<?php if ($this->fungsi->user_login()->role_id == 2) { ?>
<script>
<?php foreach ($koordinat as $key => $row) { ?>
  var mymaps<?php $row->id?> = L.map('mapids').setView([<?= $row->lat?>, <?= $row->lng ?>], 10,6);
  L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw'
  }).addTo(mymaps);

  var greenIcon = L.icon({
      iconUrl: '<?= base_url('uploads/mark.png')?>',
      iconSize:     [35, 35], // size of the icon
      popupAnchor:  [-3, -10] // point from which the popup should open relative to the iconAnchor
  }); 
  <?php } ?>
  <?php foreach ($rows as $key => $data) { ?>
  var lokasi <?php $data->id?> = L.marker([<?= $data->latitude ?>, <?= $data->longitude ?>], {icon: greenIcon}).bindPopup("<b>Data Lahan Garam</b> <br>Lahan Integrasi : <?= $data->lahan_integrasi ?> Ha <br> Lahan NonIntegrasi : <?= $data->lahan_nonintegrasi ?> Ha <br>Kota : <?= $data->kabkota ?> <br>Kecamatan : <?= $data->kecamatan ?> <br>Desa : <?= $data->desa ?> ",
  ).addTo(mymaps);
  <?php } ?>
  <?php foreach ($geojson as $key => $geo) { ?>
    var <?= $geo->kota?> = {
        "color": "<?= $geo->color?>",
        "weight": 0.5,
        "opacity": 0.9
    };
    
    var waduk = {
        "color": "#00CED1",
        "weight": 0.5,
        "opacity": 0.9
    };
    var hutan = {
        "color": "#2F4F4F",
        "weight": 0.5,
        "opacity": 0.9
    };
    
  function popUp(f,l){
      var out = [];
      if (f.properties){
          out.push("Provinsi: "+f.properties['PROVINSI']);
          out.push("Kab/Kota: "+f.properties['KABKOT']);
          l.bindPopup(out.join("<br />"));
      }
  }
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('') ?>assets/geojson/<?= $geo->geojson?>"],{onEachFeature:popUp,style: <?= $geo->kota?>}).addTo(mymaps);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/waduk.geojson') ?>"],{onEachFeature:popUp,style: waduk}).addTo(mymaps);
    var jsonTest = new L.GeoJSON.AJAX(["<?= base_url('assets/geojson/hutan.geojson') ?>"],{onEachFeature:popUp,style: hutan}).addTo(mymaps);
<?php } ?>
</script>
<?php } ?>
<!-- END ADMIN KABKOTA MAP --> 
<script>
    $(document).ready(function() { // Ketika halaman sudah siap (sudah selesai di load)
        // Kita sembunyikan dulu untuk loadingnya
        $("#view_map").hide();
        $("#btn-view").click(function() { // Ketika user mengganti atau memilih data provinsi
            $("#view_map").show(); // Sembunyikan dulu combobox kota nya
            
        });
        $("#btn-hide").click(function() { // Ketika user mengganti atau memilih data provinsi
            $("#view_map").hide(); // Sembunyikan dulu combobox kota nya
            
        });
    });
</script>
