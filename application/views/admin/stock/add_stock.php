
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Tambah Stok Garam</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Tambah</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Form Tambah Data Stok </h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="<?= site_url('stock/add')?>" method="POST">
                <div class="card-body">
                  <p style="color:red;"><small>*Penulisan angka desimal pada stok menggunakan titik (.) Contoh : 10.00</small></p>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group"> 
                        <label for="exampleInputEmail1">Stok Garam (Kg)</label>
                        <input type="text" name="stock" class="form-control" value="<?= set_value('stock') ?>" placeholder="Masukkan stok garam" required>
                          <?= form_error('stock', '<div class="text-danger"><small>', '</small></div>') ?>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Bulan</label>
                         <select name="bulan" class="form-control not-dark" required>
                              <option value="">--Select--</option>
                              <option value="Januari">Januari</option>
                              <option value="Februari">Februari</option>
                              <option value="Maret">Maret</option>
                              <option value="April">April</option>
                              <option value="Mei">Mei</option>
                              <option value="Juni">Juni</option>
                              <option value="Juli">Juli</option>
                              <option value="Agustus">Agustus</option>
                              <option value="September">September</option>
                              <option value="Oktober">Oktober</option>
                              <option value="November">November</option>
                              <option value="Desember">Desember</option>
                          </select>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword2">Tahun</label>
                         <select name="tahun" class="form-control not-dark" required>
                              <option value="">--Select--</option>
                              <option value="2030">2030</option>
                              <option value="2029">2029</option>
                              <option value="2028">2028</option>
                              <option value="2027">2027</option>
                              <option value="2026">2026</option>
                              <option value="2025">2025</option>
                              <option value="2024">2024</option>
                              <option value="2023">2023</option>
                              <option value="2022">2022</option>
                              <option value="2021">2021</option>
                              <option value="2020">2020</option>
                              <option value="2019">2019</option>
                              <option value="2018">2018</option>
                              <option value="2017">2017</option>
                              <option value="2016">2016</option>
                              <option value="2015">2015</option>
                        </select>
                      </div> 
                    </div>
                   
                    <div class="col-md-6">  
                      <?php if ($this->fungsi->user_login()->role_id == 1) { ?>
                      <div class="form-group" >
                          <label for="kabkota">Kab/Kota</label>
                          <select name="kabkota_id" class="form-control not-dark" id="kota" required>
                            <option value="">--Select--</option>
                            <?php foreach ($kabkota as $key => $data) { ?>
                            <option value="<?= $data->id ?>" ><?= $data->name ?></option>
                            <?php } ?>
										      </select>
                      </div>
                      <div class="form-group" >
                          <label for="kecamatan_id">Kecamatan</label>
                          <select name="kecamatan_id" class="form-control not-dark" id="kecamatan" required>
												        <option value="">--Select--</option>
											    </select>
                      </div>
                      <?php } ?>
                      <?php if ($this->fungsi->user_login()->role_id == 3) { ?>
                      <div class="form-group" >
                          <label for="kabkota">Kab/Kota</label>
                          <select name="kabkota_id" class="form-control not-dark" id="kota" readonly>
                            <option value="<?= $row->kabkota_id?>" selected><?= $row->kabkota?></option>
										      </select>
                      </div>
                      <div class="form-group" >
                          <label for="kecamatan_id">Kecamatan</label>
                          <select name="kecamatan_id" class="form-control not-dark" id="kecamatan" readonly>
												        <option value="<?= $row->kecamatan_id?>" selected><?= $row->kecamatan?></option>
											    </select>
                      </div>
                      <?php } ?>
                      <?php if ($this->fungsi->user_login()->role_id == 2) { ?>
                      <input type="hidden" name="kabkota_id"  class="form-control" value="<?= $this->session->userdata('kabkota_id')?>" readonly>
                      <div class="form-group" >
                          <label for="kecamatan_id">Kecamatan</label>
                          <select name="kecamatan_id" class="form-control not-dark" id="kecamatan" required>
												        <option value="">--Select--</option>
                                 <?php foreach ($kecam as $key => $data) { ?>
                                <option value="<?= $data->id ?>" ><?= $data->name ?></option>
                                <?php } ?>
											    </select>
                      </div>
                      <div class="form-group" >
                          <label for="desa">Kelurahan</label>
                          <select name="desa" class="form-control not-dark" id="desa" required>
												    <option value="">--Select--</option>
											    </select>
                      </div>
                      <?php } ?>
                      <?php if ($this->fungsi->user_login()->role_id == 1) { ?>
                      <div class="form-group" >
                          <label for="desa">Kelurahan</label>
                          <select name="desa" class="form-control not-dark" id="desa" required>
												    <option value="">--Select--</option>
											    </select>
                      </div>
                      <?php } ?>
                      
                      <?php if ($this->fungsi->user_login()->role_id == 3) { ?>
                      <div class="form-group" >
                          <label for="desa">Kelurahan</label>
                          <select name="desa" class="form-control not-dark" id="desa"  readonly>
												    <option value="<?= $row->kelurahan_id?>" selected><?= $row->desa?></option>
											    </select>
                      </div>
                      <?php } ?>
                    </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (left) -->
          <!-- right column -->
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  