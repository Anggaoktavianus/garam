<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Profile</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Profile</li>
            </ol>
          </div>
        </div>
        <?php
          $info= $this->session->flashdata('info');
          $pesan= $this->session->flashdata('pesan');

          if($info == 'success'){ ?>
            <div class="alert alert-success">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <i class="icon-gift"></i><strong><?=$info?></strong> <?=$pesan?>.
            </div>
          <?php    
          }elseif($info == 'danger'){ ?>
			<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<i class="icon-gift"></i><strong><?=$info?></strong> <?=$pesan?>.	
			</div>
		      <?php  }else{ } ?>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <!-- Input addon -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Identitas </h3>
              </div>
	            <?= form_open_multipart('user/profile') ?>
              <div class="card-body">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text">@</span>
                  </div>
                  <input type="text" name="nama" class="form-control" placeholder="Username" value="<?= $user['nama'];?>">
		            <?= form_error('nama', '<div class="text-danger"><small>', '</small></div>') ?>
                </div>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                  </div>
		              <!-- <input type="hidden" name="id" class="form-control" > -->
                  <input type="email" name="email" class="form-control" value="<?= $user['email'];?>" placeholder="Email">
		             <?= form_error('email', '<div class="text-danger"><small>', '</small></div>') ?>
                </div>

                <!-- ADMIN GUDANG -->
                <?php if ($this->fungsi->user_login()->role_id == 3) { ?>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-map-marker"></i></span>
                  </div> 
                  
                  <select name="kabkota" class="form-control" readonly >
                  <option value="<?= $row->kabkota_id?>" selected><?= $row->kabkota?></option>
                  </select>
                </div>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-map-marker"></i></span>
                  </div> 
                  <select name="kecamatan" class="form-control" readonly >
                  <option value="<?= $row->kecamatan_id?>" selected><?= $row->kecamatan?></option>
                  </select>
                </div>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-map-marker"></i></span>
                  </div> 
                  <select name="desa" class="form-control" readonly >
                  <option value="<?= $row->kelurahan_id?>" selected><?= $row->desa?></option>
                  </select>
                </div>
                <?php } ?>

                <!-- ADMIN KABKOTA -->
                <?php if ($this->fungsi->user_login()->role_id == 2) { ?>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-map-marker"></i></span>
                  </div> 
                  <select name="kabkota" class="form-control" readonly >
                  <option value="<?= $row->kabkota_id?>" selected><?= $row->kabkota?></option>
                  </select>
                </div>
                <?php } ?>
		            <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fas fa-eye"></i></span>
                  </div> 
                  <select name="status" class="form-control" readonly >
                  <option value="1" selected>Aktif</option>
                  </select>
                </div>
		            <div class="input-group mb-3">
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
              </div>
	          </form>
	        </div>
        </div>
          <!--/.col (left) -->
          <!-- right column -->
        <div class="col-md-6">
          <!-- Form Element sizes -->
          <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">Password </h3>
              </div>
              <?= form_open_multipart('user/changepassword') ?>
                <div class="card-body">
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-key"></i></span>
                    </div>
                    <input type="password" name="password" class="form-control" placeholder="Masukkan password lama">
                    <!-- <input type="hidden" name="id" class="form-control" placeholder="Masukkan password"> -->
                  </div>
                  <?= form_error('password', '<div class="text-danger"><small>', '</small></div>') ?>
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-key"></i></span>
                    </div>
                    <input type="password" name="newpassword" class="form-control" value="" placeholder="Masukkan password baru">
                  </div>
                  <?= form_error('newpassword', '<div class="text-danger"><small>', '</small></div>') ?>
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-key"></i></span>
                    </div>
                    <input type="password" name="newpassword2" class="form-control" value="" placeholder="Konfirmasi password">
        
                  </div>
                  <?= form_error('newpassword2', '<div class="text-danger"><small>', '</small></div>') ?>
                  <div class="input-group mb-3">
                    <button type="submit" class="btn btn-primary">Ubah Password</button>
                  </div>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
              <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>

    </div>
  