<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit petambak</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Edit</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Form Edit Data petambak </h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="" method="POST" enctype="multipart/form-data">
                <div class="card-body">
                  <p style="color:red;"><small>*Penulisan angka desimal pada luas lahan menggunakan titik (.) Contoh : 10.00</small></p>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group"> 
                        <label for="exampleInputEmail1">Nama</label>
                        <input type="hidden" name="id" class="form-control"  value="<?= $row->id ?>" >
                        <input type="text" name="nama" class="form-control" value="<?= $row->nama ?>" placeholder="Masukkan Nama" >
                          <?= form_error('nama', '<div class="text-danger"><small>', '</small></div>') ?>
                      </div>
                       <div class="form-group">
                        <label for="exampleInputEmail1">Tempat Lahir</label>
                        <input type="text" name="lahir" class="form-control" value="<?= $row->tempat_lahir ?>" id="exampleInputEmail1" placeholder="Luas meja" >
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword2">Status Petambak</label>
                        <select name="status_petambak" class="form-control not-dark" >
                            <option value="<?= $row->status_petambak ?>"><?= $row->status_petambak ?></option>
                            <option value="">--Select--</option>
                            <option value="Pemilik">Pemilik</option>
                            <option value="Penggarap">Penggarap</option>
										      </select>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword2">Status Lahan</label>
                        <select name="status_lahan" class="form-control not-dark" >
                            <option value="<?= $row->status_lahan ?>"><?= $row->status_lahan ?></option>
                            <option value="">--Select--</option>
                            <option value="Sewa">Sewa</option>
                            <option value="Milik Sendiri">Milik Sendiri</option>
                            <option value="Lainnya">Lainnya</option>
										      </select>
                      </div>
                      <?php if ($this->fungsi->user_login()->role_id == 1) { ?>
                     <div class="form-group" id="otherFieldDiv">
                          <label for="kecamatan_id">Kecamatan</label>
                          <select name="kecamatan_id" class="form-control not-dark" id="kecamatan">
												        <option value="">--Select--</option>
                                <?php foreach ($keca as $key => $data) { ?>
                            <option value="<?= $data->id ?>"<?= $data->id == $row->kecamatan_id ? "selected" : null ?> ><?= $data->name ?></option>
                            <?php } ?>
											    </select>
                      </div>
                      <?php } ?>
                      <div class="form-group" >
                          <label for="koperasi">Koperasi</label>
                          <select name="koperasi_id" class="form-control not-dark" id="koperasi" >
												    <option value="">--Select--</option>
                            <?php foreach ($koperasi as $key => $data) { ?>
                            <option value="<?= $data->id ?>"<?= $data->id == $row->koperasi_id ? "selected" : null ?> ><?= $data->nama ?></option>
                            <?php } ?>
											    </select>
                      </div>
                      <div class="form-group" >
                          <label for="koperasi">Kelompok</label>
                          <select name="kelompok_id" class="form-control not-dark" id="kelompok" >
												    <option value="">--Select--</option>
                            <?php foreach ($kelompok as $key => $data) { ?>
                            <option value="<?= $data->id ?>"<?= $data->id == $row->kelompok_id ? "selected" : null ?> ><?= $data->nama ?></option>
                            <?php } ?>
											    </select>
                      </div>
                    </div>
                    <div class="col-md-6">  
                      <div class="form-group">
                        <label for="exampleInputPassword2">NIK</label>
                        <input type="text" name="nik" class="form-control" value="<?= $row->nik ?>" id="exampleInputPassword2" placeholder="Longitude" >
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword2">Tanggal Lahir</label>
                        <input type="date" name="tgl_lahir" class="form-control" value="<?=  $row->tgl_lahir ?>" id="exampleInputPassword2" placeholder="Nama ketua" required>
                      </div> 
                      <div class="form-group">
                        <label for="exampleInputPassword2">Luas Lahan (㎡)</label>
                        <input type="text" name="luas_lahan" class="form-control" value="<?= $row->luas_lahan ?>" id="exampleInputPassword2" placeholder="Luas Lahan" required>
                      </div> 
                      <?php if ($this->fungsi->user_login()->role_id == 1) { ?>
                      <div class="form-group">
                          <label for="kabkota">Kab/Kota</label>
                          <select name="kabkota_id" class="form-control not-dark" id="kota" >
                            <option value="">--Select--</option>
                            <?php foreach ($kabkota as $key => $data) { ?>
                            <option value="<?= $data->id ?>"<?= $data->id == $row->kabkota_id ? "selected" : null ?> ><?= $data->name ?></option>
                            <?php } ?>
										      </select>
                      </div>
                      <?php } ?>
                      <?php if ($this->fungsi->user_login()->role_id == 2) { ?>
                      <input type="hidden" name="kabkota_id"  class="form-control" value="<?= $this->session->userdata('kabkota_id')?>" readonly>
                      <div class="form-group" >
                          <label for="kecamatan_id">Kecamatan</label>
                          <select name="kecamatan_id" class="form-control not-dark" id="kecamatan" >
												        <option value="">--Select--</option>
                                 <?php foreach ($kecam as $key => $data) { ?>
                                <option value="<?= $data->id ?>"<?= $data->id == $row->kecamatan_id ? "selected" : null ?> ><?= $data->name ?></option>
                                <?php } ?>
											    </select>
                      </div>
                      <?php } ?>
                      <div class="form-group" id="otherFieldDiv">
                          <label for="desa">Kelurahan</label>
                          <select name="desa" class="form-control not-dark" id="desa" >
												    <option value="">--Select--</option>
                            <?php foreach ($desa as $key => $data) { ?>
                            <option value="<?= $data->id ?>"<?= $data->id == $row->kelurahan_id ? "selected" : null ?> ><?= $data->name ?></option>
                            <?php } ?>
											    </select>
                      </div>
                      <div class="form-group">
                          <label>Alamat</label>
                          <textarea class="form-control" name="alamat" rows="5"  placeholder="Masukkan alamat ..."><?= $row->alamat ?></textarea>
                      </div>
                      
                    </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (left) -->
          <!-- right column -->
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  