<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?=base_url('assets/canvas/')?>css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="<?=base_url('assets/canvas/')?>style.css" type="text/css" />
	<link rel="stylesheet" href="<?=base_url('assets/canvas/')?>css/dark.css" type="text/css" />
	<link rel="stylesheet" href="<?=base_url('assets/canvas/')?>css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="<?=base_url('assets/canvas/')?>css/animate.css" type="text/css" />
	<link rel="stylesheet" href="<?=base_url('assets/canvas/')?>css/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="<?=base_url('assets/canvas/')?>css/responsive.css" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Document Title
	============================================= -->
	<title>Register - GARAM GAYENG</title>

</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap nopadding">

				<div class="section nopadding nomargin" style="width: 100%; height: 100%; position: absolute; left: 0; top: 0; background: url('<?=base_url()?>assets/pic1.jpg') center center no-repeat; background-size: cover;"></div>

				<div class="section nobg full-screen nopadding nomargin">
					<div class="container-fluid vertical-middle divcenter clearfix">
						<div class="card divcenter noradius noborder" style="max-width: 600px; background-color: rgba(255,255,255,0.93);">
							<div class="card-body" style="padding: 40px;">
								<form action="" method="POST">
									<h3 style="color:#C02942">REGISTER GUDANG</h3>
									<div class="row">
										<div class="col">
											<label for="login-form-username">Nama:</label>
											<input type="text" id="" name="nama" class="form-control not-dark" required/>
											<input type="hidden" id="" name="role_id" class="form-control not-dark" value="3"/>
											<input type="hidden" id="" name="status" class="form-control not-dark" value="0"/>
											 <?= form_error('nama', '<div class="text-danger"><small>', '</small></div>') ?>
										</div>
										<div class="col">
											<label for="login-form-username">Email:</label>
											<input type="email" id="" name="email" class="form-control not-dark" required/>
											 <?= form_error('email', '<div class="text-danger"><small>', '</small></div>') ?>
										</div> 
									</div><br>
									<div class="row">
										<div class="col">
										<label for="login-form-password">Password:</label>
										<input type="password" id="" name="password"  class="form-control not-dark"  required/>
										 <?= form_error('password', '<div class="text-danger"><small>', '</small></div>') ?>
										</div>
										<div class="col">
											<label for="login-form-password">Confirm Password:</label>
											<input type="password" id="" name="password2"  class="form-control not-dark"  required/>
											 <?= form_error('password2', '<div class="text-danger"><small>', '</small></div>') ?>
										</div>
									</div><br>
									<div class="col_full">
										<label for="kabkota">Kab/Kota</label>
										<select name="kabkota_id" class="form-control not-dark" id="kota" >
											<option value="">--Select--</option>
											<?php foreach ($kabkota as $key => $data) { ?>
											<option value="<?= $data->id ?>" ><?= $data->name ?></option>
											<?php } ?>
											<!--  -->
										</select>
									</div>
									<div class="row">
										<div class="col">
											<label for="kecamatan">Kecamatan</label>
											<select name="kecamatan_id" class="form-control not-dark" id="kecamatan">
												<option value="">--Select--</option>
											</select>
										</div>
										<div class="col">
											<label for="desa">Kelurahan</label>
											<select name="desa" class="form-control not-dark" id="desa" >
												<option value="">--Select--</option>
												
											</select>
										</div>
									</div><br>
									<div class="col_full nobottommargin">
									<button type="submit" class="button button-3d button-red nomargin" id="" >Daftar</button>
									<a href="<?=base_url('homes/login')?>" class="fright">Login Akun ?</a>
									</div>
									
									
								</form>

								<div class="line line-sm"></div>

								<div class="center dark"><small style="color:black">
								    <h5> <a href="<?=base_url()?>" >Beranda</a><br/></h5>
								    Copyrights &copy; 2021 Dinas Kelautan dan Perikanan<br/> Provinsi Jawa Tengah.</small></div>

								
							</div>
						</div>

						

					</div>
				</div>

			</div>

		</section><!-- #content end -->

    </div><!-- #wrapper end -->
    
    	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

<!-- External JavaScripts
============================================= -->
<script src="<?=base_url('assets/canvas/')?>js/jquery.js"></script>
<script src="<?=base_url('assets/canvas/')?>js/plugins.js"></script>

<!-- Footer Scripts
============================================= -->
<script src="<?=base_url('assets/canvas/')?>js/functions.js"></script>
<script>
        $(document).ready(function() { // Ketika halaman sudah siap (sudah selesai di load)
            // Kita sembunyikan dulu untuk loadingnya
            $("#loading").hide();

            $("#kota").change(function() { // Ketika user mengganti atau memilih data provinsi
                $("#kecamatan").hide(); // Sembunyikan dulu combobox kota nya
		// $("#desa").hide();
                $("#loading").show();
                // Tampilkan loadingnya

                $.ajax({
                    type: "POST", // Method pengiriman data bisa dengan GET atau POST
                    url: "<?php echo base_url("homes/list_sub"); ?>",
                    // Isi dengan url/path file php yang dituju
                    data: {
                        id: $("#kota").val()
                    }, // data yang akan dikirim ke file yang dituju
                    dataType: "json",
                    beforeSend: function(e) {
                        if (e && e.overrideMimeType) {
                            e.overrideMimeType("application/json;charset=UTF-8");
                        }
                    },
                    success: function(response) { // Ketika proses pengiriman berhasil
                        $("#loading").hide();
                        // Sembunyikan loadingnya

                        // set isi dari combobox kota
                        // lalu munculkan kembali combobox kotanya
                        $("#kecamatan").html(response.list_sub).show();

                    },
                    error: function(xhr, ajaxOptions, thrownError) { // Ketika ada error
                        alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
                    }
                });

            });
        });
</script>
    <script>
        $(document).ready(function() { // Ketika halaman sudah siap (sudah selesai di load)
            // Kita sembunyikan dulu untuk loadingnya
            $("#loading2").hide();

            $("#kecamatan").change(function() { // Ketika user mengganti atau memilih data provinsi
                $("#desa").hide(); // Sembunyikan dulu combobox kota nya
                $("#loading2").show(); // Tampilkan loadingnya

                $.ajax({
                    type: "POST", // Method pengiriman data bisa dengan GET atau POST
                    url: "<?php echo base_url("homes/list_desa"); ?>", // Isi dengan url/path file php yang dituju
                    data: {
                        id: $("#kecamatan").val()
                    }, // data yang akan dikirim ke file yang dituju
                    dataType: "json",
                    beforeSend: function(e) {
                        if (e && e.overrideMimeType) {
                            e.overrideMimeType("application/json;charset=UTF-8");
                        }
                    },
                    success: function(response) { // Ketika proses pengiriman berhasil
                        $("#loading2").hide(); // Sembunyikan loadingnya

                        // set isi dari combobox kota
                        // lalu munculkan kembali combobox kotanya
                        $("#desa").html(response.list_desa).show();
                    },
                    error: function(xhr, ajaxOptions, thrownError) { // Ketika ada error
                        alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
                    }
                });
            });
        });
    </script>

</body>
</html>