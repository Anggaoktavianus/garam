<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Tambah Gudang</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Tambah</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Form Tambah Data Gudang </h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="<?= site_url('gudang/add')?>" method="POST">
                <div class="card-body">
                  <p style="color:red;"><small>*Penulisan angka desimal pada kapasitas menggunakan titik (.) Contoh : 10.00</small></p>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group"> 
                        <label for="exampleInputEmail1">Nama Pemilik</label>
                        <input type="text" name="nama" class="form-control" value="<?= set_value('nama') ?>" placeholder="Masukkan Nama Pemilik" required>
                          <?= form_error('nama', '<div class="text-danger"><small>', '</small></div>') ?>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword2">Latitude</label>
                        <input type="text" name="latitude" class="form-control" value="<?= set_value('latitude') ?>" id="exampleInputPassword2" placeholder="latitude" >
                      </div>
                    </div>
                    <div class="col-md-6">  
                      <div class="form-group">
                        <label for="exampleInputEmail1">Kapasitas</label>
                        <input type="text" name="luas" class="form-control" value="<?= set_value('luas') ?>" id="exampleInputEmail1" placeholder="Kapasitas Gudang" required>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword2">Longitude</label>
                        <input type="text" name="longitude" class="form-control" value="<?= set_value('longitude') ?>" id="exampleInputPassword2" placeholder="Longitude" >
                      </div>
                    </div>
                    <div class="col-md-6">
                      
                      <?php if ($this->fungsi->user_login()->role_id == 1) { ?>
                      <div class="form-group" >
                          <label for="kabkota">Kab/Kota</label>
                          <select name="kabkota_id" class="form-control not-dark" id="kota" required>
                            <option value="">--Select--</option>
                            <?php foreach ($kabkota as $key => $data) { ?>
                            <option value="<?= $data->id ?>" ><?= $data->name ?></option>
                            <?php } ?>
										      </select>
                      </div>
                      <?php } ?>
                      <?php if ($this->fungsi->user_login()->role_id == 2) { ?>
                      <input type="hidden" name="kabkota_id"  class="form-control" value="<?= $this->session->userdata('kabkota_id')?>" readonly>
                      <div class="form-group" >
                          <label for="kecamatan_id">Kecamatan</label>
                          <select name="kecamatan_id" class="form-control not-dark" id="kecamatan" required>
												        <option value="">--Select--</option>
                                 <?php foreach ($kecam as $key => $data) { ?>
                                <option value="<?= $data->id ?>" ><?= $data->name ?></option>
                                <?php } ?>
											    </select>
                      </div>
                      <?php } ?>
                      <div class="form-group" id="otherFieldDiv">
                          <label for="desa">Kelurahan</label>
                          <select name="desa" class="form-control not-dark" id="desa" required>
												    <option value="">--Select--</option>
											    </select>
                      </div>
                    </div>
                    
                    <div class="col-md-6">
                      <?php if ($this->fungsi->user_login()->role_id == 1) { ?>
                      <div class="form-group" >
                          <label for="kecamatan_id">Kecamatan</label>
                          <select name="kecamatan_id" class="form-control not-dark" id="kecamatan" required>
												        <option value="">--Select--</option>
											    </select>
                      </div>
                       <?php } ?>
                       <?php if ($this->fungsi->user_login()->role_id == 2) { ?>
                      <div class="form-group" >
                          <label for="koperasi">Koperasi <small>(Optional)</small></label>
                          <select name="koperasi_id" class="form-control not-dark" id="koperasi" >
												    <option value="">--Select--</option>
											    </select>
                      </div>
                      <div class="form-group" >
                          <label for="koperasi">Kelompok <small>(Optional)</small></label>
                          <select name="kelompok_id" class="form-control not-dark" id="kelompok" >
												    <option value="">--Select--</option>
											    </select>
                      </div>
                      <?php } ?>
                      <?php if ($this->fungsi->user_login()->role_id == 1) { ?>
                      <div class="form-group" >
                          <label for="koperasi">Koperasi <small>(Optional)</small> </label>
                          <select name="koperasi_id" class="form-control not-dark" id="koperasi" >
												    <option value="">--Select--</option>
											    </select>
                      </div>
                      <?php } ?>
                    </div>
                    <div class="col-md-6">
                      <?php if ($this->fungsi->user_login()->role_id == 1) { ?>
                      <div class="form-group" >
                          <label for="koperasi">Kelompok <small>(Optional)</small></label>
                          <select name="kelompok_id" class="form-control not-dark" id="kelompok" >
												    <option value="">--Select--</option>
											    </select>
                      </div>
                      <?php } ?>
                      <div class="form-group">
                          <label>Alamat</label>
                          <textarea class="form-control" name="alamat" rows="5" placeholder="Masukkan alamat ..."><?= set_value('alamat')?></textarea>
                      </div>
                    </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (left) -->
          <!-- right column -->
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <script>
        $(document).ready(function() { // Ketika halaman sudah siap (sudah selesai di load)
            // Kita sembunyikan dulu untuk loadingnya
            $("#loading").hide();

            $("#kota").change(function() { // Ketika user mengganti atau memilih data provinsi
                $("#kecamatan").hide(); // Sembunyikan dulu combobox kota nya
		// $("#desa").hide();
                $("#loading").show();
                // Tampilkan loadingnya

                $.ajax({
                    type: "POST", // Method pengiriman data bisa dengan GET atau POST
                    url: "<?php echo base_url("gudang/list_sub"); ?>",
                    // Isi dengan url/path file php yang dituju
                    data: {
                        id: $("#kota").val()
                    }, // data yang akan dikirim ke file yang dituju
                    dataType: "json",
                    beforeSend: function(e) {
                        if (e && e.overrideMimeType) {
                            e.overrideMimeType("application/json;charset=UTF-8");
                        }
                    },
                    success: function(response) { // Ketika proses pengiriman berhasil
                        $("#loading").hide();
                        // Sembunyikan loadingnya

                        // set isi dari combobox kota
                        // lalu munculkan kembali combobox kotanya
                        $("#kecamatan").html(response.list_sub).show();

                    },
                    error: function(xhr, ajaxOptions, thrownError) { // Ketika ada error
                        alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
                    }
                });

            });
        });
</script>
    <script>
        $(document).ready(function() { // Ketika halaman sudah siap (sudah selesai di load)
            // Kita sembunyikan dulu untuk loadingnya
            $("#loading2").hide();

            $("#kecamatan").change(function() { // Ketika user mengganti atau memilih data provinsi
                $("#desa").hide(); // Sembunyikan dulu combobox kota nya
                $("#loading2").show(); // Tampilkan loadingnya

                $.ajax({
                    type: "POST", // Method pengiriman data bisa dengan GET atau POST
                    url: "<?php echo base_url("gudang/list_desa"); ?>", // Isi dengan url/path file php yang dituju
                    data: {
                        id: $("#kecamatan").val()
                    }, // data yang akan dikirim ke file yang dituju
                    dataType: "json",
                    beforeSend: function(e) {
                        if (e && e.overrideMimeType) {
                            e.overrideMimeType("application/json;charset=UTF-8");
                        }
                    },
                    success: function(response) { // Ketika proses pengiriman berhasil
                        $("#loading2").hide(); // Sembunyikan loadingnya

                        // set isi dari combobox kota
                        // lalu munculkan kembali combobox kotanya
                        $("#desa").html(response.list_desa).show();
                    },
                    error: function(xhr, ajaxOptions, thrownError) { // Ketika ada error
                        alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
                    }
                });
            });
        });
    </script>
  