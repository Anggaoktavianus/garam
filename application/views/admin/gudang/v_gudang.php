<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Data Gudang Garam Rakyat</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Gudang </li>
          </ol>
        </div>
      </div>
      <?php
        $info= $this->session->flashdata('info');
        $pesan= $this->session->flashdata('pesan');
        if($info == 'success'){ ?>
          <div class="alert alert-success">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <i class="icon-gift"></i><strong><?=$info?></strong> <?=$pesan?>.
            </div>
        <?php    
        }elseif($info == 'danger'){ ?>
          <div class="alert alert-danger">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <i class="icon-gift"></i><strong><?=$info?></strong> <?=$pesan?>.	
          </div>
        <?php  }else{ } ?>
    </div><!-- /.container-fluid -->
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Tabel Gudang Garam Rakyat </h3><br><br>
              <a href="<?= site_url('gudang/add_index')?>" class="btn btn-primary btn-sm"><i class="fas fa-plus"></i>&nbsp; Tambah Data</a>
              <a href="<?= site_url('filter')?>" class="btn btn-secondary btn-sm"><i class="fas fa-file-excel"></i>&nbsp; Import Data</a>
            </div>
            <!-- <font color="green"><?php echo $this->session->flashdata('pesan'); ?></font> -->
            <!-- /.card-header -->
            <div class="card-body">
              <div class="table-responsive">
                <!-- ADMIN GUDANG -->
                <?php if ($this->fungsi->user_login()->role_id == 1) { ?>
                <table id="example" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Pemilik</th>
                    <th>Kapasitas(Ton)</th>
                    <th>Alamat</th>
                    <th>Koordinat</th>
                    <th>Koperasi</th>
                    <th>Kelompok</th>
                    <!-- <th>Longitude</th> -->
                    <th>Action</th>
                  </tr>
                  </thead> 
                  <tbody>
                  
                  </tbody>
                  <tfoot>
                  <tr>
                    <th>No</th>
                    <th>Nama Pemilik</th>
                    <th>Kapasitas(Ton)</th>
                    <th>Alamat</th>
                    <th>Koordinat</th>
                    <th>Koperasi</th>
                    <th>Kelompok</th>
                    <!-- <th>Longitude</th> -->
                    <th>Action</th>
                  </tr>
                  </tfoot>
                </table>
                <?php } ?>
                <?php if ($this->fungsi->user_login()->role_id == 2) { ?>
                <table id="kota" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Pemilik</th>
                    <th>Kapasitas(Ton)</th>
                    <th>Alamat</th>
                    <th>Koordinat</th>
                    <th>Koperasi</th>
                    <th>Kelompok</th>
                    <!-- <th>Longitude</th> -->
                    <th>Action</th>
                  </tr>
                  </thead> 
                  <tbody>
                  
                  </tbody>
                  <tfoot>
                  <tr>
                    <th>No</th>
                    <th>Nama Pemilik</th>
                    <th>Kapasitas(Ton)</th>
                    <th>Alamat</th>
                    <th>Koordinat</th>
                    <th>Koperasi</th>
                    <th>Kelompok</th>
                    <!-- <th>Longitude</th> -->
                    <th>Action</th>
                  </tr>
                  </tfoot>
                </table>
                <?php } ?>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- <script> 
    $(document).ready(function(){
        var table = $('#example').DataTable({
        processing: true,
        serverSide: true,
        responsive: true, 
        lengthChange: false, 
        stateSave: true,
        autoWidth: false,
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        dom: '<"html5buttons">Bfrtip',
        language: {
                buttons: {
                    colvis : 'Show/Hide', // label button show / hide
                    colvisRestore: "Reset Kolom", //lael untuk reset kolom ke default
                    columns: '0,1,2,3,4,5,6'
                }
        },
        
        buttons : [
                    {extend: 'colvis', postfixButtons: [ 'colvisRestore' ] },
                    {extend: 'pdf', title:'Data Gudang'},
                    {extend: 'excel', title: 'Data Gudang'},
                    {extend:'print',title: 'Data Gudang'},
        ],
        ajax: {
                "url": "<?= site_url('gudang/get_ajax') ?>",
                "type": "POST"
            },
    });
    });
</script> -->
<script>
  $(function () {
    $("#example").DataTable({
      "processing": true,
      "searching": true,
      "responsive": true, 
      "lengthChange": true,
      "stateSave": true, 
      "autoWidth": false,
      "serverSide": true,
      "ajax": {
                "url": "<?= site_url('gudang/get_ajax') ?>",
                "type": "POST"
            },
      dom: '<"html5buttons">Bfrtip',
      'lengthMenu' : [
                        [ 10, 25, 50, -1 ],
                        [ '10 rows', '25 rows', '50 rows', 'Show all' ]
                ],   
      buttons: [
            
            {
                extend: 'excelHtml5', 
                title: 'Data Gudang',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                }
            },
            {
                extend: 'pdfHtml5',
                title: 'Data Gudang',
                orientation: 'potrait',
                pageSize: 'LEGAL',
                width: 'auto',
                exportOptions: {
                 
                    columns: [ 0, 1, 2, 3, 4, 5, 6 ],
                    
                }
                
            },
            {
                extend: 'pageLength',
                title: 'Data Gudang',
                exportOptions: {
                    columns: ':visible'
                }
            },
        ]
       
    }).buttons().container().appendTo('#example_wrapper .col-md-6:eq(0)');
  });
</script>
<script>
  $(function () {
    $("#kota").DataTable({
      "processing": true,
      "searching": true,
      "responsive": true, 
      "lengthChange": true,
      "stateSave": true, 
      "autoWidth": false,
      "serverSide": true,
      "ajax": {
                "url": "<?= site_url('gudang/get_ajax_kota') ?>",
                "type": "POST"
            },
      dom: '<"html5buttons">Bfrtip',
      'lengthMenu' : [
                        [ 10, 25, 50, -1 ],
                        [ '10 rows', '25 rows', '50 rows', 'Show all' ]
                ],   
      buttons: [
            
            {
                extend: 'excelHtml5', 
                title: 'Data Gudang',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                }
            },
            {
                extend: 'pdfHtml5',
                title: 'Data Gudang',
                orientation: 'potrait',
                pageSize: 'LEGAL',
                width: 'auto',
                exportOptions: {
                 
                    columns: [ 0, 1, 2, 3, 4, 5, 6 ],
                    
                }
                
            },
            {
                extend: 'pageLength',
                title: 'Data Gudang',
                exportOptions: {
                    columns: ':visible'
                }
            },
        ]
       
    }).buttons().container().appendTo('#kota_wrapper .col-md-6:eq(0)');
  });
</script>

