<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Koperasi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Koperasi </li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Import</h3><br><br>
                <div class="form-group">
                  <div class="col-md-4">
                                        <div class="form-group" >
                                          <label for="kecamatan_id">Kota</label>
                                          <select name="kabkota" class="form-control not-dark" id="kota">
                        <option value="">--Select--</option>
                        <?php foreach ($kabkotas as $key => $data) { ?>
                                          <option value="<?= $data->id ?>" ><?= $data->name ?></option>
                                          <?php } ?>
                        </select>
                                  </div>
                                    </div>
                   <form method="get" action="<?php echo base_url("gudang/filter")?>">
                    <div class="row">
                        
                        <div class="col-md-4">
                      <div class="form-group" >
                        <label for="kecamatan_id">Kecamatan</label>
                        <select name="ditrict_id" class="form-control not-dark kecamatans" id="kecamatan">
                          <option value="">--Select--</option>
                        </select>
                      </div>
                                    </div>
                        <div class="col-md-4"> 
                      <div class="form-group" >
                        <label for="desa">Kelurahan</label>
                        <select name="id" class="form-control not-dark kabkota" id="desa" >
                          <option value="">--Select--</option>
                        </select>
                      </div>
                                    </div>
                                      <br><input type="submit" class="btn btn-primary" value="Cari">
                    </div>
                    <div class="card-body">
                <div class="table-responsive">
                <table id="example1" class="table table-bordered table-striped" >
                  <thead>
                      <tr>
                          <th>No</th>
                          <th>Kode</th>
                          <th>Kabupaten</th>
                          <th>Kode</th>
                          <th>Kecamatan</th>
                          <th>Kode</th>
                          <th>Kelurahan/desa</th>
                      </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1;
                            foreach ($filter as $key => $data) {
                          ?>
                        <tr>
                          <td><?= $no++ ?></td>
                              <td><?= $data->id_prov ?></td>
                              <td><?= $data->prov ?></td>
                              <td><?= $data->id_kel?></td>
                              <td><?= $data->district_id ?></td>
                              <td><?= $data->id ?></td>
                              <td><?= $data->name?></td>
                              
                              </td>
                        </tr>
                        <?php
                          } ?>
                  </tbody>
                  <!-- <tfoot>
                      <tr>
                  <th>No</th>
                          <th>Kabupaten</th>
                          <th>Kecamatan</th>
              <th>Kelurahan/desa</th>
                          
                      </tr>
                  </tfoot> -->
              </table>
                </div>
                
              </div>
                   </form>
                    </div>
              </div>
		
              </div>
             
              <!-- <font color="green"><?php echo $this->session->flashdata('pesan'); ?></font> -->
              <!-- /.card-header -->
              
              
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<!-- <script type="text/javascript">
	$(document).ready(function() {
		
	    $('#tabelData').DataTable(
		    {
		// "responsive": true, 
		"lengthChange": false, 
		"autoWidth": false,
		"buttons": ["excel"]
		}).buttons().container().appendTo('#tabelData_wrapper .col-md-6:eq(0)'

	    );
	    
	    function filterData () {
		    $('#tabelData').DataTable().search(
		        $('.kabkota').val(),
		    	).draw();
		}
		$("#tabelData").hide();
		$('.kabkota').on('change', function () {
	        filterData();
		$("#tabelData").show();
		
	    });
	    
	});
</script> -->
<script>
        $(document).ready(function() { // Ketika halaman sudah siap (sudah selesai di load)
            // Kita sembunyikan dulu untuk loadingnya
            $("#loading").hide();

            $("#kota").change(function() { // Ketika user mengganti atau memilih data provinsi
                $("#kecamatan").hide(); // Sembunyikan dulu combobox kota nya
		// $("#desa").hide();
                $("#loading").show();
                // Tampilkan loadingnya

                $.ajax({
                    type: "POST", // Method pengiriman data bisa dengan GET atau POST
                    url: "<?php echo base_url("gudang/list_sub"); ?>",
                    // Isi dengan url/path file php yang dituju
                    data: {
                        id: $("#kota").val()
                    }, // data yang akan dikirim ke file yang dituju
                    dataType: "json",
                    beforeSend: function(e) {
                        if (e && e.overrideMimeType) {
                            e.overrideMimeType("application/json;charset=UTF-8");
                        }
                    },
                    success: function(response) { // Ketika proses pengiriman berhasil
                        $("#loading").hide();
                        // Sembunyikan loadingnya

                        // set isi dari combobox kota
                        // lalu munculkan kembali combobox kotanya
                        $("#kecamatan").html(response.list_sub).show();

                    },
                    error: function(xhr, ajaxOptions, thrownError) { // Ketika ada error
                        alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
                    }
                });

            });
        });
</script>
<script>
    $(document).ready(function() { // Ketika halaman sudah siap (sudah selesai di load)
        // Kita sembunyikan dulu untuk loadingnya
        $("#loading2").hide();

        $("#kecamatan").change(function() { // Ketika user mengganti atau memilih data provinsi
            $("#desa").hide(); // Sembunyikan dulu combobox kota nya
            $("#loading2").show(); // Tampilkan loadingnya

            $.ajax({
                type: "POST", // Method pengiriman data bisa dengan GET atau POST
                url: "<?php echo base_url("gudang/list_desa"); ?>", // Isi dengan url/path file php yang dituju
                data: {
                    id: $("#kecamatan").val()
                }, // data yang akan dikirim ke file yang dituju
                dataType: "json",
                beforeSend: function(e) {
                    if (e && e.overrideMimeType) {
                        e.overrideMimeType("application/json;charset=UTF-8");
                    }
                },
                success: function(response) { // Ketika proses pengiriman berhasil
                    $("#loading2").hide(); // Sembunyikan loadingnya

                    // set isi dari combobox kota
                    // lalu munculkan kembali combobox kotanya
                    $("#desa").html(response.list_desa).show();
                },
                error: function(xhr, ajaxOptions, thrownError) { // Ketika ada error
                    alert(xhr.status + "\n" + xhr.responseText + "\n" + thrownError); // Munculkan alert error
                }
            });
        });
    });
</script>

