<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />

	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?=base_url('assets/canvas/')?>css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="<?=base_url('assets/canvas/')?>style.css" type="text/css" />
	<link rel="stylesheet" href="<?=base_url('assets/canvas/')?>css/dark.css" type="text/css" />
	<link rel="stylesheet" href="<?=base_url('assets/canvas/')?>css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="<?=base_url('assets/canvas/')?>css/animate.css" type="text/css" />
	<link rel="stylesheet" href="<?=base_url('assets/canvas/')?>css/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="<?=base_url('assets/canvas/')?>css/responsive.css" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Document Title
	============================================= -->
	<title>Login - GARAM GAYENG</title>

</head>

<body class="stretched">

	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap nopadding">

				<div class="section nopadding nomargin" style="width: 100%; height: 100%; position: absolute; left: 0; top: 0; background: url('<?=base_url()?>assets/pic1.jpg') center center no-repeat; background-size: cover;"></div>

				<div class="section nobg full-screen nopadding nomargin">
					<div class="container-fluid vertical-middle divcenter clearfix">
						<div class="card divcenter noradius noborder" style="max-width: 400px; background-color: rgba(255,255,255,0.93);">
							<div class="card-body" style="padding: 40px;">
								<!-- <font color="green"><?php echo $this->session->flashdata('pesan'); ?></font> -->
								
								<form action="<?= site_url('login/login_proses') ?>" method="post">
									<h3 style="color:#C02942">LOGIN</h3>

									<?php
										$info= $this->session->flashdata('info');
										$pesan= $this->session->flashdata('pesan');

										if($info == 'success'){ ?>
											<div class="alert alert-success">
											<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
											<i class="icon-gift"></i><strong><?=$info?></strong> <?=$pesan?>.
											</div>
										<?php    
										}elseif($info == 'danger'){ ?>
											<div class="alert alert-danger">
											<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
											<i class="icon-gift"></i><strong><?=$info?></strong> <?=$pesan?>.	
											</div>
									<?php  }else{ } ?>

									<div class="col_full">
										<label for="login-form-username">Email:</label>
										<input type="email" id="" name="email" class="form-control not-dark" required/>
										<?php echo form_error('email', '<div class="text-danger"><small>', '</small></div>');?>
									</div>

									<div class="col_full">
										<label for="login-form-password">Password:</label>
										<input type="password" id="" name="password"  class="form-control not-dark"  required/>
										<?php echo form_error('password', '<div class="text-danger"><small>', '</small></div>');?>
									</div>

									<div class="col_full nobottommargin">
									<button type="submit" class="button button-3d button-red nomargin" id="" >Login</button>
									<a href="<?=base_url('homes/register_proses')?>" class="fright">Daftar Akun</a>
									
									</div>
									
									
								</form>

								<div class="line line-sm"></div>

								<div class="center dark"><small style="color:black">
								    <h5> <a href="<?=base_url()?>" >Beranda</a><br/></h5>
								    Copyrights &copy; 2021 Dinas Kelautan dan Perikanan<br/> Provinsi Jawa Tengah.</small></div>

								
							</div>
						</div>

						

					</div>
				</div>

			</div>

		</section><!-- #content end -->

    </div><!-- #wrapper end -->
    
    	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

<!-- External JavaScripts
============================================= -->
<script src="<?=base_url('assets/canvas/')?>js/jquery.js"></script>
<script src="<?=base_url('assets/canvas/')?>js/plugins.js"></script>

<!-- Footer Scripts
============================================= -->
<script src="<?=base_url('assets/canvas/')?>js/functions.js"></script>

</body>
</html>