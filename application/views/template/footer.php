<!-- Footer
		============================================= -->
		<footer id="footer" class="dark" style="background-color:#C02942">

			<div class="container clearfix">

					<div class="col_half" style="color:#fff">
						Copyrights &copy; 2021 All Rights Reserved by Dinas Kelautan dan Perikanan Provinsi Jawa Tengah.
						<!-- <div class="copyright-links"><a href="#">Terms of Use</a> / <a href="#">Privacy Policy</a></div> -->
					</div>
			</div>

			<div class="col_half col_last tright">
						Pengunjung Web :
					    <a href="https://www.hitwebcounter.com" target="_blank">
<img src="https://hitwebcounter.com/counter/counter.php?page=7851981&style=0006&nbdigits=5&type=page&initCount=0" title="Free Counter" Alt="web counter"   border="0" /></a>   
						<div class="fright clearfix" >
							&nbsp;&nbsp;-&nbsp;&nbsp;Versi 1.0
						</div>

			</div>
			<!-- Copyrights
			============================================= -->
			<div id="copyrights">

				
			</div><!-- #copyrights end -->

		</footer><!-- #footer end -->

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- External JavaScripts
	============================================= -->
	<script src="<?=base_url('assets/canvas/')?>js/jquery.js"></script>
	<script src="<?=base_url('assets/canvas/')?>js/plugins.js"></script>
	<script src="<?=base_url('assets/contact/js/popper.js')?>"></script>

	<!-- Footer Scripts
	============================================= -->
	<script src="<?=base_url('assets/canvas/')?>js/functions.js"></script>
	<script src="<?=base_url();?>/assets/adminlte/bower_components/select2/js/select2.min.js"></script>
	<!-- ChartJS -->
	<script src="<?= base_url('assets/plugins/chart.js/Chart.min.js')?>"></script>
	<!-- datatables -->
	<script src="<?= base_url('assets/plugins/datatables/jquery.dataTables.min.js')?>"></script>
	<script src="<?= base_url('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')?>"></script>
	<script src="<?= base_url('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js')?>"></script>
	<script src="<?= base_url('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')?>"></script>
	<script src="<?= base_url('assets/plugins/datatables-buttons/js/dataTables.buttons.min.js')?>"></script>
	<script src="<?= base_url('assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js')?>"></script>
	<script src="<?= base_url('assets/plugins/jszip/jszip.min.js')?>"></script>
	<script src="<?= base_url('assets/plugins/pdfmake/pdfmake.min.js')?>"></script>
	<script src="<?= base_url('assets/plugins/pdfmake/vfs_fonts.js')?>"></script>
	<script src="<?= base_url('assets/plugins/datatables-buttons/js/buttons.html5.min.js')?>"></script>
	<script src="<?= base_url('assets/plugins/datatables-buttons/js/buttons.print.min.js')?>"></script>
	<script src="<?= base_url('assets/plugins/datatables-buttons/js/buttons.colVis.min.js')?>"></script>