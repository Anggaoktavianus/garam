<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Astama Technology" />
	<link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('assets/dist/img/jateng.png')?>">
	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<?=base_url('assets/canvas/')?>css/bootstrap.css" type="text/css" />
	<link rel="stylesheet" href="<?=base_url('assets/canvas/')?>style.css" type="text/css" />
	<link rel="stylesheet" href="<?=base_url('assets/canvas/')?>css/swiper.css" type="text/css" />
	<link rel="stylesheet" href="<?=base_url('assets/canvas/')?>css/dark.css" type="text/css" />
	<link rel="stylesheet" href="<?=base_url('assets/canvas/')?>css/font-icons.css" type="text/css" />
	<link rel="stylesheet" href="<?=base_url('assets/canvas/')?>css/animate.css" type="text/css" />
	<link rel="stylesheet" href="<?=base_url('assets/canvas/')?>css/magnific-popup.css" type="text/css" />

	<link rel="stylesheet" href="<?=base_url('assets/canvas/')?>css/responsive.css" type="text/css" />
	<!-- <link rel="stylesheet" href="<?=base_url();?>/assets/adminlte/bower_components/select2/css/select2.min.css" type="text/css" />
	<script src="<?=base_url();?>/assets/adminlte/bower_components/select2/js/select2.min.js"></script> -->

	<!-- Contact us -->
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	
	<link rel="stylesheet" href="<?= base_url('assets/contact/css/style.css')?>">
	<!-- datatables -->
	<link rel="stylesheet" href="<?= base_url('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')?>">
	<link rel="stylesheet" href="<?= base_url('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')?>">
	<link rel="stylesheet" href="<?= base_url('assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')?>">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" />
  	<script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>
	<!-- Document Title
	============================================= -->
	<title>Garam Gayeng</title>

</head>