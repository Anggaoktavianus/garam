<body class="stretched">
	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">
		<!-- Header============================================= -->
		<header id="header" class=" dark">

			<div id="header-wrap">

				<div class="container clearfix">

					<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
					<!-- Logo
					============================================= -->
					<div id="logo">
						<a href="<?=base_url()?>" class="standard-logo" data-dark-logo="<?=base_url('assets/canvas/')?>images/ggm1.png"><img src="<?=base_url('assets/canvas/')?>images/ggm1.png" alt="Canvas Logo"></a>
						<a href="<?=base_url()?>" class="retina-logo" data-dark-logo="<?=base_url('assets/canvas/')?>images/ggm1.png"><img src="<?=base_url('assets/canvas/')?>images/ggm1.png" alt="Canvas Logo"></a>
					</div>
					<!-- #logo end -->
					<!-- Primary Navigation
					============================================= -->
					<nav id="primary-menu">
						<ul>
							<!-- <li><a href="<?=base_url()?>"><div >Beranda</div></a></li> -->
							<!-- <li><a href="#tentang"><div >Tentang</div></a></li> -->
							<!--<li><a href="#dasarhukum"><div >Panduan</div></a></li>-->
							<li><a href="<?=base_url('homes/lahan')?>"><div >Lahan</div></a></li>
							<li><a href="<?=base_url('homes/produksi')?>"><div >Produksi</div></a></li>
							<li><a href="<?=base_url('homes/stock')?>"><div >Stock</div></a></li>
							<li class="menu-item">
									<a class="menu-link" href="#"><div>Data Lain</div></a>
									<ul class="sub-menu-container">
										<!-- <li class="menu-item">
											<a class="menu-link" href="#dasarhukum"><div>Data Lahan</div></a>
										</li>
										<li class="menu-item">
											<a class="menu-link" href="<?=base_url('homes/perusahaan')?>"><div>Data Produksi Garam</div></a>
										</li>
										<li class="menu-item">
											<a class="menu-link" href="<?=base_url('homes/desa/merah')?>"><div>Desa Stock Garam</div></a>
										</li> -->
										<li class="menu-item">
											<a class="menu-link" href="<?=base_url('homes/gudang')?>"><div>Data Gudang</div></a>
										</li>
										<li class="menu-item">
											<a class="menu-link" href="<?=base_url('homes/kelompok')?>"><div>Data Kelompok</div></a>
										</li>
										<li class="menu-item">
											<a class="menu-link" href="<?=base_url('homes/koperasi')?>"><div>Data Koperasi</div></a>
										</li>
										
									</ul>
								</li>
								<li><a href="<?=base_url('homes/contact')?>"><div >Contact</div></a></li>
							<li><a href="<?=base_url('homes/login/')?>"><div >Login</div></a></li>
							<!--<li><a href="<?=base_url('welcome/register/')?>"><div >Register</div></a></li>-->
						</ul>
					</nav>
					<!-- #primary-menu end -->
				</div>
			</div>
		</header><!-- #header end -->