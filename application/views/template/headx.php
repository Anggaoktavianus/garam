<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Garam Gayeng</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url('assets/plugins/fontawesome-free/css/all.min.css')?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="<?= base_url('assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') ?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?= base_url('assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css')?>">
  <!-- JQVMap -->
  <!-- <link rel="stylesheet" href="<?= base_url('assets/plugins/jqvmap/jqvmap.min.css')?>"> -->
  <!-- datatables -->
  <link rel="stylesheet" href="<?= base_url('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')?>">
  <link rel="stylesheet" href="<?= base_url('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')?>">
  <link rel="stylesheet" href="<?= base_url('assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url('assets/dist/css/adminlte.min.css')?>">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?= base_url('assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')?>">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?= base_url('assets/plugins/daterangepicker/daterangepicker.css')?>">
  <!-- summernote -->
  <link rel="stylesheet" href="<?= base_url('assets/plugins/summernote/summernote-bs4.min.css')?>">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Preloader -->
  <!-- <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__shake" src="<?= base_url('assets/dist/img/AdminLTELogo.png')?>" alt="AdminLTELogo" height="60" width="60">
  </div> -->

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index3.html" class="nav-link">Home</a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Navbar Search -->
      <li class="nav-item">
        <a class="nav-link" data-widget="navbar-search" href="#" role="button">
          <i class="fas fa-search"></i>
        </a>
        <div class="navbar-search-block">
          <form class="form-inline">
            <div class="input-group input-group-sm">
              <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
              <div class="input-group-append">
                <button class="btn btn-navbar" type="submit">
                  <i class="fas fa-search"></i>
                </button>
                <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                  <i class="fas fa-times"></i>
                </button>
              </div>
            </div>
          </form>
        </div>
      </li>
      <?php 
        foreach($hitung as $row):
        $total = $row['total'];
        ?>
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-bell"></i>
          <span class="badge badge-warning navbar-badge"><?= $total?></span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header"><?= $total?> Notifications</span>
          <div class="dropdown-divider"></div>
          <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#test1">Open Modal 1 </button> -->
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item" data-toggle="modal" data-target="#test1">
            <i class="fas fa-users mr-2"></i> <?= $total?> permintaan aktivasi
            <span class="float-right text-muted text-sm">12 hours</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-file mr-2"></i> 3 new reports
            <span class="float-right text-muted text-sm">2 days</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
        </div>
      </li>
      <?php endforeach; ?>
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-user"></i>
          <span class="badge badge-warning navbar-badge"></span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header">Profile</span>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-envelope mr-2"></i> <?= $this->session->userdata('email')?>
            <span class="float-right text-muted text-sm"></span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-user mr-2"></i> <?= $this->session->userdata('nama')?>
            <span class="float-right text-muted text-sm"></span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="<?=site_url('login/logout')?>" class="dropdown-item">
            <i class="fas fa-power-off mr-2"></i> Logout
            <span class="float-right text-muted text-sm"></span>
          </a>
          <div class="dropdown-divider"></div>
          
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
          <i class="fas fa-th-large"></i>
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="<?= base_url('assets/dist/img/jateng.png')?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Garam Gayeng</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?= base_url('assets/dist/img/user2-160x160.jpg')?>" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?= $this->session->userdata('nama')?></a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="<?= site_url('dashboard')?>" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <i class="right fas fa-angle"></i>
              </p>
            </a>
          </li>
          <?php if ($this->fungsi->user_login()->role_id == 1  ) { ?>
          <li class="nav-item">
            <a href="pages/widgets.html" class="nav-link">
              <i class="nav-icon fas fa-map-marker-alt"></i>
              <p>
                Data Lahan
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>
	        <li class="nav-item">
            <a href="pages/widgets.html" class="nav-link">
              <i class="nav-icon fas fa-list"></i>
              <p>
                Data Produksi
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>
	        <li class="nav-item">
            <a href="pages/widgets.html" class="nav-link">
              <i class="nav-icon fas fa-chart-bar"></i>
              <p>
                Data Stok Garam
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>
	        <li class="nav-item">
            <a href="pages/widgets.html" class="nav-link">
              <i class="nav-icon fas fa-user-friends"></i>
              <p>
                Data Penambak
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>
	        <li class="nav-item">
            <a href="pages/widgets.html" class="nav-link">
              <i class="nav-icon fas fa-object-group"></i>
              <p>
                Data Kelompok
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>
	        <li class="nav-item">
            <a href="pages/widgets.html" class="nav-link">
              <i class="nav-icon fas fa-pallet"></i>
              <p>
                Data Koperasi
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>
	        <li class="nav-item">
            <a href="<?= site_url('gudang') ?>" class="nav-link">
              <i class="nav-icon fas fa-warehouse"></i>
              <p>
                Data Gudang
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>
           <li class="nav-item">
            <a href="<?= site_url('')?>" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Laporan
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>
          
	        <li class="nav-item ">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Setting
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= site_url('user')?>" class="nav-link">
                  <i class="nav-icon fas fa-user"></i>
                  <p>
                    Management User
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="./index2.html" class="nav-link">
                  <i class="nav-icon fas fa-newspaper"></i>
                  <p>Berita</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="./index3.html" class="nav-link">
                  <i class="nav-icon fas fa-image"></i>
                  <p>Galeri</p>
                </a>
              </li>
            </ul>
          </li>
            <?php } ?>
            <?php if ($this->fungsi->user_login()->role_id == 2  ) { ?>
          <li class="nav-item">
            <a href="pages/widgets.html" class="nav-link">
              <i class="nav-icon fas fa-map-marker-alt"></i>
              <p>
                Data Lahan
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>
	        <li class="nav-item">
            <a href="pages/widgets.html" class="nav-link">
              <i class="nav-icon fas fa-list"></i>
              <p>
                Data Produksi
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>
	        <li class="nav-item">
            <a href="pages/widgets.html" class="nav-link">
              <i class="nav-icon fas fa-chart-bar"></i>
              <p>
                Data Stok Garam
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>
	        <li class="nav-item">
            <a href="pages/widgets.html" class="nav-link">
              <i class="nav-icon fas fa-user-friends"></i>
              <p>
                Data Penambak
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>
	        <li class="nav-item">
            <a href="pages/widgets.html" class="nav-link">
              <i class="nav-icon fas fa-object-group"></i>
              <p>
                Data Kelompok
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>
	        <li class="nav-item">
            <a href="pages/widgets.html" class="nav-link">
              <i class="nav-icon fas fa-pallet"></i>
              <p>
                Data Koperasi
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>
	        <li class="nav-item">
            <a href="<?= site_url('gudang') ?>" class="nav-link">
              <i class="nav-icon fas fa-warehouse"></i>
              <p>
                Data Gudang
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>
           <li class="nav-item">
            <a href="<?= site_url('')?>" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Laporan
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>
           <li class="nav-item ">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Setting
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= site_url('user')?>" class="nav-link">
                  <i class="nav-icon fas fa-user"></i>
                  <p>
                    Management User
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="./index2.html" class="nav-link">
                  <i class="nav-icon fas fa-newspaper"></i>
                  <p>Berita</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="./index3.html" class="nav-link">
                  <i class="nav-icon fas fa-image"></i>
                  <p>Galeri</p>
                </a>
              </li>
            </ul>
          </li>
          
	       
            <?php } ?>
            <?php if ($this->fungsi->user_login()->role_id == 3) { ?>
            <li class="nav-item">
            <a href="pages/widgets.html" class="nav-link">
              <i class="nav-icon fas fa-chart-bar"></i>
              <p>
                Data Stok Garam
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>
          <?php } ?>
	        <li class="nav-item">
          <!-- <li class="nav-item">
            <a href="<?= site_url('login/logout')?>" class="nav-link">
              <i class="nav-icon fas fa-power-off"></i>
              <p>
                Logout
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li> -->
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

<!-- MODAL AKTIVASI AKUN -->
<div id="test1" class="modal fade" role="dialog" style="z-index: 1400;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Ini adalah Bagian Header Modal -->
            <div class="modal-header">
                <h4 class="modal-title">Akun belum teraktivasi</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Ini adalah Bagian Body Modal -->
      <div class="modal-body">
              <div class="table-responsive">
                <table id="example2" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Level</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1;
                      foreach ($aktif as $key => $data) {
                    ?>
                  <tr>
                    <td><?= $no++ ?></td>
                        <td><?= $data->nama ?></td>
                        <td><?= $data->email ?></td>
                        <td><?= $data->role ?></td>
                        <td>
                          <form action="<?= site_url('user/aktivasi') ?>" method="post">
                          <input type="hidden" name="status" value="1">
                          <input type="hidden" name="id" value="<?= $data->id ?>">
                          <button onclick="return confirm('Apakah anda yakin aktivasi user ini?')" class="btn btn-danger btn-sm text-white">
                          Aktivasi
                          </button>
                        </form>
                      </td>
                  </tr>
                   <?php
                    } ?>
                 
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
      </div>      
    </div>
  </div>
</div>
 
  