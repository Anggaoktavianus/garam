

    <!-- Bootstrap Data Table Plugin -->
<link rel="stylesheet" href="<?=base_url('assets/canvas/')?>css/components/bs-datatable.css" type="text/css" />

	<section id="slider" class="slider-element slider-parallax swiper_wrapper full-screen clearfix">

		<div class="slider-parallax-inner">

			<div class="swiper-container swiper-parent">
				<div class="swiper-wrapper">
					<div class="swiper-slide dark" style="background-image: url('assets/slide3.png'); width:100%">
						<div class="container clearfix">
							<div class="slider-caption slider-caption-center">
								<!-- <h2 data-animate="fadeInUp" style="color:#C02942">Selamat Datang</h2>
								<p class="d-none d-sm-block" data-animate="fadeInUp" data-delay="200" style="color:#C02942"><b>Sistem Pelaporan CSR - Provinsi Jawa Tengah</b></p> -->
							</div>
						</div>
					</div>
					
					<div class="swiper-slide dark" style="background-image: url(<?= site_url('assets/slide22.png')?>); background-position: fixed;">
						<div class="container clearfix">
							<div class="slider-caption">
								<!-- <h2 data-animate="fadeInUp" style="color:#C02942">SILAP CSR</h2>
								<p class="d-none d-sm-block" data-animate="fadeInUp" data-delay="200" style="color:#C02942">BIRO KESRA SETDA PROVINSI JAWA TENGAH</p> -->
							</div>
						</div>
					</div>
					
					<div class="swiper-slide dark" style="background-image: url(<?= site_url('assets/slide1.png')?>); background-position: fixed;">
						<div class="container clearfix">
							<div class="slider-caption">
								<!-- <h2 data-animate="fadeInUp" style="color:#C02942">SILAP CSR</h2>
								<p class="d-none d-sm-block" data-animate="fadeInUp" data-delay="200" style="color:#C02942">BIRO KESRA SETDA PROVINSI JAWA TENGAH</p> -->
							</div>
						</div>
					</div>
					
					<!-- <div class="swiper-slide dark" style="background-image: url(<?= site_url('assets/slide4.jpeg')?>); background-position: center top;">
						<div class="container clearfix">
							<div class="slider-caption">
								<h2 data-animate="fadeInUp" style="color:#C02942">SILAP CSR</h2>
								<p class="d-none d-sm-block" data-animate="fadeInUp" data-delay="200" style="color:#C02942">BIRO KESRA SETDA PROVINSI JAWA TENGAH</p>
							</div>
						</div>
					</div> -->
				</div>
				<div class="slider-arrow-left"><i class="icon-angle-left"></i></div>
				<div class="slider-arrow-right"><i class="icon-angle-right"></i></div>
				<div class="slide-number"><div class="slide-number-current"></div><span>/</span><div class="slide-number-total"></div></div>
			</div>

		</div>

	</section>

	<!-- Content
	============================================= -->
	<section id="content">

		<div class="content-wrap" id="tentang">

			<div class="promo promo-full promo-border header-stick bottommargin-lg" >
				<div class="container clearfix">
					<h3>Dinas Kelautan Dan Perikanan Provinsi Jawa Tengah</h3> &nbsp;&nbsp;
					<!-- <span>We strive to provide Our Customers with Top Notch Support to make their Theme Experience Wonderful</span> -->
					<p></p>	<a href="<?=BASE_URL('homes/register')?>" style="background-color:#C02942" class="button button-reveal button-xlarge button-rounded tright"><i class="icon-angle-right"></i><span>Daftar Akun Gudang</span></a>
				</div>
			</div>
			
			<div class="container" >
				<div class="row align-items-center">

					<div class="col-lg-5">
						<div class="heading-block">
						<h1>Tentang Aplikasi</h1>
						</div>
						<p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					</div>

					<div class="col-lg-7 align-self-end">

						<div class="position-relative overflow-hidden">
							<img src="<?=base_url('assets/ad.jpeg')?>" data-animate="fadeInUp" data-delay="100" alt="Chrome">
							<img src="<?=base_url('assets/ad.jpeg')?>" style="top: 0; left: 0; min-width: 100%; min-height: 100%;" data-animate="fadeInUp" data-delay="400" alt="iPhone" class="position-absolute">
						</div>

					</div>

				</div>
			</div>

			
	</div>

	<div class="container clearfix">

				<!-- <div class="heading-block topmargin-lg center">
					<h2>Bidang - Bidang CSR</h2>
					<span class="mx-auto">Berikut ini adalah bidang-bidang CSR Provinsi Jawa Tengah</span>
				</div>

				<div class="row col-mb-50 mb-4">
					<div class="col-lg-4 col-md-6">
		
						<div class="feature-box flex-md-row-reverse text-md-right" data-animate="fadeIn">
							<div class="fbox-icon">
								<a href="#"><i class="" style="background-color:#C02942"></i></a>
							</div>
							<div class="fbox-content">
								<h3></h3>
								<p></p>
							</div>
						</div>
						<br/>
					

						

					</div>

					<div class="col-lg-4 d-md-none d-lg-block text-center">
						<img src="<?=base_url()?>assets/jtg.png" alt="iphone 2">
						<br/>
						<br/>
						<br/>
						<br/>
						<img src="<?=base_url()?>assets/pak_ganjar.png" alt="iphone 2">
					</div>

					<div class="col-lg-4 col-md-6">
						
						

						<div class="feature-box" data-animate="fadeIn">
							<div class="fbox-icon">
								<a href="#"><i class="" style="background-color:#C02942"></i></a>
							</div>
							<div class="fbox-content">
								<h3></h3>
								<p></p>
							</div>
						</div>
						<br/>
						
							


					

					</div>
				</div>

			</div> -->
			
			
	<div class="line"></div>		
		<div class="container clearfix">
				
				<div class="heading-block center nobottommargin" >
					<h2>Maksud Dan Tujuan, Sasaran</h2>
					<span>Sistem Data Stock Garam</span>
				</div>
				<br/>
				
		<div class="row row-table">
		<div class="col-md-6 col-table">
			<div class="col-content md">
			<h3>Maksud dan Tujuan:</h3>
			<h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum..</h4>
			</div>
		</div>
		<div class="col-md-6 col-table">
			<div class="col-content bg">
			<h3>Sasaran:</h3>
			<h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h4>
			</div>
		</div>
		</div>
		</div>
				
				
				<!-- <div class="clear"></div> -->
				<!-- <div class="line"></div>
				
					<div class="container clearfix">
				
				<div class="heading-block center nobottommargin" >
					<h2>Zona Kemiskinan</h2>
					<span>Kabupaten/Kota Provinsi Jawa Tengah (Maret 2020)</span>
				</div>
				<br/>
				
			<div class="row row-table">
				<div class="col-md-6 col-table">
				<div class="col-content md">
					<div id="rekap" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
					<br/>
							<table class="table table-striped table-bordered">
							<tr>
								<th >Jawa Tengah</th>
							
								<th>11,41</th>
							</tr>
							<tr>
								<th>Nasional</th>
							
								<th>9,78</th>
							</tr>
							
							
							</table>
				</div>
				</div>
		<div class="col-md-6 col-table">
			<div class="col-content bg">
			<div class="table-responsive">
					<table id="datatable1" class="table table-striped table-bordered" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>Kabupaten/Kota</th>
				<th>Persentase Kemiskinan</th>
				
							
				
								
								
							</tr>
						</thead>
						
						<tbody>
							
							<tr  style="background-color: red">
								<td></td>
								<td></td>
				
							
							</tr>
							
							
							
						</tbody>
					</table>
					<br/>
					<table class="table table-striped table-bordered">
						<tr>
						<th style="background-color:#7fffd4">Hijau</th>
						<th>Hijau	Di bawah Provinsi Jawa Tengah dan Nasional</th>
						<th>< 9,78</th>
						</tr>
						<tr>
						<th style="background-color:#fff404">Kuning</th>
						<th>Di bawah Provinsi Jawa Tengah dan di atas Nasional</th>
						<th>9,78 >= 11,41</th>
						</tr>
						
						<tr>
						<th style="background-color:#ff0404">Merah</th>
						<th>Di atas Provinsi Jawa Tengah dan Nasional</th>
						<th>> 11,41</th>
						</tr>
						
					</table>
				

				</div>
			</div>
		</div>
		</div>
		</div>   -->
		
		
				
				
				
		<div class="line"></div>
				
			<div class="heading-block center">
					<h3>Galeri Kegiatan</h3>
					
			</div>



			<div id="oc-clients-full" class="owl-carousel image-carousel carousel-widget" data-margin="30" data-nav="false" data-loop="true" data-autoplay="5000" data-pagi="false" data-items-xs="2" data-items-sm="3" data-items-md="4" data-items-lg="4" data-items-xl="4">

					
		
		
		<a href="<?=base_url('assets/')?>foto/dkp7.jpeg" data-fancybox="group" data-caption="" >
								<img class="videoThumb bg-shadow" src="<?=base_url('assets/')?>foto/dkp7.jpeg">
		</a>
		
		<a href="<?=base_url('assets/')?>foto/dkp6.jpeg" data-fancybox="group" data-caption="" >
								<img class="videoThumb bg-shadow" src="<?=base_url('assets/')?>foto/dkp6.jpeg">
		</a>
		
		<a href="<?=base_url('assets/')?>foto/dkp5.jpeg" data-fancybox="group" data-caption="" >
								<img class="videoThumb bg-shadow" src="<?=base_url('assets/')?>foto/dkp5.jpeg">
		</a>
		
		<a href="<?=base_url('assets/')?>foto/dkp4.jpeg" data-fancybox="group" data-caption="" >
								<img class="videoThumb bg-shadow" src="<?=base_url('assets/')?>foto/dkp4.jpeg">
		</a>
		
		<a href="<?=base_url('assets/')?>foto/dkp3.jpeg" data-fancybox="group" data-caption="" >
								<img class="videoThumb bg-shadow" src="<?=base_url('assets/')?>foto/dkp3.jpeg">
		</a>
		
		<a href="<?=base_url('assets/')?>foto/dkp2.jpeg" data-fancybox="group" data-caption="" >
								<img class="videoThumb bg-shadow" src="<?=base_url('assets/')?>foto/dkp2.jpeg">
		</a>
		
		<a href="<?=base_url('assets/')?>foto/dkp1.jpeg" data-fancybox="group" data-caption="" >
								<img class="videoThumb bg-shadow" src="<?=base_url('assets/')?>foto/dkp1.jpeg">
		</a>
		

				</div>

				<!-- <div class="clear"></div> -->
				<div class="line"></div>



				<!--<div id="oc-clients-full" class="owl-carousel image-carousel carousel-widget" data-margin="30" data-nav="false" data-loop="true" data-autoplay="5000" data-pagi="false" data-items-xs="2" data-items-sm="3" data-items-md="4" data-items-lg="4" data-items-xl="4">

					<a href="#"><img src="<?=base_url('assets/canvas/')?>images/clients/6.png" alt="Clients"></a>
					<a href="#"><img src="<?=base_url('assets/canvas/')?>images/clients/7.png" alt="Clients"></a>


				</div>-->


			</div>

		</div>
			
			

        </section><!-- #content end -->
        
        
        

		<?php include(__DIR__ . "/../template/footer.php"); ?>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    
    
    
		
		   
   <!-- library JS fancybox -->
<link rel="stylesheet" type="text/css" href="https://dkp.jatengprov.go.id/assets/fancybox/jquery.fancybox.css">
<script src="https://dkp.jatengprov.go.id/assets/fancybox/jquery.fancybox.js"></script>
<script type="text/javascript">
    $("[data-fancybox]").fancybox({ });
</script>

	<script src="<?=base_url('assets/canvas/')?>js/components/bs-datatable.js"></script>
    <script>

$(document).ready(function() {
    $('#datatable1').dataTable({
        "pageLength": 5,
        "bLengthChange": false,
    });
});

</script>
        
</body>
</html>