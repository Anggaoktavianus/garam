<style>
#container {
  height: 500px; 
}

.highcharts-figure, .highcharts-data-table table {
  min-width: 310px; 
  max-width: 800px;
  margin: 1em auto;
}

.highcharts-data-table table {
  font-family: Verdana, sans-serif;
  border-collapse: collapse;
  border: 1px solid #EBEBEB;
  margin: 10px auto;
  text-align: center;
  width: 100%;
  max-width: 500px;
}
.highcharts-data-table caption {
  padding: 1em 0;
  font-size: 1.2em;
  color: #555;
}
.highcharts-data-table th {
  font-weight: 600;
  padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
  padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
  background: #f8f8f8;
}
.highcharts-data-table tr:hover {
  background: #f1f7ff;
}
</style>


<!-- Bootstrap Data Table Plugin -->
<link rel="stylesheet" href="<?=base_url('assets/canvas/')?>css/components/bs-datatable.css" type="text/css" />
	<!-- Content
	============================================= -->
	<section id="content">
		<div class="content-wrap" id="tentang">
			<div class="container" >
				<div class="row align-items-center">
					<div class="col-lg-12">
						<div class="heading-block">
						<h1>Data Tabel Kelompok</h1>
						</div>
					</div>
					<div class="col-lg-12 align-self-end">
						<div class="position-relative overflow-hidden">
							<div class="card-body">
							<div class="table-responsive">
							<table id="example1" class="table table-bordered table-striped">
							<thead>
							<tr>
								<th>No</th>
                    <th>Nama</th>
                    <th>Telepon</th>
                    <th>Email</th>
                    <th>Alamat</th>
                    <th>Ketua</th>
                    <th>Anggota</th>
                    <th>Koperasi</th>
							</tr>
							</thead> 
							<tbody>
							<?php $no = 1;
							foreach ($row->result() as $key => $data) {
							?>
							<tr>
							<td><?= $no++ ?></td>
								<td><?= $data->nama ?></td>
								<td><?= $data->telepon ?></td>
                <td><?= $data->email ?></td>
								<td><?= $data->alamat?>,<?= $data->desa?>,<?= $data->kecamatan?>,<?= $data->kabkota?></td>
								<td><?= $data->ketua ?></td>
								<td><?= $data->jumlah_anggota ?></td>
								<td><?= $data->koperasi ?></td>
							</tr>
							<?php } ?>
							</tbody>
							<tfoot>
							<tr>
								<th>No</th>
                    <th>Nama</th>
                    <th>Telepon</th>
                    <th>Email</th>
                    <th>Alamat</th>
                    <th>Ketua</th>
                    <th>Anggota</th>
                    <th>Koperasi</th>
							</tr>
							</tfoot>
							</table>
							</div>
							
						</div>
						<!-- /.card-body -->	
						</div>
					</div>
				</div>
			</div>
		</div>
    	</section>
	<!-- Main content -->
	<!-- #content end -->
<?php include(__DIR__ . "/../template/footer.php"); ?>	   
   <!-- library JS fancybox -->
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["excel", "pdf"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
</body>
</html>