<style>
.responsive-map-container {
    position: relative;
    padding-bottom: 56.25%;
    padding-top: 30px;
    height: 0;
    overflow: hidden;
}

.responsive-map-container iframe,   
.responsive-map-container object,  
.responsive-map-container embed {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
}
</style>
<!-- Bootstrap Data Table Plugin -->
<link rel="stylesheet" href="<?=base_url('assets/canvas/')?>css/components/bs-datatable.css" type="text/css" />
	<!-- Content
	============================================= -->
	<section class="ftco-section">
		<div class="container">
			<div class="col-lg-12">
				<div class="heading-block">
					<h1>Kontak DKP</h1>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-md-12">
					<div class="wrapper">
						<div class="row no-gutters">
							<div class="col-lg-8 col-md-7 order-md-last d-flex align-items-stretch">
								<div class="contact-wrap w-100 p-md-5 p-4">
									<h3 class="mb-4">View on map</h3>
									<form method="POST" id="contactForm" name="contactForm" class="contactForm">
										<div class="row">
											<div class="col-md-12">
												<div class="form-group"> 
													<div class="responsive-map-container">
														<!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.286517381234!2d110.41097151447303!3d-6.9754831702417714!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e70f4ad75f9108b%3A0xd41646274d040790!2sDinas%20Kelautan%20dan%20Perikanan%20Provinsi%20Jawa%20Tengah!5e0!3m2!1sid!2sid!4v1631023434639!5m2!1sid!2sid" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe> -->
														<?= $row->lokasi?>	
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
							<div class="col-lg-4 col-md-6 d-flex align-items-stretch">
								<div class="info-wrap bg-primary w-100 p-md-5 p-4">
									<h3>Kontak Kami</h3>
									<p class="mb-4">Dinas Kelautan dan Perikanan Provinsi Jawa Tengah</p>
				        				<div class="dbox w-100 d-flex align-items-start">
				        					<div class="icon d-flex align-items-center justify-content-center">
				        						<span class="fa fa-map-marker"></span>
				        					</div>
				        					<div class="text pl-1">
					            					<p><span>Alamat:</span> <?= ucwords(strtolower($row->alamat)) ?></p>
					          				</div>
				          				</div>
									<div class="dbox w-100 d-flex align-items-center">
											<div class="icon d-flex align-items-center justify-content-center">
												<span class="fa fa-phone"></span>
											</div>
											<div class="text pl-1">
										<p><span>Phone:</span> <a href="tel://<?= $row->telepon?>"><?= $row->telepon?></a></p>
										</div>
									</div>
									<div class="dbox w-100 d-flex align-items-center">
										<div class="icon d-flex align-items-center justify-content-center">
											<span class="fa fa-paper-plane"></span>
										</div>
										<div class="text pl-1">
											<p><span>Email:</span> <a href="mailto:<?= $row->email?>"><?= $row->email?></a></p>
										</div>
									</div>
									<div class="dbox w-100 d-flex align-items-center">
										<div class="icon d-flex align-items-center justify-content-center">
											<span class="fa fa-globe"></span>
										</div>
										<div class="text pl-1">
											<p><span>Website: </span> <a href="<?= $row->web?>"><?= $row->web?></a></p>
										</div>
									</div>
									<div class="dbox w-100 d-flex align-items-center">
										<div class="icon d-flex align-items-center justify-content-center">
											<span class="fa fa-instagram"></span>
										</div>
										<div class="text pl-1">
											<p><span>Instagram: </span> <a href=" https://instagram.com/<?= $row->ig?>"><?= $row->ig?></a></p>
										</div>
									</div>
									<div class="dbox w-100 d-flex align-items-center">
										<div class="icon d-flex align-items-center justify-content-center">
											<span class="fa fa-whatsapp"></span>
										</div>
										<div class="text pl-1">
											<p><span>Whatsapp: </span> <a href="https://api.whatsapp.com/send?phone=<?= $row->wa?>"><?= $row->wa?></a></p>
										</div>
									</div>
									<div class="dbox w-100 d-flex align-items-center">
										<div class="icon d-flex align-items-center justify-content-center">
											<span class="fa fa-twitter"></span>
										</div>
										<div class="text pl-1">
											<p><span>Twitter: </span> <a href="https://twitter.com/<?= $row->twitter?>"><?= $row->twitter?></a></p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<!-- #content end -->
	<!-- Main content -->
    <!-- /.content -->
<?php include(__DIR__ . "/../template/footer.php"); ?>	   