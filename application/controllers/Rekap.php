<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Rekap extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
		not_login();
		$this->load->model(['m_user']);
		$this->load->model('m_grafik');
	}
	
	public function index()
	{
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();
		$grafiklahan = $this->m_grafik->grafik_lahan();
		$data = array(
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
			'grafik_lahan' => $grafiklahan,
			

		);
	$this->template->load('template', 'admin/rekap/lahan', $data);

	}

	public function produksi()
	{
	$aktif = $this->m_user->get_aktivasi();	
	$aktifkota = $this->m_user->get_aktivasi_kota();
	$hitung = $this->m_user->count();
	$hitungs = $this->m_user->count_kota();	
	$grafikproduksi = $this->m_grafik->grafik_produksi();
	
	$data = array(
		'aktifkota' => $aktifkota,
		'aktif' => $aktif,
		'hitung' => $hitung,
		'hitungs' => $hitungs,
		'grafik_produksi' => $grafikproduksi,

	);
	$this->template->load('template', 'admin/rekap/produksi', $data);

	}

	public function stok()
	{
	$aktif = $this->m_user->get_aktivasi();	
	$aktifkota = $this->m_user->get_aktivasi_kota();
	$hitung = $this->m_user->count();
	$hitungs = $this->m_user->count_kota();	
	$grafikstok = $this->m_grafik->grafik_stok();
	
	$data = array(
		'aktifkota' => $aktifkota,
		'aktif' => $aktif,
		'hitung' => $hitung,
		'hitungs' => $hitungs,
		'grafik_stok' => $grafikstok,

	);
	$this->template->load('template', 'admin/rekap/stok', $data);

	}

	public function petambak()
	{
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();	
		$grafikpetambak = $this->m_grafik->grafik_petambak();
		$data = array(
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
			'grafik_petambak' => $grafikpetambak,

		);
		$this->template->load('template', 'admin/rekap/petambak', $data);

	}

	public function kelompok()
	{
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();	
		
		$data = array(
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,

		);
		$this->template->load('template', 'admin/rekap/kelompok', $data);

	}

	public function koperasi()
	{
	$aktif = $this->m_user->get_aktivasi();	
	$aktifkota = $this->m_user->get_aktivasi_kota();
	$hitung = $this->m_user->count();
	$hitungs = $this->m_user->count_kota();	
	
	$data = array(
		'aktifkota' => $aktifkota,
		'aktif' => $aktif,
		'hitung' => $hitung,
		'hitungs' => $hitungs,

	);
	$this->template->load('template', 'admin/rekap/koperasi', $data);

	}

	public function gudang()
	{
	$aktif = $this->m_user->get_aktivasi();	
	$aktifkota = $this->m_user->get_aktivasi_kota();
	$hitung = $this->m_user->count();
	$hitungs = $this->m_user->count_kota();	
	$grafikgudang = $this->m_grafik->grafik_gudang();
	
	$data = array(
		'aktifkota' => $aktifkota,
		'aktif' => $aktif,
		'hitung' => $hitung,
		'hitungs' => $hitungs,
		'grafik_gudang' => $grafikgudang,

	);
	$this->template->load('template', 'admin/rekap/gudang', $data);

	}

	// REKAP KOTA
	public function lahan_kota()
	{
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();
		$grafiklahan_kota = $this->m_grafik->grafik_lahan_kota();
		// $grafikproduksi = $this->m_grafik->grafik_produksi();
		// $grafikstok = $this->m_grafik->grafik_stok();
		// $grafikgudang = $this->m_grafik->grafik_gudang();
		$data = array(
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
			'grafik_lahan_kota' => $grafiklahan_kota,
			// 'grafik_produksi' => $grafikproduksi,
			// 'grafik_stok' => $grafikstok,
			// 'grafik_gudang' => $grafikgudang,

		);
	$this->template->load('template', 'admin/rekap/lahan', $data);

	}

	public function produksi_kota()
	{
	$aktif = $this->m_user->get_aktivasi();	
	$aktifkota = $this->m_user->get_aktivasi_kota();
	$hitung = $this->m_user->count();
	$hitungs = $this->m_user->count_kota();	
	$grafikproduksi_kota = $this->m_grafik->grafik_produksi_kota();
	
	$data = array(
		'aktifkota' => $aktifkota,
		'aktif' => $aktif,
		'hitung' => $hitung,
		'hitungs' => $hitungs,
		'grafik_produksi_kota' => $grafikproduksi_kota,

	);
	$this->template->load('template', 'admin/rekap/produksi', $data);

	}

	public function stok_kota()
	{
	$aktif = $this->m_user->get_aktivasi();	
	$aktifkota = $this->m_user->get_aktivasi_kota();
	$hitung = $this->m_user->count();
	$hitungs = $this->m_user->count_kota();	
	$grafikstok_kota = $this->m_grafik->grafik_stok_kota();

	
	$data = array(
		'aktifkota' => $aktifkota,
		'aktif' => $aktif,
		'hitung' => $hitung,
		'hitungs' => $hitungs,
		'grafik_stok_kota' => $grafikstok_kota,

	);
	$this->template->load('template', 'admin/rekap/stok', $data);

	}

	public function petambak_kota()
	{
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();	
		$grafikpetambak_kota = $this->m_grafik->grafik_petambak_kota();
		
		$data = array(
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
			'grafik_petambak_kota' => $grafikpetambak_kota,

		);
		$this->template->load('template', 'admin/rekap/petambak', $data);

	}

	public function kelompok_kota()
	{
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();	
		
		$data = array(
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,

		);
		$this->template->load('template', 'admin/rekap/kelompok', $data);

	}

	public function koperasi_kota()
	{
	$aktif = $this->m_user->get_aktivasi();	
	$aktifkota = $this->m_user->get_aktivasi_kota();
	$hitung = $this->m_user->count();
	$hitungs = $this->m_user->count_kota();	
	
	$data = array(
		'aktifkota' => $aktifkota,
		'aktif' => $aktif,
		'hitung' => $hitung,
		'hitungs' => $hitungs,

	);
	$this->template->load('template', 'admin/rekap/koperasi', $data);

	}

	public function gudang_kota()
	{
	$aktif = $this->m_user->get_aktivasi();	
	$aktifkota = $this->m_user->get_aktivasi_kota();
	$hitung = $this->m_user->count();
	$hitungs = $this->m_user->count_kota();	
	$grafikgudang_kota = $this->m_grafik->grafik_gudang_kota();
	
	$data = array(
		'aktifkota' => $aktifkota,
		'aktif' => $aktif,
		'hitung' => $hitung,
		'hitungs' => $hitungs,
		'grafik_gudang_kota' => $grafikgudang_kota,

	);
	$this->template->load('template', 'admin/rekap/gudang', $data);

	}


	
}
