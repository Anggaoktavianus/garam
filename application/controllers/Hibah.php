<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
class Hibah extends CI_Controller {

        function __construct() {
   	     parent::__construct();
		 not_login();
		//  check_admin();
   	        $this->load->model('m_hibah');
		$this->load->model('m_lahan');
		$this->load->model('m_user');
		$this->load->library('form_validation');

        }

	//IMPORT EXCEL
	public function uploaddata()
	{
		$config['upload_path'] = './uploads/hibah/';
		$config['allowed_types'] = 'xlsx|xls';
		$config['file_name'] = 'hibah' . time();
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if ($this->upload->do_upload('importexcel')) {
		$file = $this->upload->data();
		$reader = ReaderEntityFactory::createXLSXReader();

		$reader->open('uploads/hibah/' . $file['file_name']);
		foreach ($reader->getSheetIterator() as $sheet) {
			$numRow = 1;
			foreach ($sheet->getRowIterator() as $row) {
			if ($numRow > 1) {
				$databarang = array(
				'kelompok'  	=> $row->getCellAtIndex(1),
				'alamat'  	=> $row->getCellAtIndex(2),
				'ketua'  	=> $row->getCellAtIndex(3),
				'telepon'  	=> $row->getCellAtIndex(4),
				'jenis_hibah'  	=> $row->getCellAtIndex(5),
				'anggaran'      => $row->getCellAtIndex(6),
				'volume'      	=> $row->getCellAtIndex(7),
				'satuan'     	=> $row->getCellAtIndex(8),
				'tahun'      	=> $row->getCellAtIndex(9),
				'sumberdana'     => $row->getCellAtIndex(10),
				'kelurahan_id'     	=> $row->getCellAtIndex(12),
				'kecamatan_id'      	=> $row->getCellAtIndex(14),
				'kabkota_id'     => $row->getCellAtIndex(16),
				'created_at'=>date('Y-m-d H:i:s'),
				);
				$this->m_hibah->import_data($databarang);
			}
			$numRow++;
			}
			$reader->close();
			// unlink('uploads/gudang/' . $file['file_name']);
			$this->session->set_flashdata('pesan', 'import Data Berhasil');
			redirect('hibah');
		}
		} else {
		echo "Error :" . $this->upload->display_errors();
		};
	}

	function get_ajax()
    	{
		$list = $this->m_hibah->get_datatables();
		// $list = $this->m_items->get_by_jumlah();
		$data = array();
		$no = @$_POST['start'];
		foreach ($list as $a) {
		$no++;
		$row = array();
		$row[] = $no . ".";
		$row[] = $a->kelompok;
		$row[] = $a->alamat;
		$row[] = $a->ketua;
		$row[] = $a->telepon;
		$row[] = $a->jenis_hibah;
		$row[] = "Rp.".number_format($a->anggaran);
		$row[] = $a->volume;
		$row[] = $a->satuan;
		$row[] = $a->tahun;
		$row[] = $a->sumberdana;
		// add html for action
		$row[] = '<form action="' .site_url('hibah/del').'" method="post"><a href="' . site_url('hibah/edit/' . $a->id) . '" class="btn btn-success text-white btn-xs"><i class="fas fa-pencil-alt"></i></a>
			<input type="hidden" name="id" value="'. $a->id.'"> <button onclick="return confirm(\'Yakin hapus data?\')" class="btn btn-danger btn-xs text-white">
				<i class="fas fa-trash-alt "></i>
				</button></form>';
		$data[] = $row;
		}
		$output = array(
		"draw" => @$_POST['draw'],
		"recordsTotal" => $this->m_hibah->count_all(),
		"recordsFiltered" => $this->m_hibah->count_filtered(),
		"data" => $data,
		);
		// output to json format
		echo json_encode($output);
    	}

	// AJAX menampilkan LIST berdasarkan kota
	function get_ajax_kota()
    	{	
		
		$list = $this->m_hibah->get_datatables_kota();
		$data = array();
		$no = @$_POST['start'];
		foreach ($list as $a) {
		$no++;
		$row = array();
		$row[] = $no . ".";
		$row[] = $a->kelompok;
		$row[] = $a->alamat;
		$row[] = $a->ketua;
		$row[] = $a->telepon;
		$row[] = $a->jenis_hibah;
		$row[] = "Rp.".number_format($a->anggaran);
		$row[] = $a->volume;
		$row[] = $a->satuan;
		$row[] = $a->tahun;
		$row[] = $a->sumberdana;
		// add html for action
		$row[] = '<form action="' .site_url('hibah/del').'" method="post"><a href="' . site_url('hibah/edit/' . $a->id) . '" class="btn btn-success text-white btn-xs"><i class="fas fa-pencil-alt"></i></a>
			<input type="hidden" name="id" value="'. $a->id.'"> <button onclick="return confirm(\'Yakin hapus data?\')" class="btn btn-danger btn-xs text-white">
				<i class="fas fa-trash-alt "></i>
				</button></form>';
		$data[] = $row;
		}
		$output = array(
		"draw" => @$_POST['draw'],
		"recordsTotal" => $this->m_hibah->count_all_kota($this->session->userdata('kabkota_id')),
		"recordsFiltered" => $this->m_hibah->count_filtered_kota($this->session->userdata('kabkota_id')),
		"data" => $data,
		);
		// output to json format
		echo json_encode($output);
    	}

	public function index()
	{	
		$row  = $this->m_hibah->get();
		$kecam = $this->m_lahan->get_keca_kota();
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();	
		$data = array(
			'row' => $row,
			'kecam'=> $kecam,
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
		);
		$this->template->load('template', 'admin/hibah/v_hibah', $data);		
		// $this->load->view('template/headx',$data);
		// $this->load->view('admin/hibah/v_hibah',$data);
		// $this->load->view('template/footerx');
		
	}
	

	public function add_index()
	{
		$kabkota = $this->m_hibah->get_kab();
		$kecam = $this->m_lahan->get_keca_kota();
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();		
		$data = array(
			'kabkota' => $kabkota,
			'kecam' => $kecam,
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
		); 
		$this->template->load('template', 'admin/hibah/add_hibah', $data);
		// $this->load->view('template/headx',$data);
		// $this->load->view('admin/hibah/add_hibah',$data);
		// $this->load->view('template/footerx');
		
	}
        
	public function add() 
	{

		$this->form_validation->set_rules('kelompok', 'Nama Kelompok', 'trim|required');
		
		$this->form_validation->set_message('min_length', '{field} minimal 5 karakter');
		$this->form_validation->set_message('is_unique', '<span style="color:red"> *{field} sudah dipakai</span> ');

		$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

		if ($this->form_validation->run() == FALSE) {
			
            	$kabkota = $this->m_hibah->get_kab();
		$kecam = $this->m_lahan->get_keca_kota();
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();	
		$data = array(
			'kabkota' => $kabkota,
			'kecam' => $kecam,
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
		);
		$this->template->load('template', 'admin/hibah/add_hibah', $data);
		// $this->load->view('template/headx',$data);
		// $this->load->view('admin/hibah/add_hibah',$data);
		// $this->load->view('template/footerx');
		} else {
		 if($this->m_hibah->add()){
           	$this->session->set_flashdata('info', 'success');
          	 $this->session->set_flashdata('pesan', 'Berhasil menambahkan data.');
           	redirect('hibah','refresh');
		}
		echo "<script>window.location ='" . site_url('hibah') . "' ; </script>";
		}
	}

	
	public function edit($id)
	{

		$this->form_validation->set_rules('kelompok', 'Nama Kelompok', 'trim|required');
		
		$this->form_validation->set_message('min_length', '{field} minimal 5 karakter');
		$this->form_validation->set_message('is_unique', '{field} sudah dipakai');

		$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

		if ($this->form_validation->run() == FALSE) {
			 $query = $this->m_hibah->getall($id);
			if ($query->num_rows() > 0) {
				$row = $query->row();
				$kabkota = $this->m_hibah->get_kab();
				$kecam = $this->m_lahan->get_keca_kota();
				$keca = $this->m_hibah->get_keca();
				$desa = $this->m_hibah->get_kelu();
				$koperasi = $this->m_hibah->get_koperasi();
				$role = $this->m_hibah->get_role();
				$aktif = $this->m_user->get_aktivasi();	
				$aktifkota = $this->m_user->get_aktivasi_kota();
				$hitung = $this->m_user->count();
				$hitungs = $this->m_user->count_kota();	
				 $data = array(
					'row' => $row,
					'kecam' => $kecam,
					'kabkota' => $kabkota,
					'koperasi' => $koperasi,
					'keca' => $keca,
					'desa' => $desa,
					'role' => $role,
					'aktifkota' => $aktifkota,
					'aktif' => $aktif,
					'hitung' => $hitung,
					'hitungs' => $hitungs,

				);
				$this->template->load('template', 'admin/hibah/edit_hibah', $data);
				
				// $this->load->view('template/headx',$data);
				// $this->load->view('admin/hibah/edit_hibah',$data);
				// $this->load->view('template/footerx');
				// echo "<script>alert ('Data tidak ditemukan');";
			} else {
				echo "<script>alert ('Data tidak ditemukan');";
				echo "window.location ='" . site_url('user') . "' ; </scrip>";
			}

            	// redirect('user/add_index');.
		} else {
		// $post = $this->input->post(null, TRUE);
		$this->m_hibah->edit();
		 if($this->db->affected_rows() > 0){
		$this->session->set_flashdata('info', 'success');
          	 $this->session->set_flashdata('pesan', 'Berhasil merubah data.');
           	redirect('hibah','refresh');
		}
		echo "<script>window.location ='" . site_url('hibah') . "' ; </script>";
		}
	}	

	function nama_check()
	{

		$post = $this->input->post(NULL, TRUE);
		$query = $this->db->query("SELECT * FROM garam_hibah WHERE nama= '$post[nama]' AND id != '$post[id]'");
		if ($query->num_rows() > 0) {
		$this->form_validation->set_message('nama_check', '{field} ini sudah dipakai, silahkan ganti');
		return FALSE;
		} else {
		return TRUE;
		}
	}

	public function del()
	{
		$id = $this->input->post('id');
		// $this->m_hibah->del($id);

		$row = $this->m_hibah->get($id);

		if ($row) {
		$this->m_hibah->del($id);
		
		$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('pesan', 'Data berhasil dihapus');
			redirect(site_url('hibah'));
		} else {
		$this->session->set_flashdata('info', 'danger');
		$this->session->set_flashdata('pesan', 'gagal hapus ');
		redirect(site_url('hibah'));
		}
	}

	public function aktivasi()
	{
		$id = $this->input->post('id');
		// $this->m_hibah->del($id);

		$row = $this->m_hibah->get($id);

		if ($row) {
		$this->m_hibah->aktivasi($id);
		
		$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('pesan', 'Akun berhasil diaktivasi');
			redirect(site_url('user'));
		} else {
		$this->session->set_flashdata('info', 'danger');
		$this->session->set_flashdata('pesan', 'gagal aktivasi ');
		redirect(site_url('user'));
		}
	}

}

/* End of file User.php */
/* Location: ./application/controllers/User.php */