<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;

class Filter extends CI_Controller {

         function __construct() {
   	     parent::__construct();
		 not_login();
		//  check_admin();
   	        $this->load->model('m_gudang');
		$this->load->model('m_user');
		$this->load->model('m_filter');
		$this->load->library('form_validation');

         }

 
    public function index()
    {
        $this->load->helper('url');
		$this->load->helper('form');
		$id_kota = $this->input->post('kota');
		$id_kecamatan = $this->input->post('kecamatan');
		$id_desa = $this->input->post('desa');
		// $kota = $this->m_filter->get_list_kota();
        	// $kecamatan = $this->m_filter->get_list_kecamatan($id_kota);
        	// $row  = $this->m_gudang->get();
		$this->db->select('a.*, c.name as kecamatan,c.id as id_kec, d.name as desa, d.id as id_des');
		$this->db->from('garam_kota as a');
		$this->db->join('garam_kecamatan c', 'c.regency_id = a.id ','left' );
		$this->db->join('garam_desa d', 'd.district_id = c.id ','left' );
		$this->db->where('province_id = 33');

		if($id_kota != ''){
			$this->db->where('a.id', $id_kota);
		}

		if($id_kecamatan != ''){
			$this->db->where('b.id', $id_kecamatan);
		}


		if($id_desa != ''){
			$this->db->where('c.id', $id_desa);
		}
		
		
		
		$row = $this->db->get();

		$rows  = $this->m_gudang->get_gudang();
		$aktif = $this->m_gudang->get_aktivasi();	
		$hitung = $this->m_gudang->count();
		$kabkota = $this->m_gudang->get_kab();	
		$data = array(
			'row' => $row,
			'kabkota' => $kabkota,
			'rows' => $rows,
			'aktif' => $aktif,
			'hitung' => $hitung,
		);


		
        
        $this->template->load('template', 'admin/gudang/import', $data);
    }
    function list_sub()
	{
		$id_kota = $this->input->post('id');
		$sub = $this->m_user->get_kec($id_kota);

		// Buat variabel untuk menampung tag-tag option nya
		// Set defaultnya dengan tag option Pilih
		$lists = "<option value='00'>Pilih</option>";

		foreach ($sub as $data) {
		$lists .= "<option value='" . $data->id . "'>" . $data->name . "</option>"; // Tambahkan tag option ke variabel $lists
		}

		$callback = array('list_sub' => $lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota

		echo json_encode($callback); // konversi varibael $callback menjadi JSON
	}
	function list_desa()
	{
		$id_kec = $this->input->post('id');


		$sub = $this->m_user->get_kel($id_kec);

		// Buat variabel untuk menampung tag-tag option nya
		// Set defaultnya dengan tag option Pilih
		$lists = "<option value='00'>Pilih</option>";

		foreach ($sub as $data) {
		$lists .= "<option value='" . $data->id . "'>" . $data->name . "</option>"; // Tambahkan tag option ke variabel $lists
		}

		$callback = array('list_desa' => $lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota

		echo json_encode($callback); // konversi varibael $callback menjadi JSON
	}
    public function ajax_list()
	{

	// $kota = $this->input->post('kota');
    //     $kecamatan = $this->input->post('kecamatan');
	// $desa = $this->input->post('desa');

	$id_kota = $this->input->post('kota');
	$id_kecamatan = $this->input->post('kecamatan');
	$id_desa = $this->input->post('desa');

        $posts = $this->m_filter->get_datatables($id_kota, $id_kecamatan, $id_desa); 

        $data = array();
        $no = $this->input->post('start');
        foreach ($posts as $post) 
        {
            
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $post->name;
            $row[] = $post->id;
            $row[] = $post->kecamatan;
            $row[] = $post->id_kec;
            $row[] = $post->desa;
            $row[] = $post->id_des;
            // $row[] = "-";
            // $row[] =  "-";
            
            $data[] = $row;
        }
        
       $output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->m_filter->count_all(),
						"recordsFiltered" => $this->m_filter->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
 

	 public function uploaddata()
    {
        $config['upload_path'] = './uploads/gudang/';
        $config['allowed_types'] = 'xlsx|xls';
        $config['file_name'] = 'doc' . time();
        $this->load->library('upload', $config);
	$this->upload->initialize($config);
        if ($this->upload->do_upload('importexcel')) {
            $file = $this->upload->data();
            $reader = ReaderEntityFactory::createXLSXReader();

            $reader->open('uploads/gudang/' . $file['file_name']);
            foreach ($reader->getSheetIterator() as $sheet) {
                $numRow = 1;
                foreach ($sheet->getRowIterator() as $row) {
                    if ($numRow > 1) {
                        $databarang = array(
                            'nama'  => $row->getCellAtIndex(0),
                            'kapasitas'  => $row->getCellAtIndex(1),
                            'alamat'       => $row->getCellAtIndex(2),
			    'latitude'       => $row->getCellAtIndex(3),
			    'longitude'       => $row->getCellAtIndex(4),
                            'created_at'=>date('Y-m-d H:i:s'),
                        );
                        $this->m_gudang->import_data($databarang);
                    }
                    $numRow++;
                }
                $reader->close();
                unlink('uploads/gudang/' . $file['file_name']);
                $this->session->set_flashdata('pesan', 'import Data Berhasil');
                redirect('gudang');
            }
        } else {
            echo "Error :" . $this->upload->display_errors();
        };
    } 



}

/* End of file User.php */
/* Location: ./application/controllers/User.php */