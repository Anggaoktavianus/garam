<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;

class lahan extends CI_Controller {

        function __construct() {
   	     parent::__construct();
		 not_login();
		$this->load->model('m_lahan');
		$this->load->model('m_user');
		$this->load->library('form_validation');
		$this->load->library('googlemaps');

        }

	//IMPORT EXCEL
	public function uploaddata()
	{
		$config['upload_path'] = './uploads/lahan/';
		$config['allowed_types'] = 'xlsx|xls';
		$config['file_name'] = 'lahan' . time();
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if ($this->upload->do_upload('importexcel')) {
		$file = $this->upload->data();
		$reader = ReaderEntityFactory::createXLSXReader();

		$reader->open('uploads/lahan/' . $file['file_name']);
		foreach ($reader->getSheetIterator() as $sheet) {
			$numRow = 1;
			foreach ($sheet->getRowIterator() as $row) {
			if ($numRow > 1) {
				$databarang = array(
				'lahan_integrasi'  		=> $row->getCellAtIndex(1),
				'lahan_nonintegrasi'  		=> $row->getCellAtIndex(2),
				'bulan'  			=> $row->getCellAtIndex(3),
				'tahun'  			=> $row->getCellAtIndex(4),
				'kelurahan_id'  		=> $row->getCellAtIndex(6),
				'kecamatan_id'  		=> $row->getCellAtIndex(8),
				'kabkota_id'  			=> $row->getCellAtIndex(10),
				'latitude'  			=> $row->getCellAtIndex(11),
				'longitude'  			=> $row->getCellAtIndex(12),
				'created_at'			=>date('Y-m-d H:i:s'),
				);
				$this->m_lahan->import_data($databarang);
			}
			$numRow++;
			}
			$reader->close();
			// unlink('uploads/gudang/' . $file['file_name']);
			// $this->session->set_flashdata('pesan', 'import Data Berhasil');
			$this->session->set_flashdata('info', 'success');
          	 	$this->session->set_flashdata('pesan', 'Berhasil import data.');
			redirect('lahan');
		}
		} else {
		echo "Error :" . $this->upload->display_errors();
		};
	}

	function get_ajax()
    	{
		$list = $this->m_lahan->get_datatables();
		$data = array();
		$no = @$_POST['start'];
		foreach ($list as $a) {
		$no++;
		$row = array();
		$row[] = $no . ".";
		$row[] = $a->lahan_integrasi;
		$row[] = $a->lahan_nonintegrasi ;
		$row[] = $a->bulan;
		$row[] = $a->tahun;
		$row[] = $a->desa;
		$row[] = $a->kecamatan;
		$row[] = $a->kabkota;
		// add html for action
		$row[] = '<form action="' .site_url('lahan/del').'" method="post"><a href="' . site_url('lahan/edit/' . $a->id) . '" class="btn btn-success text-white btn-xs"><i class="fas fa-pencil-alt"></i></a>
			<input type="hidden" name="id" value="'. $a->id.'"> <button onclick="return confirm(\'Yakin hapus data?\')" class="btn btn-danger btn-xs text-white">
				<i class="fas fa-trash-alt "></i>
				</button></form>';
		$data[] = $row;
		}
		$output = array(
		"draw" => @$_POST['draw'],
		"recordsTotal" => $this->m_lahan->count_all(),
		"recordsFiltered" => $this->m_lahan->count_filtered(),
		"data" => $data,
		);
		// output to json format
		echo json_encode($output);
    	}

	function get_ajax_kota()
    	{	
		$list = $this->m_lahan->get_datatables_kota();
		$data = array();
		$no = @$_POST['start'];
		foreach ($list as $a) {
		$no++;
		$row = array();
		$row[] = $no . ".";
		$row[] = $a->lahan_integrasi;
		$row[] = $a->lahan_nonintegrasi ;
		$row[] = $a->bulan;
		$row[] = $a->tahun;
		$row[] = $a->desa;
		$row[] = $a->kecamatan;
		$row[] = $a->kabkota;
		
		// add html for action
		$row[] = '<form action="' .site_url('lahan/del').'" method="post"><a href="' . site_url('lahan/edit/' . $a->id) . '" class="btn btn-success text-white btn-xs"><i class="fas fa-pencil-alt"></i></a>
			<input type="hidden" name="id" value="'. $a->id.'"> <button onclick="return confirm(\'Yakin hapus data?\')" class="btn btn-danger btn-xs text-white">
				<i class="fas fa-trash-alt "></i>
				</button></form>';
		$data[] = $row;
		}
		$output = array(
		"draw" => @$_POST['draw'],
		"recordsTotal" => $this->m_lahan->count_all_kota($this->session->userdata('kabkota_id')),
		"recordsFiltered" => $this->m_lahan->count_filtered_kota($this->session->userdata('kabkota_id')),
		"data" => $data,
		);
		// output to json format
		echo json_encode($output);
    	}
	    
	public function index()
	{
		$config['center'] = '-6.9216088,110.4139686';
		$config['zoom']=15; 
		$this->googlemaps->initialize($config);
		$row  = $this->m_lahan->get();
		$rows  = $this->m_lahan->get_koordinat();
		$geojson  = $this->m_lahan->geojson();
		$bulan  = $this->m_lahan->bulan();
		$kecam = $this->m_lahan->get_keca_kota($this->session->userdata('kabkota_id'));
		$koordinat = $this->m_lahan->get_koordinat_kota();
		$tahun  = $this->m_lahan->tahun();
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();	
		$data = array(
			'row' => $row,
			'rows' => $rows,
			'geojson' => $geojson,
			'bulan' => $bulan,
			'kecam' => $kecam,
			'koordinat' => $koordinat,
			'tahun' => $tahun,
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
			'map' => $this->googlemaps->create_map(),
		);
		$this->template->load('template', 'admin/lahan/v_lahan', $data);	
		
	}
	

	public function add_index()
	{
		$kabkota = $this->m_lahan->get_kab();
		$kecam = $this->m_lahan->get_keca_kota();
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();		
		$data = array(
			'kabkota' => $kabkota,
			'kecam' => $kecam,
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
		); 
		$this->template->load('template', 'admin/lahan/add_lahan', $data);
		
	}
        
	public function add()
	{

		$this->form_validation->set_rules('bulan', 'Bulan', 'trim|required');

		$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

		if ($this->form_validation->run() == FALSE) {
			
            	$kabkota = $this->m_lahan->get_kab();
		$kecam = $this->m_lahan->get_keca_kota();
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();		
		$data = array(
			'kabkota' => $kabkota,
			'kecam' => $kecam,
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
		);
		$this->template->load('template', 'admin/lahan/add_lahan', $data);
		} else {
		 if($this->m_lahan->add()){
           	$this->session->set_flashdata('info', 'success');
          	 $this->session->set_flashdata('pesan', 'Berhasil menambahkan data.');
           	redirect('lahan','refresh');
		}
		echo "<script>window.location ='" . site_url('lahan') . "' ; </script>";
		}
	}

	
	public function edit($id)
	{

		$this->form_validation->set_rules('bulan', 'Bulan', 'trim|required');

		$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

		if ($this->form_validation->run() == FALSE) {
			 $query = $this->m_lahan->getall($id);
			if ($query->num_rows() > 0) {
				$row = $query->row();
				$kabkota = $this->m_lahan->get_kab();
				$keca = $this->m_lahan->get_keca();
				$kecam = $this->m_lahan->get_keca_kota();
				$desa = $this->m_lahan->get_kelu();
				$role = $this->m_lahan->get_role();
				$aktif = $this->m_user->get_aktivasi();	
				$aktifkota = $this->m_user->get_aktivasi_kota();
				$hitung = $this->m_user->count();
				$hitungs = $this->m_user->count_kota();	
				 $data = array(
					'row' => $row,
					'kabkota' => $kabkota,
					'keca' => $keca,
					'kecam' => $kecam,
					'desa' => $desa,
					'role' => $role,
					'aktifkota' => $aktifkota,
					'aktif' => $aktif,
					'hitung' => $hitung,
					'hitungs' => $hitungs,

				);
				$this->template->load('template', 'admin/lahan/edit_lahan', $data);
				
			} else {
				echo "<script>alert ('Data tidak ditemukan');";
				echo "window.location ='" . site_url('user') . "' ; </scrip>";
			}
		} else {
		$this->m_lahan->edit();
		 if($this->db->affected_rows() > 0){
		$this->session->set_flashdata('info', 'success');
          	 $this->session->set_flashdata('pesan', 'Berhasil merubah data.');
           	redirect('lahan','refresh');
		}
		echo "<script>window.location ='" . site_url('lahan') . "' ; </script>";
		}
	}

	public function del()
	{
		$id = $this->input->post('id');
		// $this->m_lahan->del($id);

		$row = $this->m_lahan->get($id);

		if ($row) {
		$this->m_lahan->del($id);
		
		$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('pesan', 'Data berhasil dihapus');
			redirect(site_url('lahan'));
		} else {
		$this->session->set_flashdata('info', 'danger');
		$this->session->set_flashdata('pesan', 'gagal hapus ');
		redirect(site_url('lahan'));
		}
	}

	function get_latlong(){
        $kode=$this->input->post('id');
        $data=$this->m_lahan->get_latlong($kode);
        echo json_encode($data); 
    }

}

/* End of file User.php */
/* Location: ./application/controllers/User.php */