<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;

class Stock extends CI_Controller {

        function __construct() {
   	     parent::__construct();
		 not_login();
		//  check_admin();
   	        $this->load->model('m_stock');
		$this->load->model('m_user');
		$this->load->model('m_lahan');
		$this->load->library('form_validation');

        }

	//IMPORT EXCEL
	public function uploaddata()
	{
		$config['upload_path'] = './uploads/stok/';
		$config['allowed_types'] = 'xlsx|xls';
		$config['file_name'] = 'stok' . time();
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if ($this->upload->do_upload('importexcel')) {
		$file = $this->upload->data();
		$reader = ReaderEntityFactory::createXLSXReader();

		$reader->open('uploads/stok/' . $file['file_name']);
		foreach ($reader->getSheetIterator() as $sheet) {
			$numRow = 1;
			foreach ($sheet->getRowIterator() as $row) {
			if ($numRow > 1) {
				$databarang = array(
				'stock'  	=> $row->getCellAtIndex(9),
				'bulan'  	=> $row->getCellAtIndex(1),
				'tahun'  	=> $row->getCellAtIndex(2),
				'kabkota_id'  	=> $row->getCellAtIndex(8),
				'kecamatan_id'  => $row->getCellAtIndex(6),
				'kelurahan_id'  => $row->getCellAtIndex(4),
				'created_at'=>date('Y-m-d H:i:s'),
				);
				$this->m_stock->import_data($databarang);
			}
			$numRow++;
			}
			$reader->close();
			// unlink('uploads/gudang/' . $file['file_name']);
			$this->session->set_flashdata('pesan', 'import Data Berhasil');
			redirect('stock');
		}
		} else {
		echo "Error :" . $this->upload->display_errors();
		};
	}

	function get_ajax()
    	{
		$list = $this->m_stock->get_datatables();
		$data = array();
		$no = @$_POST['start'];
		foreach ($list as $a) {
		$no++;
		$row = array();
		$row[] = $no . ".";
		$row[] = $a->stock;
		$row[] = $a->bulan;
		$row[] = $a->tahun;
		$row[] = $a->desa;
		$row[] = $a->kecamatan;
		$row[] = $a->kabkota;
		
		// $row[] = strtoupper($a->alamat).','.$a->desa.','.$a->kecamatan.','.$a->kabkota;
		
		// $row[] = $a->jumlah_anggota;
		
		// add html for action
		$row[] = '<form action="' .site_url('stock/del').'" method="post"><a href="' . site_url('stock/edit/' . $a->id) . '" class="btn btn-success text-white btn-xs"><i class="fas fa-pencil-alt"></i></a>
			<input type="hidden" name="id" value="'. $a->id.'"> <button onclick="return confirm(\'Yakin hapus data?\')" class="btn btn-danger btn-xs text-white">
				<i class="fas fa-trash-alt "></i>
				</button></form>';
		$data[] = $row;
		}
		$output = array(
		"draw" => @$_POST['draw'],
		"recordsTotal" => $this->m_stock->count_all(),
		"recordsFiltered" => $this->m_stock->count_filtered(),
		"data" => $data,
		);
		// output to json format
		echo json_encode($output);
    	}
	// END AJAX PROV
	// AJAX ADMINKAB
	function get_ajax_kota()
    	{	
		$list = $this->m_stock->get_datatables_kota();
		$data = array();
		$no = @$_POST['start'];
		foreach ($list as $a) {
		$no++;
		$row = array();
		$row[] = $no . ".";
		$row[] = $a->stock;
		$row[] = $a->bulan;
		$row[] = $a->tahun;
		$row[] = $a->desa;
		$row[] = $a->kecamatan;
		$row[] = $a->kabkota;
		
		// $row[] = strtoupper($a->alamat).','.$a->desa.','.$a->kecamatan.','.$a->kabkota;
		
		// $row[] = $a->jumlah_anggota;
		
		// add html for action
		$row[] = '<form action="' .site_url('stock/del').'" method="post"><a href="' . site_url('stock/edit/' . $a->id) . '" class="btn btn-success text-white btn-xs"><i class="fas fa-pencil-alt"></i></a>
			<input type="hidden" name="id" value="'. $a->id.'"> <button onclick="return confirm(\'Yakin hapus data?\')" class="btn btn-danger btn-xs text-white">
				<i class="fas fa-trash-alt "></i>
				</button></form>';
		$data[] = $row;
		}
		$output = array(
		"draw" => @$_POST['draw'],
		"recordsTotal" => $this->m_stock->count_all_kota($this->session->userdata('kabkota_id')),
		"recordsFiltered" => $this->m_stock->count_filtered_kota($this->session->userdata('kabkota_id')),
		"data" => $data,
		);
		// output to json format
		echo json_encode($output);
    	}
	// END AJAX KAB
	// AJAX GUDANG
	function get_ajax_gudang()
    	{	
		$list = $this->m_stock->get_datatables_gudang();
		$data = array();
		$no = @$_POST['start'];
		foreach ($list as $a) {
		$no++;
		$row = array();
		$row[] = $no . ".";
		$row[] = $a->stock;
		$row[] = $a->bulan;
		$row[] = $a->tahun;
		$row[] = $a->desa;
		$row[] = $a->kecamatan;
		$row[] = $a->kabkota;
		
		// $row[] = strtoupper($a->alamat).','.$a->desa.','.$a->kecamatan.','.$a->kabkota;
		
		// $row[] = $a->jumlah_anggota;
		
		// add html for action
		$row[] = '<form action="' .site_url('stock/del').'" method="post"><a href="' . site_url('stock/edit/' . $a->id) . '" class="btn btn-success text-white btn-xs"><i class="fas fa-pencil-alt"></i></a>
			<input type="hidden" name="id" value="'. $a->id.'"> <button onclick="return confirm(\'Yakin hapus data?\')" class="btn btn-danger btn-xs text-white">
				<i class="fas fa-trash-alt "></i>
				</button></form>';
		$data[] = $row;
		}
		$output = array(
		"draw" => @$_POST['draw'],
		"recordsTotal" => $this->m_stock->count_all_gudang(),
		"recordsFiltered" => $this->m_stock->count_filtered_gudang(),
		"data" => $data,
		);
		// output to json format
		echo json_encode($output);
    	}
	// END AJAX GUDANG

	public function index()
	{	
		$row  = $this->m_stock->get();
		$bulan  = $this->m_stock->bulan();
		$tahun  = $this->m_stock->tahun();
		$kecam  = $this->m_lahan->get_keca_kota();
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();	
		$data = array(
			'row' => $row,
			'bulan' => $bulan,
			'kecam' => $kecam,
			'tahun' => $tahun,
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
		);
		$this->template->load('template', 'admin/stock/v_stock', $data);	
		
	}

	public function add_index()
	{
		$query  = $this->m_stock->getalls();
		$row = $query->row();
		$kabkota = $this->m_stock->get_kab();
		$aktif = $this->m_user->get_aktivasi();
		$kecam  = $this->m_lahan->get_keca_kota();
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();	
		$data = array(
			'row' => $row,
			'kabkota' => $kabkota,
			'kecam' => $kecam,
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
		); 
		$this->template->load('template', 'admin/stock/add_stock', $data);
		
	}
        
	public function add()
	{

		$this->form_validation->set_rules('stock', 'Stock', 'trim|required|min_length[2]');
		
		$this->form_validation->set_message('min_length', '{field} minimal 2 karakter');
		$this->form_validation->set_message('is_unique', '<span style="color:red"> *{field} sudah dipakai</span> ');

		$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

		if ($this->form_validation->run() == FALSE) {
			
            	$kabkota = $this->m_stock->get_kab();
		$aktif = $this->m_user->get_aktivasi();
		$kecam  = $this->m_lahan->get_keca_kota();
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();	
		$data = array(
			'kabkota' => $kabkota,
			'kecam' => $kecam,
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
		);
		$this->template->load('template', 'admin/stock/add_stock', $data);
		} else {
		 if($this->m_stock->add()){
           	$this->session->set_flashdata('info', 'success');
          	 $this->session->set_flashdata('pesan', 'Berhasil menambahkan data.');
           	redirect('stock','refresh');
		}
		echo "<script>window.location ='" . site_url('stock') . "' ; </script>";
		}
	}

	
	public function edit($id)
	{

		$this->form_validation->set_rules('bulan', 'Bulan', 'trim|required|min_length[2]');
		
		$this->form_validation->set_message('min_length', '{field} minimal 5 karakter');
		$this->form_validation->set_message('is_unique', '{field} sudah dipakai');

		$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

		if ($this->form_validation->run() == FALSE) {
			 $query = $this->m_stock->getall($id);
			 $sql = $this->m_stock->getalls();
			if ($query->num_rows() > 0) {
				$row = $query->row();
				$rows = $sql->row();
				$kabkota = $this->m_stock->get_kab();
				$keca = $this->m_stock->get_keca();
				$kecam  = $this->m_lahan->get_keca_kota();
				$desa = $this->m_stock->get_kelu();
				$role = $this->m_stock->get_role();
				$aktif = $this->m_user->get_aktivasi();	
				$aktifkota = $this->m_user->get_aktivasi_kota();
				$hitung = $this->m_user->count();
				$hitungs = $this->m_user->count_kota();	
				 $data = array(
					'row' => $row,
					'rows' => $rows,
					'kabkota' => $kabkota,
					'keca' => $keca,
					'kecam' => $kecam,
					'desa' => $desa,
					'role' => $role,
					'aktifkota' => $aktifkota,
					'aktif' => $aktif,
					'hitung' => $hitung,
					'hitungs' => $hitungs,

				);
				$this->template->load('template', 'admin/stock/edit_stock', $data);
			} else {
				echo "<script>alert ('Data tidak ditemukan');";
				echo "window.location ='" . site_url('stock') . "' ; </scrip>";
			}

            	// redirect('user/add_index');.
		} else {
		// $post = $this->input->post(null, TRUE);
		$this->m_stock->edit();
		 if($this->db->affected_rows() > 0){
		$this->session->set_flashdata('info', 'success');
          	 $this->session->set_flashdata('pesan', 'Berhasil merubah data.');
           	redirect('stock','refresh');
		}
		echo "<script>window.location ='" . site_url('stock') . "' ; </script>";
		}
	}

	function nama_check()
	{

		$post = $this->input->post(NULL, TRUE);
		$query = $this->db->query("SELECT * FROM garam_stock WHERE email= '$post[email]' AND id != '$post[id]'");
		if ($query->num_rows() > 0) {
		$this->form_validation->set_message('nama_check', '{field} ini sudah dipakai, silahkan ganti');
		return FALSE;
		} else {
		return TRUE;
		}
	}

	public function del()
	{
		$id = $this->input->post('id');
		// $this->m_stock->del($id);

		$row = $this->m_stock->get($id);

		if ($row) {
		$this->m_stock->del($id);
		
		$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('pesan', 'Data berhasil dihapus');
			redirect(site_url('stock'));
		} else {
		$this->session->set_flashdata('info', 'danger');
		$this->session->set_flashdata('pesan', 'gagal hapus ');
		redirect(site_url('stock'));
		}
	}

     

	public function aktivasi()
	{
		$id = $this->input->post('id');
		// $this->m_stock->del($id);

		$row = $this->m_stock->get($id);

		if ($row) {
		$this->m_stock->aktivasi($id);
		
		$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('pesan', 'Akun berhasil diaktivasi');
			redirect(site_url('stock'));
		} else {
		$this->session->set_flashdata('info', 'danger');
		$this->session->set_flashdata('pesan', 'gagal aktivasi ');
		redirect(site_url('stock'));
		}
	}

}

/* End of file User.php */
/* Location: ./application/controllers/User.php */