<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;

class Gudang extends CI_Controller {

        function __construct() {
   	     parent::__construct();
		 not_login();
		//  check_admin();
   	        $this->load->model('m_gudang');
		$this->load->model('m_user');
		$this->load->model('m_filter');
		$this->load->model('m_lahan');
		$this->load->model('m_petambak');
		$this->load->library('form_validation');

        }

	//IMPORT EXCEL
	public function uploaddata()
	{
		$config['upload_path'] = './uploads/gudang/';
		$config['allowed_types'] = 'xlsx|xls';
		$config['file_name'] = 'gudang' . time();
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if ($this->upload->do_upload('importexcel')) {
		$file = $this->upload->data();
		$reader = ReaderEntityFactory::createXLSXReader();

		$reader->open('uploads/gudang/' . $file['file_name']);
		foreach ($reader->getSheetIterator() as $sheet) {
			$numRow = 1;
			foreach ($sheet->getRowIterator() as $row) {
			if ($numRow > 1) {
				$databarang = array(
				'nama'  	=> $row->getCellAtIndex(1),
				'kapasitas'  	=> $row->getCellAtIndex(10),
				'kabkota_id'  	=> $row->getCellAtIndex(7),
				'kecamatan_id'  => $row->getCellAtIndex(5),
				'kelurahan_id'  => $row->getCellAtIndex(3),
				// 'koperasi_id'  => $row->getCellAtIndex(5),
				// 'kelompok_id'  => $row->getCellAtIndex(3),
				// 'alamat'       	=> $row->getCellAtIndex(4),
				'latitude'      => $row->getCellAtIndex(8),
				'longitude'     => $row->getCellAtIndex(9),
				'created_at'=>date('Y-m-d H:i:s'),
				);
				$this->m_gudang->import_data($databarang);
			}
			$numRow++;
			}
			$reader->close();
			// unlink('uploads/gudang/' . $file['file_name']);
			$this->session->set_flashdata('pesan', 'import Data Berhasil');
			redirect('gudang');
		}
		} else {
		echo "Error :" . $this->upload->display_errors();
		};
	}

	// AJAX menampilkan LIST berdasarkan provinsi
	function get_ajax()
    	{
		$list = $this->m_gudang->get_datatables();
		// $list = $this->m_items->get_by_jumlah();
		$data = array();
		$no = @$_POST['start'];
		foreach ($list as $a) {
		$no++;
		$row = array();
		$row[] = $no . ".";
		$row[] = $a->nama;
		$row[] = $a->kapasitas;
		$row[] = strtoupper($a->alamat).','.$a->desa.','.$a->kecamatan.','.$a->kabkota;
		$row[] = $a->latitude.",".$a->longitude;
		$row[] = $a->koperasi;
		$row[] = $a->kelompok;
		// add html for action
		$row[] = '<form action="' .site_url('gudang/del').'" method="post"><a href="' . site_url('gudang/edit/' . $a->id) . '" class="btn btn-success text-white btn-xs"><i class="fas fa-pencil-alt"></i></a>
			<input type="hidden" name="id" value="'. $a->id.'"> <button onclick="return confirm(\'Yakin hapus data?\')" class="btn btn-danger btn-xs text-white">
				<i class="fas fa-trash-alt "></i>
				</button></form>';
		$data[] = $row;
		}
		$output = array(
		"draw" => @$_POST['draw'],
		"recordsTotal" => $this->m_gudang->count_all(),
		"recordsFiltered" => $this->m_gudang->count_filtered(),
		"data" => $data,
		);
		// output to json format
		echo json_encode($output);
    	}

	// AJAX menampilkan LIST berdasarkan kota
	function get_ajax_kota()
    	{	
		
		$list = $this->m_gudang->get_datatables_kota();
		$data = array();
		$no = @$_POST['start'];
		foreach ($list as $a) {
		$no++;
		$row = array();
		$row[] = $no . ".";
		$row[] = $a->nama;
		$row[] = $a->kapasitas;
		$row[] = strtoupper($a->alamat).','.$a->desa.','.$a->kecamatan.','.$a->kabkota;
		$row[] = $a->latitude.",".$a->longitude;
		$row[] = $a->koperasi;
		$row[] = $a->kelompok;
		// add html for action
		$row[] = '<form action="' .site_url('gudang/del').'" method="post"><a href="' . site_url('gudang/edit/' . $a->id) . '" class="btn btn-success text-white btn-xs"><i class="fas fa-pencil-alt"></i></a>
			<input type="hidden" name="id" value="'. $a->id.'"> <button onclick="return confirm(\'Yakin hapus data?\')" class="btn btn-danger btn-xs text-white">
				<i class="fas fa-trash-alt "></i>
				</button></form>';
		$data[] = $row;
		}
		$output = array(
		"draw" => @$_POST['draw'],
		"recordsTotal" => $this->m_gudang->count_all_kota($this->session->userdata('kabkota_id')),
		"recordsFiltered" => $this->m_gudang->count_filtered_kota($this->session->userdata('kabkota_id')),
		"data" => $data,
		);
		// output to json format
		echo json_encode($output);
    	}


	public function index()
	{	
		$row  = $this->m_gudang->get();
		$rows  = $this->m_gudang->get_gudang();
		$kecam = $this->m_lahan->get_keca_kota();
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();		
		$data = array(
			'row' => $row,
			'rows' => $rows,
			'kecam' => $kecam,
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
		);
		$this->template->load('template', 'admin/gudang/v_gudang', $data);		
		// $this->load->view('template/headx',$data);
		// $this->load->view('admin/gudang/v_gudang',$data);
		// $this->load->view('template/footerx');
		
	}

	function list_sub()
	{
		$id_kota = $this->input->post('id');
		$sub = $this->m_user->get_kec($id_kota);

		// Buat variabel untuk menampung tag-tag option nya
		// Set defaultnya dengan tag option Pilih
		$lists = "<option value='00'>Pilih</option>";

		foreach ($sub as $data) {
		$lists .= "<option value='" . $data->id . "'>" . $data->name . "</option>"; // Tambahkan tag option ke variabel $lists
		}

		$callback = array('list_sub' => $lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota

		echo json_encode($callback); // konversi varibael $callback menjadi JSON
	}

	function list_desa()
	{
		$id_kec = $this->input->post('id');


		$sub = $this->m_user->get_kel($id_kec);

		// Buat variabel untuk menampung tag-tag option nya
		// Set defaultnya dengan tag option Pilih
		$lists = "<option value='00'>Pilih</option>";

		foreach ($sub as $data) {
		$lists .= "<option value='" . $data->id . "'>" . $data->name . "</option>"; // Tambahkan tag option ke variabel $lists
		}

		$callback = array('list_desa' => $lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota

		echo json_encode($callback); // konversi varibael $callback menjadi JSON
	}

	function list_koperasi()
	{
		$id_kel = $this->input->post('id');


		$sub = $this->m_gudang->get_koperasi($id_kel);

		// Buat variabel untuk menampung tag-tag option nya
		// Set defaultnya dengan tag option Pilih
		$lists = "<option value='00'>Pilih</option>";

		foreach ($sub as $data) {
		$lists .= "<option value='" . $data->id . "'>" . $data->nama . "</option>"; // Tambahkan tag option ke variabel $lists
		}

		$callback = array('list_koperasi' => $lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota

		echo json_encode($callback); // konversi varibael $callback menjadi JSON
	}

	function list_kelompok()
	{
		$id_kop = $this->input->post('id');


		$sub = $this->m_gudang->get_kelompok($id_kop);

		// Buat variabel untuk menampung tag-tag option nya
		// Set defaultnya dengan tag option Pilih
		$lists = "<option value='00'>Pilih</option>";

		foreach ($sub as $data) {
		$lists .= "<option value='" . $data->id . "'>" . $data->nama . "</option>"; // Tambahkan tag option ke variabel $lists
		}

		$callback = array('list_kelompok' => $lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota

		echo json_encode($callback); // konversi varibael $callback menjadi JSON
	}

	public function indexkab()
	{
		
		$row  = $this->m_gudang->get_gudang($this->session->userdata('kabkota_id'));
		$aktif = $this->m_gudang->get_aktivasi();	
		$hitung = $this->m_gudang->count();	
		$data = array(
			'row' => $row,
			'aktif' => $aktif,
			'hitung' => $hitung,
		);
		$this->template->load('template', 'admin/user/v_user', $data);
		// $this->load->view('template/headx',$data);
		// $this->load->view('admin/user/v_user',$data);
		// $this->load->view('template/footerx');
	}
	

	public function add_index()
	{
		$kabkota = $this->m_gudang->get_kab();
		$kecam = $this->m_lahan->get_keca_kota();
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();		
		$data = array(
			'kabkota' => $kabkota,
			'kecam' =>$kecam,
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
		); 
		$this->template->load('template', 'admin/gudang/add_gudang', $data);
		// $this->load->view('template/headx',$data);
		// $this->load->view('admin/gudang/add_gudang',$data);
		// $this->load->view('template/footerx');
		
	}
        
	public function add()
	{

		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|min_length[5]|is_unique[garam_gudang.nama]');
		
		$this->form_validation->set_message('min_length', '{field} minimal 5 karakter');
		$this->form_validation->set_message('is_unique', '<span style="color:red"> *{field} sudah dipakai</span> ');

		$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

		if ($this->form_validation->run() == FALSE) {
			
            	$kabkota = $this->m_gudang->get_kab();
		$kecam = $this->m_lahan->get_keca_kota();
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();	
		$data = array(
			'kabkota' => $kabkota,
			'kecam' =>$kecam,
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
		);
		$this->template->load('template', 'admin/gudang/add_gudang', $data);
		// $this->load->view('template/headx',$data);
		// $this->load->view('admin/gudang/add_gudang',$data);
		// $this->load->view('template/footerx');
		} else {
		 if($this->m_gudang->add()){
           	$this->session->set_flashdata('info', 'success');
          	 $this->session->set_flashdata('pesan', 'Berhasil menambahkan data.');
           	redirect('gudang','refresh');
		}
		echo "<script>window.location ='" . site_url('gudang') . "' ; </script>";
		}
	}

	
	public function edit($id)
	{

		$this->form_validation->set_rules('nama', 'Nama', 'min_length[3]|callback_nama_check');
		
		$this->form_validation->set_message('min_length', '{field} minimal 5 karakter');
		$this->form_validation->set_message('is_unique', '{field} sudah dipakai');

		$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

		if ($this->form_validation->run() == FALSE) {
			 $query = $this->m_gudang->getall($id);
			if ($query->num_rows() > 0) {
				$row = $query->row();
				$kabkota = $this->m_gudang->get_kab();
				$kecam = $this->m_lahan->get_keca_kota();
				$keca = $this->m_gudang->get_keca();
				$desa = $this->m_gudang->get_kelu();
				$koperasi = $this->m_petambak->get_koperasi();
				$kelompok = $this->m_petambak->get_kelompok();
				$role = $this->m_gudang->get_role();
				$aktif = $this->m_user->get_aktivasi();	
				$aktifkota = $this->m_user->get_aktivasi_kota();
				$hitung = $this->m_user->count();
				$hitungs = $this->m_user->count_kota();	
				 $data = array(
					'row' => $row,
					'kabkota' => $kabkota,
					'kecam' => $kecam,
					'keca' => $keca,
					'desa' => $desa,
					'koperasi' => $koperasi,
					'kelompok' => $kelompok,
					'role' => $role,
					'aktifkota' => $aktifkota,
					'aktif' => $aktif,
					'hitung' => $hitung,
					'hitungs' => $hitungs,

				);
				$this->template->load('template', 'admin/gudang/edit_gudang', $data);
				
				// $this->load->view('template/headx',$data);
				// $this->load->view('admin/gudang/edit_gudang',$data);
				// $this->load->view('template/footerx');
				// echo "<script>alert ('Data tidak ditemukan');";
			} else {
				echo "<script>alert ('Data tidak ditemukan');";
				echo "window.location ='" . site_url('user') . "' ; </scrip>";
			}

            	// redirect('user/add_index');.
		} else {
		// $post = $this->input->post(null, TRUE);
		$this->m_gudang->edit();
		 if($this->db->affected_rows() > 0){
		$this->session->set_flashdata('info', 'success');
          	 $this->session->set_flashdata('pesan', 'Berhasil merubah data.');
           	redirect('gudang','refresh');
		}
		echo "<script>window.location ='" . site_url('gudang') . "' ; </script>";
		}
	}
	

	function nama_check()
	{

		$post = $this->input->post(NULL, TRUE);
		$query = $this->db->query("SELECT * FROM garam_gudang WHERE nama= '$post[nama]' AND id != '$post[id]'");
		if ($query->num_rows() > 0) {
		$this->form_validation->set_message('nama_check', '{field} ini sudah dipakai, silahkan ganti');
		return FALSE;
		} else {
		return TRUE;
		}
	}

	public function del()
	{
		$id = $this->input->post('id');
		// $this->m_gudang->del($id);

		$row = $this->m_gudang->get($id);

		if ($row) {
		$this->m_gudang->del($id);
		
		$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('pesan', 'Data berhasil dihapus');
			redirect(site_url('gudang'));
		} else {
		$this->session->set_flashdata('info', 'danger');
		$this->session->set_flashdata('pesan', 'gagal hapus ');
		redirect(site_url('gudang'));
		}
	}

     

	public function aktivasi()
	{
		$id = $this->input->post('id');
		// $this->m_gudang->del($id);

		$row = $this->m_gudang->get($id);

		if ($row) {
		$this->m_gudang->aktivasi($id);
		
		$this->session->set_flashdata('info', 'success');
		$this->session->set_flashdata('pesan', 'Akun berhasil diaktivasi');
			redirect(site_url('user'));
		} else {
		$this->session->set_flashdata('info', 'danger');
		$this->session->set_flashdata('pesan', 'gagal aktivasi ');
		redirect(site_url('user'));
		}
	}	

}

/* End of file User.php */
/* Location: ./application/controllers/User.php */