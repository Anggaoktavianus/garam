<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Koperasi extends CI_Controller {

         function __construct() {
   	     parent::__construct();
		 not_login();
		//  check_admin();
   	        $this->load->model('m_koperasi');
		$this->load->model('m_user');
		$this->load->model('m_lahan');
		$this->load->library('form_validation');

         }

	// AJAX menampilkan LIST berdasarkan provinsi
	 function get_ajax()
    	{
		$list = $this->m_koperasi->get_datatables();
		$data = array();
		$no = @$_POST['start'];
		foreach ($list as $a) {
		$no++;
		$row = array();
		$row[] = $no . ".";
		$row[] = $a->nama;
		$row[] = $a->no_akta;
		$row[] = $a->tgl_akta;
		$row[] = $a->telepon;
		$row[] = $a->email;
		$row[] = strtoupper($a->alamat).','.$a->desa.','.$a->kecamatan.','.$a->kabkota;
		$row[] = $a->ketua;
		$row[] = $a->jumlah_anggota;
		
		// add html for action
		$row[] = '<form action="' .site_url('koperasi/del').'" method="post"><a href="' . site_url('koperasi/edit/' . $a->id) . '" class="btn btn-success text-white btn-xs"><i class="fas fa-pencil-alt"></i></a>
			<input type="hidden" name="id" value="'. $a->id.'"> <button onclick="return confirm(\'Yakin hapus data?\')" class="btn btn-danger btn-xs text-white">
				<i class="fas fa-trash-alt "></i>
				</button></form>';
		$data[] = $row;
		}
		$output = array(
		"draw" => @$_POST['draw'],
		"recordsTotal" => $this->m_koperasi->count_all(),
		"recordsFiltered" => $this->m_koperasi->count_filtered(),
		"data" => $data,
		);
		// output to json format
		echo json_encode($output);
    	}

	// AJAX menampilkan LIST berdasarkan KOTA
	function get_ajax_kota()
    	{	
		$list = $this->m_koperasi->get_datatables_kota();
		$data = array();
		$no = @$_POST['start'];
		foreach ($list as $a) {
		$no++;
		$row = array();
		$row[] = $no . ".";
		$row[] = $a->nama;
		$row[] = $a->no_akta;
		$row[] = $a->tgl_akta;
		$row[] = $a->telepon;
		$row[] = $a->email;
		$row[] = strtoupper($a->alamat).','.$a->desa.','.$a->kecamatan.','.$a->kabkota;
		$row[] = $a->ketua;
		$row[] = $a->jumlah_anggota;
		
		// add html for action
		$row[] = '<form action="' .site_url('koperasi/del').'" method="post"><a href="' . site_url('koperasi/edit/' . $a->id) . '" class="btn btn-success text-white btn-xs"><i class="fas fa-pencil-alt"></i></a>
			<input type="hidden" name="id" value="'. $a->id.'"> <button onclick="return confirm(\'Yakin hapus data?\')" class="btn btn-danger btn-xs text-white">
				<i class="fas fa-trash-alt "></i>
				</button></form>';
		$data[] = $row;
		}
		$output = array(
		"draw" => @$_POST['draw'],
		"recordsTotal" => $this->m_koperasi->count_all_kota($this->session->userdata('kabkota_id')),
		"recordsFiltered" => $this->m_koperasi->count_filtered_kota($this->session->userdata('kabkota_id')),
		"data" => $data,
		);
		// output to json format
		echo json_encode($output);
    	}

	public function index()
	{	
		$row  = $this->m_koperasi->get();
		$kecam  = $this->m_lahan->get_keca_kota();
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();		
		$data = array(
			'row' => $row,
			'kecam'=>$kecam,
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
		);
		$this->template->load('template', 'admin/koperasi/v_koperasi', $data);	
		
	}
	

	public function add_index()
	{
		$kabkota = $this->m_koperasi->get_kab();
		$kecam  = $this->m_lahan->get_keca_kota();
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();		
		$data = array(
			'kabkota' => $kabkota,
			'kecam'=>$kecam,
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
		); 
		$this->template->load('template', 'admin/koperasi/add_koperasi', $data);
		
	}
        
	public function add()
	{

		$this->form_validation->set_rules('email', 'Email', 'trim|required|min_length[5]|is_unique[garam_koperasi.email]');
		
		$this->form_validation->set_message('min_length', '{field} minimal 5 karakter');
		$this->form_validation->set_message('is_unique', '<span style="color:red"> *{field} sudah dipakai</span> ');

		$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

		if ($this->form_validation->run() == FALSE) {
			
            	$kabkota = $this->m_koperasi->get_kab();
		$kecam  = $this->m_lahan->get_keca_kota();
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();		
		$data = array(
			'kabkota' => $kabkota,
			'kecam'=>$kecam,
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
		);
		$this->template->load('template', 'admin/koperasi/add_koperasi', $data);
		} else {
		 if($this->m_koperasi->add()){
           	$this->session->set_flashdata('info', 'success');
          	 $this->session->set_flashdata('pesan', 'Berhasil menambahkan data.');
           	redirect('koperasi','refresh');
		}
		echo "<script>window.location ='" . site_url('koperasi') . "' ; </script>";
		}
	}

	
	public function edit($id)
	{

		$this->form_validation->set_rules('email', 'Email', 'min_length[3]|callback_nama_check');
		
		$this->form_validation->set_message('min_length', '{field} minimal 5 karakter');
		$this->form_validation->set_message('is_unique', '{field} sudah dipakai');

		$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

		if ($this->form_validation->run() == FALSE) {
			 $query = $this->m_koperasi->getall($id);
			if ($query->num_rows() > 0) {
				$row = $query->row();
				$kabkota = $this->m_koperasi->get_kab();
				$kecam  = $this->m_lahan->get_keca_kota();
				$keca = $this->m_koperasi->get_keca();
				$desa = $this->m_koperasi->get_kelu();
				$role = $this->m_koperasi->get_role();
				$aktif = $this->m_user->get_aktivasi();	
				$aktifkota = $this->m_user->get_aktivasi_kota();
				$hitung = $this->m_user->count();
				$hitungs = $this->m_user->count_kota();	
				 $data = array(
					'row' 	=> $row,
					'kecam' =>$kecam,
					'kabkota' => $kabkota,
					'keca' => $keca,
					'desa' => $desa,
					'role' => $role,
					'aktifkota' => $aktifkota,
					'aktif' => $aktif,
					'hitung' => $hitung,
					'hitungs' => $hitungs,

				);
				$this->template->load('template', 'admin/koperasi/edit_koperasi', $data);
			} else {
				echo "<script>alert ('Data tidak ditemukan');";
				echo "window.location ='" . site_url('koperasi') . "' ; </scrip>";
			}

            	// redirect('user/add_index');.
		} else {
		// $post = $this->input->post(null, TRUE);
		$this->m_koperasi->edit();
		 if($this->db->affected_rows() > 0){
		$this->session->set_flashdata('info', 'success');
          	 $this->session->set_flashdata('pesan', 'Berhasil merubah data.');
           	redirect('koperasi','refresh');
		}
		echo "<script>window.location ='" . site_url('koperasi') . "' ; </script>";
		}
	}

	function nama_check()
	{

		$post = $this->input->post(NULL, TRUE);
		$query = $this->db->query("SELECT * FROM garam_koperasi WHERE email= '$post[email]' AND id != '$post[id]'");
		if ($query->num_rows() > 0) {
		$this->form_validation->set_message('nama_check', '{field} ini sudah dipakai, silahkan ganti');
		return FALSE;
		} else {
		return TRUE;
		}
	}

	public function del()
	{
		$id = $this->input->post('id');
		// $this->m_koperasi->del($id);

		$row = $this->m_koperasi->get($id);

		if ($row) {
		$this->m_koperasi->del($id);
		
		$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('pesan', 'Data berhasil dihapus');
			redirect(site_url('koperasi'));
		} else {
		$this->session->set_flashdata('info', 'danger');
		$this->session->set_flashdata('pesan', 'gagal hapus ');
		redirect(site_url('koperasi'));
		}
	}

	public function aktivasi()
	{
		$id = $this->input->post('id');
		// $this->m_koperasi->del($id);

		$row = $this->m_koperasi->get($id);

		if ($row) {
		$this->m_koperasi->aktivasi($id);
		
		$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('pesan', 'Akun berhasil diaktivasi');
			redirect(site_url('koperasi'));
		} else {
		$this->session->set_flashdata('info', 'danger');
		$this->session->set_flashdata('pesan', 'gagal aktivasi ');
		redirect(site_url('koperasi'));
		}
	}

}

/* End of file User.php */
/* Location: ./application/controllers/User.php */