<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

        function __construct() {
   	     parent::__construct();
		 not_login();
		//  check_admin();
   	        $this->load->model('m_user');
		$this->load->model('m_koperasi');
		$this->load->library('form_validation');

        }

	public function index()
	{	
		$row  = $this->m_user->get();
		$rows  = $this->m_user->get_gudang();
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();		
		$data = array(
			'row' => $row,
			'rows' => $rows,
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
		);
		$this->template->load('template', 'admin/user/v_user', $data);
		// $this->load->view('template/headx',$data);
		// $this->load->view('admin/user/v_user',$data);
		// $this->load->view('template/footerx');
	}

	public function indexkab()
	{
		
		$row  = $this->m_user->get_gudang(); 
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();		
		$data = array(
			'row' => $row,
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
		);
		$this->template->load('template', 'admin/user/v_user', $data);
		// $this->load->view('template/headx',$data);
		// $this->load->view('admin/user/v_user',$data);
		// $this->load->view('template/footerx');
	}

	public function add_index()
	{
		$kabkota = $this->m_user->get_kab();
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();		
		$data = array(
			'kabkota' => $kabkota,
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
		);
		$this->template->load('template', 'admin/user/add_user', $data);
		// $this->load->view('template/headx',$data);
		// $this->load->view('admin/user/add_user',$data);
		// $this->load->view('template/footerx');
		
	}
        
	public function add()
	{

		$this->form_validation->set_rules('email', 'Email', 'trim|required|min_length[5]|is_unique[garam_admin.email]');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|min_length[5]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		$this->form_validation->set_rules(
		'password2',
		'Konfirmasi password',
		'trim|required|matches[password]',
		array('matches' => '{field} tidak sesuai dengan password')
		);
		$this->form_validation->set_rules('role_id', 'Akses', 'trim|required');

		$this->form_validation->set_message('required', '{field} masih kosong, silahkan diisi');
		$this->form_validation->set_message('min_length', '{field} minimal 5 karakter');
		$this->form_validation->set_message('is_unique', '<span style="color:red"> *{field} sudah dipakai</span> ');

		$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

		if ($this->form_validation->run() == FALSE) {
			
            	$kabkota = $this->m_user->get_kab();
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();	
		
		$data = array(
			'kabkota' => $kabkota,
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
			
		);
		$this->template->load('template', 'admin/user/add_user', $data);
		// $this->load->view('template/headx',$data);
		// $this->load->view('admin/user/add_user',$data);
		// $this->load->view('template/footerx');
		} else {
		 if($this->m_user->m_register()){
           	$this->session->set_flashdata('info', 'success');
          	 $this->session->set_flashdata('pesan', 'Berhasil menambahkan data.');
           	redirect('user','refresh');
		}
		echo "<script>window.location ='" . site_url('user') . "' ; </script>";
		}
	}

	public function edit_index()
	{
		$kabkota = $this->m_user->get_kab();
		$role = $this->m_user->get_role();
		$kabkota = $this->m_user->get_kab();
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();		
		$data = array(
			'kabkota' => $kabkota,
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
			'role' => $role,
		);
		$this->template->load('template', 'admin/user/edit_user', $data);
		// $this->load->view('template/headx',$data);
		// $this->load->view('admin/user/edit_user',$data);
		// $this->load->view('template/footerx');
		
	}
	public function edit($id)
	{

		$this->form_validation->set_rules('email', 'email', 'min_length[3]|callback_email_check');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|min_length[5]');
		if ($this->input->post('password')) {
		$this->form_validation->set_rules('password', 'Password', 'min_length[5]');
		$this->form_validation->set_rules(
			'password2',
			'Konfirmasi password',
			'matches[password]',
			array('matches' => '{field} tidak sesuai dengan password')
		);
		}
		if ($this->input->post('password2')) {
		$this->form_validation->set_rules(
			'password2',
			'Konfirmasi password',
			'matches[password]',
			array('matches' => '{field} tidak sesuai dengan password')
		);
		}
		$this->form_validation->set_rules('role_id', 'Akses', 'trim|required');

		$this->form_validation->set_message('required', '{field} masih kosong, silahkan diisi');
		$this->form_validation->set_message('min_length', '{field} minimal 5 karakter');
		$this->form_validation->set_message('is_unique', '{field} sudah dipakai');

		$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

		if ($this->form_validation->run() == FALSE) {
			 $query = $this->m_user->getall($id);
			if ($query->num_rows() > 0) {
				$row = $query->row();
				$kabkota = $this->m_user->get_kab();
				$role = $this->m_user->get_role();
				$aktif = $this->m_user->get_aktivasi();	
				$aktifkota = $this->m_user->get_aktivasi_kota();
				$hitung = $this->m_user->count();
				$hitungs = $this->m_user->count_kota();	
				$keca = $this->m_koperasi->get_keca();
				$desa = $this->m_koperasi->get_kelu();	
				 $data = array(
					'row' => $row,
					'kabkota' => $kabkota,
					'role' => $role,
					'aktifkota' => $aktifkota,
					'aktif' => $aktif,
					'hitung' => $hitung,
					'hitungs' => $hitungs,
					'keca' => $keca,
					'desa' => $desa,

				);
		
				$this->template->load('template', 'admin/user/edit_user', $data);
				// $this->load->view('template/headx',$data);
				// $this->load->view('admin/user/edit_user',$data);
				// $this->load->view('template/footerx');
				// echo "<script>alert ('Data tidak ditemukan');";
			} else {
				echo "<script>alert ('Data tidak ditemukan');";
				echo "window.location ='" . site_url('user') . "' ; </scrip>";
			}

            	// redirect('user/add_index');

            # code...
		} else {
		// $post = $this->input->post(null, TRUE);
		$this->m_user->edit();
		 if($this->db->affected_rows() > 0){
		$this->session->set_flashdata('info', 'success');
          	 $this->session->set_flashdata('pesan', 'Berhasil merubah data user.');
           	redirect('user','refresh');
		}
		echo "<script>window.location ='" . site_url('user') . "' ; </script>";
		}
	}
	 

	public function profile()
	{	$id = $this->input->post('id');
		// $this->form_validation->set_rules('email', 'email', 'min_length[3]|callback_email_check');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|min_length[5]');
		// $this->form_validation->set_rules('role_id', 'Akses', 'trim|required');

		$this->form_validation->set_message('required', '{field} masih kosong, silahkan diisi');
		$this->form_validation->set_message('min_length', '{field} minimal 5 karakter');
		$this->form_validation->set_message('is_unique', '{field} sudah dipakai');

		$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');
		$kabkota = $this->m_user->get_kab();
		$query = $this->m_user->getalls($id);
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();	
		$row = $query->row();	
		$user = $this->db->get_where('garam_admin',['email' => $this->session->userdata('email')])->row_array(); 	
		$data = array(
			'kabkota' => $kabkota,
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
			'user' => $user,
			'row' => $row,
		);

		if ($this->form_validation->run() == FALSE) {
			$this->template->load('template', 'admin/profile', $data);
			// $this->session->set_flashdata('info', 'danger');
          	 	// $this->session->set_flashdata('pesan', 'Gagal merubah data user.');
		}else {
			$name 		= $this->input->post('nama');
			$email 		= $this->input->post('email');
			$kabkota 	= $this->input->post('kabkota');
			$kecamatan 	= $this->input->post('kecamatan');
			$desa 		= $this->input->post('desa');

			$this->db->set('nama',$name);
			$this->db->set('kabkota_id',$kabkota);
			$this->db->set('kecamatan_id',$kecamatan);
			$this->db->set('kelurahan_id',$desa);
			// $this->db->set('email',$email);

			$this->db->where('email', $email);
			$this->db->update('garam_admin');

		 $this->session->set_flashdata('info', 'success');
          	 $this->session->set_flashdata('pesan', 'Berhasil merubah data user.');
           		redirect('user/profile','refresh');
		}
		
	}

	public function changepassword()
	{
		// $this->form_validation->set_rules('email', 'email', 'min_length[3]|callback_email_check');
		$this->form_validation->set_rules('password', 'Old Password', 'trim|required|min_length[5]');
		$this->form_validation->set_rules('newpassword', 'New Password', 'trim|required|min_length[5]|matches[newpassword2]');
		$this->form_validation->set_rules('newpassword2', 'Confirm New Password', 'trim|required|min_length[5]|matches[newpassword]');

		$this->form_validation->set_message('required', '{field} masih kosong, silahkan diisi');
		$this->form_validation->set_message('min_length', '{field} minimal 5 karakter');
		$this->form_validation->set_message('is_unique', '{field} sudah dipakai');

		$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');
		
		$aktif = $this->m_user->get_aktivasi();	
		$hitung = $this->m_user->count();
		$user = $this->db->get_where('garam_admin',['email' => $this->session->userdata('email')])->row_array(); 	
		$data = array(
			'aktif' => $aktif,
			'hitung' => $hitung,
			'user' => $user,
		);

		if ($this->form_validation->run() == FALSE) {
			$this->template->load('template', 'admin/profile', $data);
		}else {
			$passwordlama = $this->input->post('password');
			$passwordbaru = $this->input->post('newpassword');
			if (!password_verify($passwordlama,$user['password'])) {
				 $this->session->set_flashdata('info', 'danger');
          			 $this->session->set_flashdata('pesan', 'Password lama salah');
           			redirect('user/profile','refresh');
			}else {
				if ($passwordbaru == $passwordlama) {
				$this->session->set_flashdata('info', 'danger');
          			 $this->session->set_flashdata('pesan', 'Password baru tidak boleh sama dengan password lama');
           			redirect('user/profile','refresh');
				}else {
					$passwordhash = get_hash($passwordbaru);
					$this->db->set('password',$passwordhash);
					$this->db->where('email',$this->session->userdata('email'));
					$this->db->update('garam_admin');
					 $this->session->set_flashdata('info', 'success');
          			 $this->session->set_flashdata('pesan', 'Password berhasil diubah');
				   redirect('user/profile','refresh');
				}
			}
		}
		
	}
	

	function email_check()
	{

		$post = $this->input->post(NULL, TRUE);
		$query = $this->db->query("SELECT * FROM garam_admin WHERE email= '$post[email]' AND id != '$post[id]'");
		if ($query->num_rows() > 0) {
		$this->form_validation->set_message('email_check', '{field} ini sudah dipakai, silahkan ganti');
		return FALSE;
		} else {
		return TRUE;
		}
	}

	public function del()
	{
		$id = $this->input->post('id');
		// $this->m_user->del($id);

		$row = $this->m_user->get($id);

		if ($row) {
		$this->m_user->del($id);
		
		$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('pesan', 'Data berhasil dihapus');
			redirect(site_url('user'));
		} else {
		$this->session->set_flashdata('info', 'danger');
		$this->session->set_flashdata('pesan', 'gagal hapus ');
		redirect(site_url('user'));
		}
	}

	public function reset($id)
	{
	
		$row = $this->m_user->reset($id);
		if ($row) {
			
			$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('pesan', 'Reset Password Berhasil');
			redirect(site_url('user'));
		} else {
			$this->session->set_flashdata('info', 'danger');
			$this->session->set_flashdata('pesan', 'Reset Password Gagal');
			redirect(site_url('user'));
		}
	}

	public function aktivasi()
	{
		$id = $this->input->post('id');
		// $this->m_user->del($id);

		$row = $this->m_user->get($id);

		if ($row) {
		$this->m_user->aktivasi($id);
		
		$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('pesan', 'Akun berhasil diaktivasi');
			redirect(site_url('user'));
		} else {
		$this->session->set_flashdata('info', 'danger');
		$this->session->set_flashdata('pesan', 'gagal aktivasi ');
		redirect(site_url('user'));
		}
	}

}

/* End of file User.php */
/* Location: ./application/controllers/User.php */