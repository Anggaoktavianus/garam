<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;

class Filter extends CI_Controller {

        function __construct() {
   	     	parent::__construct();
		 not_login();
		//  check_admin();
   	        $this->load->model('m_gudang');
		$this->load->model('m_user');
		$this->load->model('m_filter');
		$this->load->library('form_validation');

         }

	// FILTER GUDANG 
	public function index()
	{
		$this->load->helper('url');
		$this->load->helper('form');
		$id_kota = $this->input->post('id');
		$kota = $this->m_filter->get_list_kota();
		// $kecamatan = $this->m_filter->get_list_kecamatan($id_kota);
		$row  = $this->m_gudang->get();
		$rows  = $this->m_gudang->get_gudang();
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();	
		$kabkota = $this->m_gudang->get_kab();	
		$data = array(
			'row' => $row,
			'kabkota' => $kabkota,
			'rows' => $rows,
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
		);

		$opt = array('' => '-Select-');
		foreach ($kota as $country) {
			$opt[$country] = $country;
		}
		// $opts = array('' => '-Select-');
		// foreach ($kecamatan as $kec) {
		// 	$opts[$kec] = $kec;
		// }
//  
		$data['form_kota'] = form_dropdown('',$opt,'','id="name" class="form-control"');
		// $data['form_kecamatan'] = form_dropdown('',$opts,'','id="kecamatan" class="form-control"');
		$this->template->load('template', 'admin/gudang/import', $data);
	}

	
    	public function ajax_list()
	{

		$posts = $this->m_filter->get_datatables(); 

		$data = array();
		$no = $this->input->post('start');
		foreach ($posts as $post) 
		{
		 
		$no++;
		$row = array();
		$row[] = $no;
		$row[] = "";
		$row[] = $post->desa;
		$row[] = $post->id_des;
		$row[] = $post->kecamatan;
		$row[] = $post->id_kec;
		$row[] = $post->name;
		$row[] = $post->id;
		$row[] = "";
		$row[] = "";
		$row[] = "";
		
		$data[] = $row;
		}
		
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->m_filter->count_all(),
			"recordsFiltered" => $this->m_filter->count_filtered(),
			"data" => $data,);
			//output to json format
			echo json_encode($output);
	}
 
	// END FILTER GUDANG
	// FILTER STOK 
	public function stok()
	{
		$this->load->helper('url');
		$this->load->helper('form');
		$id_kota = $this->input->post('id');
		$kota = $this->m_filter->get_list_kota();
		$row  = $this->m_gudang->get();
		$rows  = $this->m_gudang->get_gudang();
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();	
		$kabkota = $this->m_gudang->get_kab();	
		$data = array(
			'row' => $row,
			'kabkota' => $kabkota,
			'rows' => $rows,
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
		);

		$opt = array('' => '-Select-');
		foreach ($kota as $country) {
			$opt[$country] = $country;
		}
//  
		$data['form_kota'] = form_dropdown('',$opt,'','id="name" class="form-control"');
		// $data['form_kecamatan'] = form_dropdown('',$opts,'','id="kecamatan" class="form-control"');
		$this->template->load('template', 'admin/stock/import', $data);
	}

	
    	public function ajax_list_stok()
	{

		$posts = $this->m_filter->get_datatables(); 

		$data = array();
		$no = $this->input->post('start');
		foreach ($posts as $post) 
		{
		 
		$no++;
		$row = array();
		$row[] = $no;
		$row[] = "";
		$row[] = "";
		$row[] = $post->desa;
		$row[] = $post->id_des;
		$row[] = $post->kecamatan;
		$row[] = $post->id_kec;
		$row[] = $post->name;
		$row[] = $post->id;
		$row[] = "";
		
		$data[] = $row;
		}
		
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->m_filter->count_all(),
			"recordsFiltered" => $this->m_filter->count_filtered(),
			"data" => $data,);
			//output to json format
			echo json_encode($output);
	}
 
	// END FILTER STOK

	// FILTER PETAMBAK 
	public function petambak()
	{
		$this->load->helper('url');
		$this->load->helper('form');
		$id_kota = $this->input->post('id');
		$kota = $this->m_filter->get_list_kota();
		$row  = $this->m_gudang->get();
		$rows  = $this->m_gudang->get_gudang();
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();	
		$kabkota = $this->m_gudang->get_kab();	
		$data = array(
			'row' => $row,
			'kabkota' => $kabkota,
			'rows' => $rows,
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
		);

		$opt = array('' => '-Select-');
		foreach ($kota as $country) {
			$opt[$country] = $country;
		}
//  
		$data['form_kota'] = form_dropdown('',$opt,'','id="name" class="form-control"');
		// $data['form_kecamatan'] = form_dropdown('',$opts,'','id="kecamatan" class="form-control"');
		$this->template->load('template', 'admin/petambak/import', $data);
	}

	
    	public function ajax_list_petambak()
	{

		$posts = $this->m_filter->get_datatables(); 
		$data = array();
		$no = $this->input->post('start');
		foreach ($posts as $post) 
		{
		 
		$no++;
		$row = array();
		$row[] = $no;
		$row[] = $post->desa;
		$row[] = $post->id_des;
		$row[] = $post->kecamatan;
		$row[] = $post->id_kec;
		$row[] = $post->name;
		$row[] = $post->id;
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "yyyy-mm-dd";
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "";
		
		$data[] = $row;
		}
		
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->m_filter->count_all(),
			"recordsFiltered" => $this->m_filter->count_filtered(),
			"data" => $data,);
			//output to json format
			echo json_encode($output);
	}
 
	// END FILTER PETAMBAK

	// FILTER KELOMPOK 
	public function kelompok()
	{
		$this->load->helper('url');
		$this->load->helper('form');
		$id_kota = $this->input->post('id');
		$kota = $this->m_filter->get_list_kota();
		$row  = $this->m_gudang->get();
		$rows  = $this->m_gudang->get_gudang();
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();	
		$kabkota = $this->m_gudang->get_kab();	
		$data = array(
			'row' => $row,
			'kabkota' => $kabkota,
			'rows' => $rows,
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
		);

		$opt = array('' => '-Select-');
		foreach ($kota as $country) {
			$opt[$country] = $country;
		}
//  
		$data['form_kota'] = form_dropdown('',$opt,'','id="name" class="form-control"');
		// $data['form_kecamatan'] = form_dropdown('',$opts,'','id="kecamatan" class="form-control"');
		$this->template->load('template', 'admin/kelompok/import', $data);
	}

	
    	public function ajax_list_kelompok()
	{

		$posts = $this->m_filter->get_datatables(); 
		$data = array();
		$no = $this->input->post('start');
		foreach ($posts as $post) 
		{
		 
		$no++;
		$row = array();
		$row[] = $no;
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = $post->desa;
		$row[] = $post->id_des;
		$row[] = $post->kecamatan;
		$row[] = $post->id_kec;
		$row[] = $post->name;
		$row[] = $post->id;
		$row[] = "";
		$row[] = "";
		
		$data[] = $row;
		}
		
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->m_filter->count_all(),
			"recordsFiltered" => $this->m_filter->count_filtered(),
			"data" => $data,);
			//output to json format
			echo json_encode($output);
	}
 
	// END FILTER KELOMPOK

	// FILTER PETAMBAK 
	public function produksi()
	{
		$this->load->helper('url');
		$this->load->helper('form');
		$id_kota = $this->input->post('id');
		$kota = $this->m_filter->get_list_kota();
		$row  = $this->m_gudang->get();
		$rows  = $this->m_gudang->get_gudang();
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();	
		$kabkota = $this->m_gudang->get_kab();	
		$data = array(
			'row' => $row,
			'kabkota' => $kabkota,
			'rows' => $rows,
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
		);

		$opt = array('' => '-Select-');
		foreach ($kota as $country) {
			$opt[$country] = $country;
		}
//  
		$data['form_kota'] = form_dropdown('',$opt,'','id="name" class="form-control"');
		// $data['form_kecamatan'] = form_dropdown('',$opts,'','id="kecamatan" class="form-control"');
		$this->template->load('template', 'admin/produksi/import', $data);
	}

	
    	public function ajax_list_produksi()
	{

		$posts = $this->m_filter->get_datatables(); 
		$data = array();
		$no = $this->input->post('start');
		foreach ($posts as $post) 
		{
		 
		$no++;
		$row = array();
		$row[] = $no;
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = $post->desa;
		$row[] = $post->id_des;
		$row[] = $post->kecamatan;
		$row[] = $post->id_kec;
		$row[] = $post->name;
		$row[] = $post->id;
		
		
		$data[] = $row;
		}
		
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->m_filter->count_all(),
			"recordsFiltered" => $this->m_filter->count_filtered(),
			"data" => $data,);
			//output to json format
			echo json_encode($output);
	}
 
	// END FILTER PETAMBAK
	// FILTER LAHAN 
	public function lahan()
	{
		$this->load->helper('url');
		$this->load->helper('form');
		$id_kota = $this->input->post('id');
		$kota = $this->m_filter->get_list_kota();
		$row  = $this->m_gudang->get();
		$rows  = $this->m_gudang->get_gudang();
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();	
		$kabkota = $this->m_gudang->get_kab();	
		$data = array(
			'row' => $row,
			'kabkota' => $kabkota,
			'rows' => $rows,
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
		);

		$opt = array('' => '-Select-');
		foreach ($kota as $country) {
			$opt[$country] = $country;
		}
//  
		$data['form_kota'] = form_dropdown('',$opt,'','id="name" class="form-control"');
		// $data['form_kecamatan'] = form_dropdown('',$opts,'','id="kecamatan" class="form-control"');
		$this->template->load('template', 'admin/lahan/import', $data);
	}

	
    	public function ajax_list_lahan()
	{

		$posts = $this->m_filter->get_datatables(); 
		$data = array();
		$no = $this->input->post('start');
		foreach ($posts as $post) 
		{
		 
		$no++;
		$row = array();
		$row[] = $no;
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = $post->desa;
		$row[] = $post->id_des;
		$row[] = $post->kecamatan;
		$row[] = $post->id_kec;
		$row[] = $post->name;
		$row[] = $post->id;
		$row[] = "";
		$row[] = "";
		
		
		$data[] = $row;
		}
		
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->m_filter->count_all(),
			"recordsFiltered" => $this->m_filter->count_filtered(),
			"data" => $data,);
			//output to json format
			echo json_encode($output);
	}
 
	// END FILTER Lahan

	// FILTER GGN 
	public function ggn()
	{
		$this->load->helper('url');
		$this->load->helper('form');
		$id_kota = $this->input->post('id');
		$kota = $this->m_filter->get_list_kota();
		$row  = $this->m_gudang->get();
		$rows  = $this->m_gudang->get_gudang();
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();	
		$kabkota = $this->m_gudang->get_kab();	
		$data = array(
			'row' => $row,
			'kabkota' => $kabkota,
			'rows' => $rows,
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
		);

		$opt = array('' => '-Select-');
		foreach ($kota as $country) {
			$opt[$country] = $country;
		}
//  
		$data['form_kota'] = form_dropdown('',$opt,'','id="name" class="form-control"');
		// $data['form_kecamatan'] = form_dropdown('',$opts,'','id="kecamatan" class="form-control"');
		$this->template->load('template', 'admin/ggn/import', $data);
	}

	
    	public function ajax_list_ggn()
	{

		$posts = $this->m_filter->get_datatables(); 
		$data = array();
		$no = $this->input->post('start');
		foreach ($posts as $post) 
		{
		 
		$no++;
		$row = array();
		$row[] = $no;
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = $post->desa;
		$row[] = $post->id_des;
		$row[] = $post->kecamatan;
		$row[] = $post->id_kec;
		$row[] = $post->name;
		$row[] = $post->id;
		
		
		
		$data[] = $row;
		}
		
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->m_filter->count_all(),
			"recordsFiltered" => $this->m_filter->count_filtered(),
			"data" => $data,);
			//output to json format
			echo json_encode($output);
	}
 
	// END FILTER GGN

	// FILTER HIBAH 
	public function hibah()
	{
		$this->load->helper('url');
		$this->load->helper('form');
		$id_kota = $this->input->post('id');
		$kota = $this->m_filter->get_list_kota();
		$row  = $this->m_gudang->get();
		$rows  = $this->m_gudang->get_gudang();
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();	
		$kabkota = $this->m_gudang->get_kab();	
		$data = array(
			'row' => $row,
			'kabkota' => $kabkota,
			'rows' => $rows,
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
		);

		$opt = array('' => '-Select-');
		foreach ($kota as $country) {
			$opt[$country] = $country;
		}
//  
		$data['form_kota'] = form_dropdown('',$opt,'','id="name" class="form-control"');
		// $data['form_kecamatan'] = form_dropdown('',$opts,'','id="kecamatan" class="form-control"');
		$this->template->load('template', 'admin/hibah/import', $data);
	}

	
    	public function ajax_list_hibah()
	{

		$posts = $this->m_filter->get_datatables(); 
		$data = array();
		$no = $this->input->post('start');
		foreach ($posts as $post) 
		{
		 
		$no++;
		$row = array();
		$row[] = $no;
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = "";
		$row[] = $post->desa;
		$row[] = $post->id_des;
		$row[] = $post->kecamatan;
		$row[] = $post->id_kec;
		$row[] = $post->name;
		$row[] = $post->id;
		
		
		
		$data[] = $row;
		}
		
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->m_filter->count_all(),
			"recordsFiltered" => $this->m_filter->count_filtered(),
			"data" => $data,);
			//output to json format
			echo json_encode($output);
	}
 
	// END FILTER GGN

	function list_sub()
	{
		$id_kota = $this->input->post('id');
		$sub = $this->m_user->get_kec($id_kota);

		// Buat variabel untuk menampung tag-tag option nya
		// Set defaultnya dengan tag option Pilih
		$lists = "<option value='00'>Pilih</option>";

		foreach ($sub as $data) {
		$lists .= "<option value='" . $data->id . "'>" . $data->name . "</option>"; // Tambahkan tag option ke variabel $lists
		}

		$callback = array('list_sub' => $lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota

		echo json_encode($callback); // konversi varibael $callback menjadi JSON
	}


	function list_desa()
	{
		$id_kec = $this->input->post('id');


		$sub = $this->m_user->get_kel($id_kec);

		// Buat variabel untuk menampung tag-tag option nya
		// Set defaultnya dengan tag option Pilih
		$lists = "<option value='00'>Pilih</option>";

		foreach ($sub as $data) {
		$lists .= "<option value='" . $data->id . "'>" . $data->name . "</option>"; // Tambahkan tag option ke variabel $lists
		}

		$callback = array('list_desa' => $lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota

		echo json_encode($callback); // konversi varibael $callback menjadi JSON
	}

}

/* End of file User.php */
/* Location: ./application/controllers/User.php */