<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
		not_login();


		$this->load->model(['m_user']);
		$this->load->model(['m_grafik']);
		
	}

	public function index()
	{
		 
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();
		$t_stock = $this->m_user->stock_garam();
		$t_stock_kota = $this->m_user->stock_garam_kota();
		$t_lahan = $this->m_user->lahan_integrasi();
		$t_lahan_kota = $this->m_user->lahan_integrasi_kota();
		$t_lahans = $this->m_user->lahan_nonintegrasi();
		$t_lahans_kota = $this->m_user->lahan_nonintegrasi_kota();
		$t_petambak = $this->m_user->total_penambak();
		$t_petambak_kota = $this->m_user->total_penambak_kota();
		$t_gudang = $this->m_user->total_gudang();
		$t_gudang_kota = $this->m_user->total_gudang_kota();
		$produksi_integrasi = $this->m_user->produksi_integrasi();
		$produksi_integrasi_kota = $this->m_user->produksi_integrasi_kota();
		$produksi_nonintegrasi = $this->m_user->produksi_nonintegrasi();
		$produksi_nonintegrasi_kota = $this->m_user->produksi_nonintegrasi_kota();
		$grafiklahan = $this->m_grafik->grafik_lahan();
		$grafiklahan_kota = $this->m_grafik->grafik_lahan_kota();
		$grafikproduksi = $this->m_grafik->grafik_produksi();
		$grafikproduksi_kota = $this->m_grafik->grafik_produksi_kota();
		$grafikstok = $this->m_grafik->grafik_stok();
		$grafikstok_kota = $this->m_grafik->grafik_stok_kota();
		$grafikstok_gudang = $this->m_grafik->grafik_stok_gudang();
		$grafikpetambak = $this->m_grafik->grafik_petambak();
		$grafikpetambak_kota = $this->m_grafik->grafik_petambak_kota();
		$data = array(
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
			'stock'=>$t_stock,
			'stock_kota'=>$t_stock_kota,
			'lahan'=>$t_lahan,
			'lahan_kota'=>$t_lahan_kota,
			'lahans'=>$t_lahans,
			'lahans_kota'=>$t_lahans_kota,
			'petambak'=>$t_petambak,
			'petambak_kota'=>$t_petambak_kota,
			'gudang'=>$t_gudang,
			'gudang_kota'=>$t_gudang_kota,
			'produksi_integrasi'=>$produksi_integrasi,
			'produksi_integrasi_kota'=>$produksi_integrasi_kota,
			'produksi_nonintegrasi'=>$produksi_nonintegrasi,
			'produksi_nonintegrasi_kota'=>$produksi_nonintegrasi_kota,
			'grafik_lahan' => $grafiklahan,
			'grafik_lahan_kota' => $grafiklahan_kota,
			'grafik_produksi' => $grafikproduksi,
			'grafik_produksi_kota' => $grafikproduksi_kota,
			'grafik_stok' => $grafikstok,
			'grafik_stok_kota' => $grafikstok_kota,
			'grafik_stok_gudang' => $grafikstok_gudang,
			'grafik_petambak' => $grafikpetambak,
			'grafik_petambak_kota' => $grafikpetambak_kota,

		);
		$this->template->load('template', 'admin/dashboard', $data);
	}

	public function kabkota()
	{
		$aktifkota = $this->m_user->get_aktivasi();	
		$hitung = $this->m_user->count_kota();
		$t_stock = $this->m_user->stock_garam();
		$t_lahan = $this->m_user->lahan_integrasi();
		$t_lahans = $this->m_user->lahan_nonintegrasi();
		$t_petambak = $this->m_user->total_penambak();
		$t_gudang = $this->m_user->total_gudang();
		$produksi_integrasi = $this->m_user->produksi_integrasi();
		$produksi_nonintegrasi = $this->m_user->produksi_nonintegrasi();
		$data = array(
			'aktifkota' => $aktifkota,
			'hitung' => $hitung,
			'stock'=>$t_stock,
			'lahan'=>$t_lahan,
			'lahans'=>$t_lahans,
			'petambak'=>$t_petambak,
			'gudang'=>$t_gudang,
			'produksi_integrasi'=>$produksi_integrasi,
			'produksi_nonintegrasi'=>$produksi_nonintegrasi,

		); 
		$this->template->load('template', 'admin/dashboard', $data);

	}

	public function gudang()
	{
		$aktif = $this->m_user->get_aktivasi();
		$aktifkota = $this->m_user->get_aktivasi_kota();	
		$hitung = $this->m_user->count();
		$t_stock = $this->m_user->stock_garam();
		$t_lahan = $this->m_user->lahan_integrasi();
		$t_lahans = $this->m_user->lahan_nonintegrasi();
		$t_petambak = $this->m_user->total_penambak();
		$t_gudang = $this->m_user->total_gudang();
		$produksi_integrasi = $this->m_user->produksi_integrasi();
		$produksi_nonintegrasi = $this->m_user->produksi_nonintegrasi();
		$data = array(
			'aktif' => $aktif,
			'aktifkota' => $aktifkota,
			'hitung' => $hitung,
			'stock'=>$t_stock,
			'lahan'=>$t_lahan,
			'lahans'=>$t_lahans,
			'petambak'=>$t_petambak,
			'gudang'=>$t_gudang,
			'produksi_integrasi'=>$produksi_integrasi,
			'produksi_nonintegrasi'=>$produksi_nonintegrasi,

		);
		$this->template->load('template', 'admin/dashboard', $data);

	}


	
}
