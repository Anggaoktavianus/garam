<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homes extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
   	     parent::__construct();
		 is_login();
   	        $this->load->model('m_user');
		$this->load->model('m_gudang');
		$this->load->model('m_koperasi');
		$this->load->model('m_kelompok');
		$this->load->model('m_contact');
		$this->load->library('form_validation');

        }

	public function index()
	{
		$this->load->view('template/head');
		$this->load->view('template/header');
		$this->load->view('front/home');
        // $this->load->view('template/footer');
	}

	public function register()
	{
		$kabkota = $this->m_user->get_kab();
		$aktif = $this->m_user->get_aktivasi();	
		$hitung = $this->m_user->count();	
		$data = array(
			'kabkota' => $kabkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
		);
		// $this->load->view('template/head');
		$this->load->view('admin/register',$data);
        // $this->load->view('template/footer');
	}
	

	public function login()
	{
		// $this->load->view('template/head');
		// $this->load->view('template/header');
		$this->load->view('admin/login');
	}

	public function valid()
	{
	$this->load->view('template/heads');
	$this->load->view('admin/validate');
	$this->load->view('template/footerx');
	}
	

	function list_sub()
	{
		$id_kota = $this->input->post('id');


		$sub = $this->m_user->get_kec($id_kota);

		// Buat variabel untuk menampung tag-tag option nya
		// Set defaultnya dengan tag option Pilih
		$lists = "<option value='00'>Pilih</option>";

		foreach ($sub as $data) {
		$lists .= "<option value='" . $data->id . "'>" . $data->name . "</option>"; // Tambahkan tag option ke variabel $lists
		}

		$callback = array('list_sub' => $lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota

		echo json_encode($callback); // konversi varibael $callback menjadi JSON
	}

	function list_desa()
	{
		$id_kec = $this->input->post('id');


		$sub = $this->m_user->get_kel($id_kec);

		// Buat variabel untuk menampung tag-tag option nya
		// Set defaultnya dengan tag option Pilih
		$lists = "<option value='00'>Pilih</option>";

		foreach ($sub as $data) {
		$lists .= "<option value='" . $data->id . "'>" . $data->name . "</option>"; // Tambahkan tag option ke variabel $lists
		}

		$callback = array('list_desa' => $lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota

		echo json_encode($callback); // konversi varibael $callback menjadi JSON
	}

	public function register_proses(){

		$this->form_validation->set_rules('email', 'Email', 'trim|required|min_length[5]|is_unique[garam_admin.email]');
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|min_length[5]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		$this->form_validation->set_rules(
		'password2',
		'Konfirmasi password',
		'trim|required|matches[password]',
		array('matches' => '{field} tidak sesuai dengan password')
		);
		$this->form_validation->set_rules('role_id', 'Akses', 'trim|required');

		$this->form_validation->set_message('required', '{field} masih kosong, silahkan diisi');
		$this->form_validation->set_message('min_length', '{field} minimal 5 karakter');
		$this->form_validation->set_message('is_unique', '<span style="color:red"> *{field} sudah dipakai</span> ');

		$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

		if ($this->form_validation->run() == FALSE) {
            	$kabkota = $this->m_user->get_kab();
		$aktif = $this->m_user->get_aktivasi();	
		$hitung = $this->m_user->count();	
		$data = array(
			'kabkota' => $kabkota,
			'aktif' => $aktif,
			'hitung' => $hitung, 
		);

		$this->load->view('admin/register',$data);
		
		} else {
		 if($this->m_user->m_register_gudang()){
           	 $this->session->set_flashdata('info', 'success');
          	 $this->session->set_flashdata('pesan', 'Terima kasih sudah melakukan registrasi ');
           	redirect('homes/login','refresh');
		}
		echo "<script>window.location ='" . site_url('homes/login') . "' ; </script>";
		}

	
		
	}

	public function lahan()
	{
		$this->load->view('template/head');
		$this->load->view('template/header');
		$this->load->view('front/lahan');
        // $this->load->view('template/footer');
	}
	
	public function produksi()
	{
		$this->load->view('template/head');
		$this->load->view('template/header');
		$this->load->view('front/produksi');
        // $this->load->view('template/footer');
	}

	public function stock()
	{
		$this->load->view('template/head');
		$this->load->view('template/header');
		$this->load->view('front/stock');
        // $this->load->view('template/footer');
	}

	public function gudang()
	{	
		$row  = $this->m_gudang->get();	
		$data = array(
			'row' => $row,
		);
		$this->load->view('template/head');
		$this->load->view('template/header');
		$this->load->view('front/gudang',$data);
		
	}

	public function kelompok()
	{
		$row  = $this->m_kelompok->get();	
		$data = array(
			'row' => $row,
		);
		$this->load->view('template/head');
		$this->load->view('template/header');
		$this->load->view('front/kelompok',$data);
        // $this->load->view('template/footer');
	}

	public function koperasi()
	{
		$row  = $this->m_koperasi->get();	
		$data = array(
			'row' => $row,
		);
		$this->load->view('template/head');
		$this->load->view('template/header');
		$this->load->view('front/koperasi',$data);
        // $this->load->view('template/footer');
	}

	public function contact()
	{	
		$id = $this->input->post('id');
		$query = $this->m_contact->getall($id);
		if ($query->num_rows() > 0) {
				$row = $query->row();
				 $data = array(
					'row' => $row,
				);
				$this->load->view('template/head');
				$this->load->view('template/header');
				$this->load->view('front/contact',$data);
			} else {
				echo "<script>alert ('Data tidak ditemukan');";
				echo "window.location ='" . site_url('homes') . "' ; </scrip>";
			}
		
		
	}

    

    
}


