<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;

class Produksi extends CI_Controller {

        function __construct() {
   	     parent::__construct();
		 not_login();
		$this->load->model('m_produksi');
		$this->load->model('m_user');
		$this->load->model('m_lahan');
		$this->load->library('form_validation');

        }

	//IMPORT EXCEL
	public function uploaddata()
	{
		$config['upload_path'] = './uploads/produksi/';
		$config['allowed_types'] = 'xlsx|xls';
		$config['file_name'] = 'produksi' . time();
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if ($this->upload->do_upload('importexcel')) {
		$file = $this->upload->data();
		$reader = ReaderEntityFactory::createXLSXReader();

		$reader->open('uploads/produksi/' . $file['file_name']);
		foreach ($reader->getSheetIterator() as $sheet) {
			$numRow = 1;
			foreach ($sheet->getRowIterator() as $row) {
			if ($numRow > 1) {
				$databarang = array(
				'kp1_lahan_integrasi'  		=> $row->getCellAtIndex(1),
				'kp1_harga_integrasi'  		=> $row->getCellAtIndex(2),
				'kp2_lahan_integrasi'  		=> $row->getCellAtIndex(3),
				'kp2_harga_integrasi'  		=> $row->getCellAtIndex(4),
				'kp3_lahan_integrasi'  		=> $row->getCellAtIndex(5),
				'kp3_harga_integrasi'  		=> $row->getCellAtIndex(6),
				'kp1_lahan_nonintegrasi'  	=> $row->getCellAtIndex(7),
				'kp1_harga_nonintegrasi'  	=> $row->getCellAtIndex(8),
				'kp2_lahan_nonintegrasi'  	=> $row->getCellAtIndex(9),
				'kp2_harga_nonintegrasi'  	=> $row->getCellAtIndex(10),
				'kp3_lahan_nonintegrasi'  	=> $row->getCellAtIndex(11),
				'kp3_harga_nonintegrasi'  	=> $row->getCellAtIndex(12),
				'kp1_garam_nontambak'  		=> $row->getCellAtIndex(13),
				'kp1_harga_nontambak'  		=> $row->getCellAtIndex(14),
				'kp2_garam_nontambak'  		=> $row->getCellAtIndex(15),
				'kp2_harga_nontambak'  		=> $row->getCellAtIndex(16),
				'kp3_garam_nontambak'  		=> $row->getCellAtIndex(17),
				'kp3_harga_nontambak'  		=> $row->getCellAtIndex(18),
				'bulan'  			=> $row->getCellAtIndex(19),
				'tahun'  			=> $row->getCellAtIndex(20),
				'kelurahan_id'  		=> $row->getCellAtIndex(22),
				'kecamatan_id'  		=> $row->getCellAtIndex(24),
				'kabkota_id'  			=> $row->getCellAtIndex(26),
				'created_at'			=>date('Y-m-d H:i:s'),
				);
				$this->m_produksi->import_data($databarang);
			}
			$numRow++;
			}
			$reader->close();
			// unlink('uploads/gudang/' . $file['file_name']);
			$this->session->set_flashdata('pesan', 'import Data Berhasil');
			redirect('produksi');
		}
		} else {
		echo "Error :" . $this->upload->display_errors();
		};
	}


	function get_ajax()
    	{
		$list = $this->m_produksi->get_datatables();
		$data = array();
		$no = @$_POST['start'];
		foreach ($list as $a) {
		$no++;
		$row = array();
		$row[] = $no . ".";
		$row[] = $a->kp1_lahan_integrasi;
		$row[] = "Rp.".number_format($a->kp1_harga_integrasi);
		$row[] = $a->kp2_lahan_integrasi;
		$row[] = "Rp.".number_format($a->kp2_harga_integrasi);
		$row[] = $a->kp3_lahan_integrasi;
		$row[] = "Rp.".number_format($a->kp3_harga_integrasi);
		$row[] = $a->kp1_lahan_nonintegrasi;
		$row[] = "Rp.".number_format($a->kp1_harga_nonintegrasi);
		$row[] = $a->kp2_lahan_nonintegrasi;
		$row[] = "Rp.".number_format($a->kp2_harga_nonintegrasi);
		$row[] = $a->kp3_lahan_nonintegrasi;
		$row[] = "Rp.".number_format($a->kp3_harga_nonintegrasi);
		$row[] = $a->kp1_garam_nontambak;
		$row[] = "Rp.".number_format($a->kp1_harga_nontambak);
		$row[] = $a->kp2_garam_nontambak;
		$row[] = "Rp.".number_format($a->kp2_harga_nontambak);
		$row[] = $a->kp3_garam_nontambak;
		$row[] = "Rp.".number_format($a->kp3_harga_nontambak);
		$row[] = $a->bulan;
		$row[] = $a->tahun;
		$row[] = $a->desa;
		$row[] = $a->kecamatan;
		$row[] = $a->kabkota;
		// add html for action
		$row[] = '<form action="' .site_url('produksi/del').'" method="post"><a href="' . site_url('produksi/edit/' . $a->id) . '" class="btn btn-success text-white btn-xs"><i class="fas fa-pencil-alt"></i></a>
			<input type="hidden" name="id" value="'. $a->id.'"> <button onclick="return confirm(\'Yakin hapus data?\')" class="btn btn-danger btn-xs text-white">
				<i class="fas fa-trash-alt "></i>
				</button></form>';
		$data[] = $row;
		}
		$output = array(
		"draw" => @$_POST['draw'],
		"recordsTotal" => $this->m_produksi->count_all(),
		"recordsFiltered" => $this->m_produksi->count_filtered(),
		"data" => $data,
		);
		// output to json format
		echo json_encode($output);
    	}

	//     Ajax Kab/kota
	function get_ajax_kota()
    	{	
		$list = $this->m_produksi->get_datatables_kota();
		$data = array();
		$no = @$_POST['start'];
		foreach ($list as $a) {
		$no++;
		$row = array();
		$row[] = $no . ".";
		$row[] = $a->kp1_lahan_integrasi;
		$row[] = "Rp.".number_format($a->kp1_harga_integrasi);
		$row[] = $a->kp2_lahan_integrasi;
		$row[] = "Rp.".number_format($a->kp2_harga_integrasi);
		$row[] = $a->kp3_lahan_integrasi;
		$row[] = "Rp.".number_format($a->kp3_harga_integrasi);
		$row[] = $a->kp1_lahan_nonintegrasi;
		$row[] = "Rp.".number_format($a->kp1_harga_nonintegrasi);
		$row[] = $a->kp2_lahan_nonintegrasi;
		$row[] = "Rp.".number_format($a->kp2_harga_nonintegrasi);
		$row[] = $a->kp3_lahan_nonintegrasi;
		$row[] = "Rp.".number_format($a->kp3_harga_nonintegrasi);
		$row[] = $a->kp1_garam_nontambak;
		$row[] = "Rp.".number_format($a->kp1_harga_nontambak);
		$row[] = $a->kp2_garam_nontambak;
		$row[] = "Rp.".number_format($a->kp2_harga_nontambak);
		$row[] = $a->kp3_garam_nontambak;
		$row[] = "Rp.".number_format($a->kp3_harga_nontambak);
		$row[] = $a->bulan;
		$row[] = $a->tahun;
		$row[] = $a->desa;
		$row[] = $a->kecamatan;
		$row[] = $a->kabkota;
		// add html for action
		$row[] = '<form action="' .site_url('produksi/del').'" method="post"><a href="' . site_url('produksi/edit/' . $a->id) . '" class="btn btn-success text-white btn-xs"><i class="fas fa-pencil-alt"></i></a>
			<input type="hidden" name="id" value="'. $a->id.'"> <button onclick="return confirm(\'Yakin hapus data?\')" class="btn btn-danger btn-xs text-white">
				<i class="fas fa-trash-alt "></i>
				</button></form>';
		$data[] = $row;
		}
		$output = array(
		"draw" => @$_POST['draw'],
		"recordsTotal" => $this->m_produksi->count_all_kota($this->session->userdata('kabkota_id')),
		"recordsFiltered" => $this->m_produksi->count_filtered_kota($this->session->userdata('kabkota_id')),
		"data" => $data,
		);
		// output to json format
		echo json_encode($output);
    	}

	public function index()
	{	
		$row  = $this->m_produksi->get();
		$bulan  = $this->m_produksi->bulan();
		$tahun  = $this->m_produksi->tahun();
		$kecam  = $this->m_lahan->get_keca_kota(); 
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();
		$data = array(
			'row' => $row,
			'bulan' => $bulan,
			'kecam' => $kecam,
			'tahun' => $tahun,
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
		);
		$this->template->load('template', 'admin/produksi/v_produksi', $data);	
		
	}
	

	public function add_index()
	{
		$kabkota = $this->m_produksi->get_kab();
		$aktif = $this->m_user->get_aktivasi();
		$kecam  = $this->m_lahan->get_keca_kota();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();		
		$data = array(
			'kabkota' => $kabkota,
			'kecam' => $kecam,
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
		); 
		$this->template->load('template', 'admin/produksi/add_produksi', $data);
		
	}
        
	public function add()
	{

		// $this->form_validation->set_rules('', 'NIK', 'trim|required|min_length[2]|is_unique[garam_produksi.produksi]');
		$this->form_validation->set_rules('kp1_lahan_integrasi', 'Lahan Integrasi', 'trim|required');
		$this->form_validation->set_message('min_length', '{field} minimal 5 karakter');
		$this->form_validation->set_message('is_unique', '<span style="color:red"> *{field} sudah dipakai</span> ');

		$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

		if ($this->form_validation->run() == FALSE) {
			
            	$kabkota = $this->m_produksi->get_kab();
		$aktif = $this->m_user->get_aktivasi();
		$kecam  = $this->m_lahan->get_keca_kota();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();	
		$data = array(
			'kabkota' => $kabkota,
			'kecam' => $kecam,
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
		);
		$this->template->load('template', 'admin/produksi/add_produksi', $data);
		} else {
		 if($this->m_produksi->add()){
           	$this->session->set_flashdata('info', 'success');
          	 $this->session->set_flashdata('pesan', 'Berhasil menambahkan data.');
           	redirect('produksi','refresh');
		}
		echo "<script>window.location ='" . site_url('produksi') . "' ; </script>";
		}
	}

	
	public function edit($id)
	{

		$this->form_validation->set_rules('kp1_lahan_integrasi', 'Lahan Integrasi', 'trim|required');
		
		$this->form_validation->set_message('min_length', '{field} minimal 5 karakter');
		$this->form_validation->set_message('is_unique', '{field} sudah dipakai');

		$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

		if ($this->form_validation->run() == FALSE) {
			 $query = $this->m_produksi->getall($id);
			if ($query->num_rows() > 0) {
				$row = $query->row();
				$kabkota = $this->m_produksi->get_kab();
				$keca = $this->m_produksi->get_keca();
				$kecam  = $this->m_lahan->get_keca_kota();
				$desa = $this->m_produksi->get_kelu();
				$role = $this->m_produksi->get_role();
				$aktif = $this->m_user->get_aktivasi();	
				$aktifkota = $this->m_user->get_aktivasi_kota();
				$hitung = $this->m_user->count();
				$hitungs = $this->m_user->count_kota();	
				 $data = array(
					'row' => $row,
					'kabkota' => $kabkota,
					'keca' => $keca,
					'kecam' => $kecam,
					'desa' => $desa,
					'role' => $role,
					'aktifkota' => $aktifkota,
					'aktif' => $aktif,
					'hitung' => $hitung,
					'hitungs' => $hitungs,

				);
				$this->template->load('template', 'admin/produksi/edit_produksi', $data);
				
			} else {
				echo "<script>alert ('Data tidak ditemukan');";
				echo "window.location ='" . site_url('user') . "' ; </scrip>";
			}
		} else {
		$this->m_produksi->edit();
		 if($this->db->affected_rows() > 0){
		$this->session->set_flashdata('info', 'success');
          	 $this->session->set_flashdata('pesan', 'Berhasil merubah data.');
           	redirect('produksi','refresh');
		}
		echo "<script>window.location ='" . site_url('produksi') . "' ; </script>";
		}
	}
	 
	function nama_check()
	{

		$post = $this->input->post(NULL, TRUE);
		$query = $this->db->query("SELECT * FROM garam_produksi WHERE nik= '$post[nik]' AND id != '$post[id]' AND deleted_at IS NULL");
		if ($query->num_rows() > 0) {
		$this->form_validation->set_message('nama_check', '{field} ini sudah dipakai, silahkan ganti');
		return FALSE;
		} else {
		return TRUE;
		}
	}

	public function del()
	{
		$id = $this->input->post('id');
		// $this->m_produksi->del($id);

		$row = $this->m_produksi->get($id);

		if ($row) {
		$this->m_produksi->del($id);
		
		$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('pesan', 'Data berhasil dihapus');
			redirect(site_url('produksi'));
		} else {
		$this->session->set_flashdata('info', 'danger');
		$this->session->set_flashdata('pesan', 'gagal hapus ');
		redirect(site_url('produksi'));
		}
	}

	

	public function aktivasi()
	{
		$id = $this->input->post('id');
		// $this->m_produksi->del($id);

		$row = $this->m_produksi->get($id);

		if ($row) {
		$this->m_produksi->aktivasi($id);
		
		$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('pesan', 'Akun berhasil diaktivasi');
			redirect(site_url('user'));
		} else {
		$this->session->set_flashdata('info', 'danger');
		$this->session->set_flashdata('pesan', 'gagal aktivasi ');
		redirect(site_url('user'));
		}
	}

}

/* End of file User.php */
/* Location: ./application/controllers/User.php */