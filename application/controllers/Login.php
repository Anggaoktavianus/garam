<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

    public function index()
    {
        is_login();
	// $this->load->model('m_user');
        $this->load->view('admin/login');
    }
    

    public function login_proses() {
        $this->load->model('m_user');
        $this->form_validation->set_rules('email', 'E-mail', 'trim|required|min_length[3]|max_length[45]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[3]|max_length[12]');

        if ($this->form_validation->run() == TRUE) {
                
            if($this->m_user->m_cek_mail()->num_rows()==1) { //cek email terdaftar
            
                $db=$this->m_user->m_cek_mail()->row();
                if(hash_verified($this->input->post('password'),$db->password)) {
                    //    $this->input->post('alamat',$almt->alamat);
                        
                        $data_login=array(
                                'id' =>$db->id,
                                'email'  =>$db->email,
                                'nama'   =>$db->nama,
                                'status'   =>$db->status,
                                'kabkota_id'   =>$db->kabkota_id,
                                'kelurahan_id'   =>$db->kelurahan_id,
                                // 'alamat'   =>$alamat->alamat

                        );
                    
                    $this->session->set_userdata($data_login);
                    redirect('dashboard');

                } else{
                    $this->session->set_flashdata('info', 'danger');
                    $this->session->set_flashdata('pesan', 'Login gagal: password atau email salah!');
                    redirect('homes/login','refresh');
                }
                

            } else { 
                // jika status belum aktif!
                if ($this->m_user->m_cek_mails()->num_rows()==1) {
                    $db=$this->m_user->m_cek_mails()->row();
                    if(hash_verified($this->input->post('password'),$db->password)) {
                        $data_logins=array(
                                'id' =>$db->id,
                                'email'  =>$db->email,
                                'nama'   =>$db->nama,
                                'status'   =>$db->status,
                                'kabkota_id'   =>$db->kabkota_id,
                                'kelurahan_id'   =>$db->kelurahan_id,

                        );
                    
                    // $this->session->set_userdata($data_logins);
                    // redirect('homes/valid','refresh');
                    $this->load->view('template/heads');
                    $this->load->view('admin/validate',$data_logins);
                    $this->load->view('template/footerx');

                    } else{
                        $this->session->set_flashdata('info', 'danger');
                        $this->session->set_flashdata('pesan', 'Login gagal: Email belum aktif atau password salah!!');
                        redirect('homes/login','refresh');
                    }
                    
                }else {
                    $this->session->set_flashdata('info', 'danger');
                    $this->session->set_flashdata('pesan', 'Login gagal: Email belum terdaftar!');
                    redirect('homes/login','refresh');
                }
            }

        } else { 
            $this->session->set_flashdata('info', 'success');
            $this->session->set_flashdata('pesan', 'error');
            redirect('homes/','refresh');
        }

	}

	public function logout()
    {
        $params = array('id','email');

        $this->session->unset_userdata($params);
        redirect('homes/login');
        # code...
   	}


    
}
