<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;

class Kelompok extends CI_Controller {

        function __construct() {
   	     parent::__construct();
		 not_login();
		//  check_admin();
   	        // $this->load->model('m_gudang');
		$this->load->model('m_kelompok');
		$this->load->model('m_user');
		$this->load->model('m_lahan');
		$this->load->library('form_validation');

        }

	//IMPORT EXCEL
	public function uploaddata()
	{
		$config['upload_path'] = './uploads/kelompok/';
		$config['allowed_types'] = 'xlsx|xls';
		$config['file_name'] = 'kelompok' . time();
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if ($this->upload->do_upload('importexcel')) {
		$file = $this->upload->data();
		$reader = ReaderEntityFactory::createXLSXReader();

		$reader->open('uploads/kelompok/' . $file['file_name']);
		foreach ($reader->getSheetIterator() as $sheet) {
			$numRow = 1;
			foreach ($sheet->getRowIterator() as $row) {
			if ($numRow > 1) {
				$databarang = array(
				'nama'  		=> $row->getCellAtIndex(1),
				'telepon'  		=> $row->getCellAtIndex(2),
				'email'  		=> $row->getCellAtIndex(3),
				'sk'  			=> $row->getCellAtIndex(4),
				'alamat'  		=> $row->getCellAtIndex(5),
				'kelurahan_id'  	=> $row->getCellAtIndex(7),
				'kecamatan_id'  	=> $row->getCellAtIndex(9),
				'kabkota_id'  		=> $row->getCellAtIndex(11),
				'ketua'     		=> $row->getCellAtIndex(12),
				'jumlah_anggota'       	=> $row->getCellAtIndex(13),
				// 'status_lahan'      	=> $row->getCellAtIndex(14),
				// 'koperasi_id'  		=> $row->getCellAtIndex(7),
				// 'kelompok_id'  		=> $row->getCellAtIndex(5),
				'created_at'		=>date('Y-m-d H:i:s'),
				);
				$this->m_kelompok->import_data($databarang);
			}
			$numRow++;
			}
			$reader->close();
			// unlink('uploads/gudang/' . $file['file_name']);
			$this->session->set_flashdata('pesan', 'import Data Berhasil');
			redirect('kelompok');
		}
		} else {
		echo "Error :" . $this->upload->display_errors();
		};
	}

	function get_ajax()
    	{
		$list = $this->m_kelompok->get_datatables();
		// $list = $this->m_items->get_by_jumlah();
		$data = array();
		$no = @$_POST['start'];
		foreach ($list as $a) {
		$no++;
		$row = array();
		$row[] = $no . ".";
		$row[] = $a->nama;
		$row[] = $a->telepon;
		$row[] = $a->email;
		$row[] = $a->sk;
		$row[] = strtoupper($a->alamat).','.$a->desa.','.$a->kecamatan.','.$a->kabkota;
		$row[] = $a->ketua;
		$row[] = $a->jumlah_anggota;
		$row[] = $a->koperasi;
		// add html for action
		$row[] = '<form action="' .site_url('kelompok/del').'" method="post"><a href="' . site_url('kelompok/edit/' . $a->id) . '" class="btn btn-success text-white btn-xs"><i class="fas fa-pencil-alt"></i></a>
			<input type="hidden" name="id" value="'. $a->id.'"> <button onclick="return confirm(\'Yakin hapus data?\')" class="btn btn-danger btn-xs text-white">
				<i class="fas fa-trash-alt "></i>
				</button></form>';
		$data[] = $row;
		}
		$output = array(
		"draw" => @$_POST['draw'],
		"recordsTotal" => $this->m_kelompok->count_all(),
		"recordsFiltered" => $this->m_kelompok->count_filtered(),
		"data" => $data,
		);
		// output to json format
		echo json_encode($output);
    	}

	function get_ajax_kota()
    	{	
		$list = $this->m_kelompok->get_datatables_kota();
		$data = array();
		$no = @$_POST['start'];
		foreach ($list as $a) {
		$no++;
		$row = array();
		$row[] = $no . ".";
		$row[] = $a->nama;
		$row[] = $a->telepon;
		$row[] = $a->email;
		$row[] = $a->sk;
		$row[] = strtoupper($a->alamat).','.$a->desa.','.$a->kecamatan.','.$a->kabkota;
		$row[] = $a->ketua;
		$row[] = $a->jumlah_anggota;
		$row[] = $a->koperasi;
		// add html for action
		$row[] = '<form action="' .site_url('kelompok/del').'" method="post"><a href="' . site_url('kelompok/edit/' . $a->id) . '" class="btn btn-success text-white btn-xs"><i class="fas fa-pencil-alt"></i></a>
			<input type="hidden" name="id" value="'. $a->id.'"> <button onclick="return confirm(\'Yakin hapus data?\')" class="btn btn-danger btn-xs text-white">
				<i class="fas fa-trash-alt "></i>
				</button></form>';
		$data[] = $row;
		}
		$output = array(
		"draw" => @$_POST['draw'],
		"recordsTotal" => $this->m_kelompok->count_all_kota($this->session->userdata('kabkota_id')),
		"recordsFiltered" => $this->m_kelompok->count_filtered_kota($this->session->userdata('kabkota_id')),
		"data" => $data,
		);
		// output to json format
		echo json_encode($output);
    	}

	public function index()
	{	
		$row  = $this->m_kelompok->get();
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$kecam  = $this->m_lahan->get_keca_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();		
		$data = array(
			'row' => $row,
			'kecam'=>$kecam,
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
		);
		$this->template->load('template', 'admin/kelompok/v_kelompok', $data);		
		// $this->load->view('template/headx',$data);
		// $this->load->view('admin/kelompok/v_kelompok',$data);
		// $this->load->view('template/footerx');
		
	} 
	

	public function add_index()
	{
		$kabkota = $this->m_kelompok->get_kab();
		$kecam  = $this->m_lahan->get_keca_kota();
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();		
		$data = array(
			'kabkota' => $kabkota,
			'kecam'=>$kecam,
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
		); 
		$this->template->load('template', 'admin/kelompok/add_kelompok', $data);
		// $this->load->view('template/headx',$data);
		// $this->load->view('admin/kelompok/add_kelompok',$data);
		// $this->load->view('template/footerx');
		
	}
        
	public function add()
	{

		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|min_length[5]|is_unique[garam_kelompok.nama]');
		
		$this->form_validation->set_message('min_length', '{field} minimal 5 karakter');
		$this->form_validation->set_message('is_unique', '<span style="color:red"> *{field} sudah dipakai</span> ');

		$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

		if ($this->form_validation->run() == FALSE) {
			
            	$kabkota = $this->m_kelompok->get_kab();
		$kecam  = $this->m_lahan->get_keca_kota();
		$aktif = $this->m_user->get_aktivasi();	
		$aktifkota = $this->m_user->get_aktivasi_kota();
		$hitung = $this->m_user->count();
		$hitungs = $this->m_user->count_kota();	
		$data = array(
			'kabkota' => $kabkota,
			'kecam'=>$kecam,
			'aktifkota' => $aktifkota,
			'aktif' => $aktif,
			'hitung' => $hitung,
			'hitungs' => $hitungs,
		);
		$this->template->load('template', 'admin/kelompok/add_kelompok', $data);
		// $this->load->view('template/headx',$data);
		// $this->load->view('admin/kelompok/add_kelompok',$data);
		// $this->load->view('template/footerx');
		} else {
		 if($this->m_kelompok->add()){
           	$this->session->set_flashdata('info', 'success');
          	 $this->session->set_flashdata('pesan', 'Berhasil menambahkan data.');
           	redirect('kelompok','refresh');
		}
		echo "<script>window.location ='" . site_url('kelompok') . "' ; </script>";
		}
	}

	
	public function edit($id)
	{

		$this->form_validation->set_rules('nama', 'Nama', 'min_length[3]|callback_nama_check');
		
		$this->form_validation->set_message('min_length', '{field} minimal 5 karakter');
		$this->form_validation->set_message('is_unique', '{field} sudah dipakai');

		$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

		if ($this->form_validation->run() == FALSE) {
			 $query = $this->m_kelompok->getall($id);
			if ($query->num_rows() > 0) {
				$row = $query->row();
				$kabkota = $this->m_kelompok->get_kab();
				$keca = $this->m_kelompok->get_keca();
				$kecam  = $this->m_lahan->get_keca_kota();
				$desa = $this->m_kelompok->get_kelu();
				$koperasi = $this->m_kelompok->get_koperasi();
				$role = $this->m_kelompok->get_role();
				$aktif = $this->m_user->get_aktivasi();	
				$aktifkota = $this->m_user->get_aktivasi_kota();
				$hitung = $this->m_user->count();
				$hitungs = $this->m_user->count_kota();	
				 $data = array(
					'row' => $row,
					'kabkota' => $kabkota,
					'koperasi' => $koperasi,'selected' =>$row->koperasi_id,
					'keca' => $keca,
					'kecam'=>$kecam,
					'desa' => $desa,
					'role' => $role,
					'aktifkota' => $aktifkota,
					'aktif' => $aktif,
					'hitung' => $hitung,
					'hitungs' => $hitungs,

				);
				$this->template->load('template', 'admin/kelompok/edit_kelompok', $data);
				
				// $this->load->view('template/headx',$data);
				// $this->load->view('admin/kelompok/edit_kelompok',$data);
				// $this->load->view('template/footerx');
				// echo "<script>alert ('Data tidak ditemukan');";
			} else {
				echo "<script>alert ('Data tidak ditemukan');";
				echo "window.location ='" . site_url('user') . "' ; </scrip>";
			}

            	// redirect('user/add_index');.
		} else {
		// $post = $this->input->post(null, TRUE);
		$this->m_kelompok->edit();
		 if($this->db->affected_rows() > 0){
		$this->session->set_flashdata('info', 'success');
          	 $this->session->set_flashdata('pesan', 'Berhasil merubah data.');
           	redirect('kelompok','refresh');
		}
		echo "<script>window.location ='" . site_url('kelompok') . "' ; </script>";
		}
	}	

	function nama_check()
	{

		$post = $this->input->post(NULL, TRUE);
		$query = $this->db->query("SELECT * FROM garam_kelompok WHERE nama= '$post[nama]' AND id != '$post[id]'");
		if ($query->num_rows() > 0) {
		$this->form_validation->set_message('nama_check', '{field} ini sudah dipakai, silahkan ganti');
		return FALSE;
		} else {
		return TRUE;
		}
	}

	public function del()
	{
		$id = $this->input->post('id');
		// $this->m_kelompok->del($id);

		$row = $this->m_kelompok->get($id);

		if ($row) {
		$this->m_kelompok->del($id);
		
		$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('pesan', 'Data berhasil dihapus');
			redirect(site_url('kelompok'));
		} else {
		$this->session->set_flashdata('info', 'danger');
		$this->session->set_flashdata('pesan', 'gagal hapus ');
		redirect(site_url('kelompok'));
		}
	}

	public function aktivasi()
	{
		$id = $this->input->post('id');
		// $this->m_kelompok->del($id);

		$row = $this->m_kelompok->get($id);

		if ($row) {
		$this->m_kelompok->aktivasi($id);
		
		$this->session->set_flashdata('info', 'success');
			$this->session->set_flashdata('pesan', 'Akun berhasil diaktivasi');
			redirect(site_url('user'));
		} else {
		$this->session->set_flashdata('info', 'danger');
		$this->session->set_flashdata('pesan', 'gagal aktivasi ');
		redirect(site_url('user'));
		}
	}

}

/* End of file User.php */
/* Location: ./application/controllers/User.php */