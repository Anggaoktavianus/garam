<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;

class Contact extends CI_Controller {

        function __construct() {
   	     parent::__construct();
		 not_login();
		//  check_admin();
   	        $this->load->model('m_gudang');
		$this->load->model('m_user');
		$this->load->model('m_contact');
		$this->load->model('m_lahan');
		$this->load->library('form_validation');

        }

	
	public function index()
	{
		$id = $this->input->post('id');
		$this->form_validation->set_rules('alamat', 'Alamat', 'min_length[3]');
		
		$this->form_validation->set_message('min_length', '{field} minimal 5 karakter');
		$this->form_validation->set_message('is_unique', '{field} sudah dipakai');

		$this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

		if ($this->form_validation->run() == FALSE) {
			 $query = $this->m_contact->getall($id);
			if ($query->num_rows() > 0) {
				$row = $query->row();
				$aktif = $this->m_user->get_aktivasi();	
				$aktifkota = $this->m_user->get_aktivasi_kota();
				$hitung = $this->m_user->count();
				$hitungs = $this->m_user->count_kota();	
				 $data = array(
					'row' => $row,
					'aktifkota' => $aktifkota,
					'aktif' => $aktif,
					'hitung' => $hitung,
					'hitungs' => $hitungs,

				);
				$this->template->load('template', 'admin/contact', $data);
			} else {
				echo "<script>alert ('Data tidak ditemukan');";
				echo "window.location ='" . site_url('contact') . "' ; </scrip>";
			}

            	// redirect('user/add_index');.
		} else {
		// $post = $this->input->post(null, TRUE);
		$this->m_contact->edit();
		 if($this->db->affected_rows() > 0){
		$this->session->set_flashdata('info', 'success');
          	 $this->session->set_flashdata('pesan', 'Berhasil merubah data.');
           	redirect('contact','refresh');
		}
		echo "<script>window.location ='" . site_url('contact') . "' ; </script>";
		}
	}

     

	public function aktivasi()
	{
		$id = $this->input->post('id');
		// $this->m_gudang->del($id);

		$row = $this->m_gudang->get($id);

		if ($row) {
		$this->m_gudang->aktivasi($id);
		
		$this->session->set_flashdata('info', 'success');
		$this->session->set_flashdata('pesan', 'Akun berhasil diaktivasi');
			redirect(site_url('user'));
		} else {
		$this->session->set_flashdata('info', 'danger');
		$this->session->set_flashdata('pesan', 'gagal aktivasi ');
		redirect(site_url('user'));
		}
	}	

}

/* End of file User.php */
/* Location: ./application/controllers/User.php */